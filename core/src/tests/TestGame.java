package tests;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.math.Vector3;
import de.tolga.dunestowerdefense.EnemyUnitPath;
import de.tolga.dunestowerdefense.Node;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGame extends Game {
    TestGame tests;
    TestScreen testScreen;
    Vector3 testPosition = new Vector3();

    @Override
    public void create() {
        testScreen = new TestScreen();
        setScreen(testScreen);
    }

    public TestGame getTestGame() {
        return tests;
    }

    public TestScreen getTestScreen() {
        return testScreen;
    }

    public void update() {
        this.EnemyUnitPath();
        System.out.println(true);
    }

    @Test
    public void EnemyUnitPath() {
        boolean[][] map = new boolean[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                map[i][j] = true;
            }
        }
        map[9][0] = false;
        map[9][1] = false;
        map[8][1] = false;
        map[7][1] = false;
        map[6][1] = false;
        map[6][2] = false;
        map[6][3] = false;
        map[6][4] = false;
        map[6][5] = false;
        map[6][6] = false;
        map[5][6] = false;
        map[4][6] = false;
        map[3][6] = false;
        map[2][6] = false;
        map[1][6] = false;
        map[1][7] = false;
        map[1][8] = false;
        map[1][9] = false;
        map[0][9] = false;
        GraphPath<Node> temp = EnemyUnitPath.getPath(map);
        boolean checkIfPathIsCorrect = false;
        try {
            checkIfPathIsCorrect = temp.get(0).x == 0 && temp.get(0).y == 9
                    && temp.get(1).x == 1 && temp.get(1).y == 9
                    && temp.get(2).x == 1 && temp.get(2).y == 8
                    && temp.get(3).x == 1 && temp.get(3).y == 7
                    && temp.get(4).x == 1 && temp.get(4).y == 6
                    && temp.get(5).x == 2 && temp.get(5).y == 6
                    && temp.get(6).x == 3 && temp.get(6).y == 6
                    && temp.get(7).x == 4 && temp.get(7).y == 6
                    && temp.get(8).x == 5 && temp.get(8).y == 6
                    && temp.get(9).x == 6 && temp.get(9).y == 6
                    && temp.get(10).x == 6 && temp.get(10).y == 5
                    && temp.get(11).x == 6 && temp.get(11).y == 4
                    && temp.get(12).x == 6 && temp.get(12).y == 3
                    && temp.get(13).x == 6 && temp.get(13).y == 2
                    && temp.get(14).x == 6 && temp.get(14).y == 1
                    && temp.get(15).x == 7 && temp.get(15).y == 1
                    && temp.get(16).x == 8 && temp.get(16).y == 1
                    && temp.get(17).x == 9 && temp.get(17).y == 1
                    && temp.get(18).x == 9 && temp.get(18).y == 0;
        } catch (Exception e) {
            checkIfPathIsCorrect = false;
        }
        assertEquals(true, checkIfPathIsCorrect);
        assertEquals(19, temp.getCount());
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                map[i][j] = false;
            }
        }
        temp = EnemyUnitPath.getPath(map);
        assertEquals(19, temp.getCount());
    }
}