package de.tolga.dunestowerdefense;

import com.badlogic.gdx.math.Vector2;


/**
 * inspired from https://happycoding.io/tutorials/libgdx/pathfinding#graphs-connections-and-heuristics
 */
public class Heuristic implements com.badlogic.gdx.ai.pfa.Heuristic<Node> {

    /**
     * Distance heuristic which is unnecessary.
     * */
    @Override
    public float estimate(Node currentNode, Node endNode) {
        return Vector2.dst(currentNode.x, currentNode.y, currentNode.x, currentNode.y);
    }
}
