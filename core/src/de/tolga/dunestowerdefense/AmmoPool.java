package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class AmmoPool {
    private ArrayList<Ammonation> ammoPoolGunTurret;
    private ArrayList<Ammonation> ammoPoolBombTower;
    private final Model gunTurretAmmoModel;
    private final Model bombTowerAmmoModel;
    private AssetManager assetManager;
    private final static Vector3 gunTurretAmmoScale = new Vector3(0.1f, 0.1f, 0.1f);
    private final static Vector3 bombTowerAmmoScale = new Vector3(1f, 1f, 1f);
    private ModelInstance ammoGunTurretModelInstance;
    private ModelInstance ammoBombTowerModelInstance;
    private Vector3 tempPosition = new Vector3();

    public AmmoPool(AssetManager assetManager){
        this.assetManager = assetManager;
        assetManager.get("crossbow_tower/arrow/crossbowtowerarrow.g3db", Model.class);
        gunTurretAmmoModel = assetManager.get("crossbow_tower/arrow/crossbowtowerarrow.g3db", Model.class);
        bombTowerAmmoModel = assetManager.get("cannonball/source/Cannonball.obj", Model.class);
        ammoPoolGunTurret = new ArrayList<Ammonation>(10000);
        ammoPoolBombTower = new ArrayList<Ammonation>(10000);
        for (int i = 0; i < 10000; i++) {
            ammoPoolGunTurret.add(i, new Ammonation(ammoGunTurretModelInstance = new ModelInstance(gunTurretAmmoModel), false));
        }
        for (int i = 0; i < 10000; i++) {
            ammoPoolBombTower.add(i, new Ammonation(ammoBombTowerModelInstance = new ModelInstance(bombTowerAmmoModel), false));
        }
    }

    /**
     * Creates an object of the class Ammunition and configures everything necessary.
     * Returns the configured ammunition.
     * Only works for gun turrets and bomb tower.
     * */
    public Ammonation shoot(EnemyUnit enemyUnit, Tower tower) {
        if (tower.getClass().equals(GunTurret.class)){
            for (int i = 0; i < ammoPoolGunTurret.size(); i++) {
                if (!ammoPoolGunTurret.get(i).isActive()){
                    ammoPoolGunTurret.get(i).setActive();
                    ammoPoolGunTurret.get(i).setTower(tower);
                    ammoPoolGunTurret.get(i).setTarget(enemyUnit);
                    tempPosition.set(tower.position.x, tower.position.y+50, tower.position.z);
                    ammoPoolGunTurret.get(i).setPosition(tempPosition);
                    return ammoPoolGunTurret.get(i);
                }
            }
        }
        else if (tower.getClass().equals(BombTower.class)) {
            for (int i = 0; i < ammoPoolBombTower.size(); i++) {
                if (!ammoPoolBombTower.get(i).isActive()){
                    ammoPoolBombTower.get(i).setActive();
                    ammoPoolBombTower.get(i).setTower(tower);
                    tempPosition.set(enemyUnit.getPosition().x, 0, enemyUnit.getPosition().z);
                    ammoPoolBombTower.get(i).setTarget(tempPosition);
                    tempPosition.set(tower.position.x, tower.position.y + 25, tower.position.z);
                    ammoPoolBombTower.get(i).setPosition(tempPosition);
                    return ammoPoolBombTower.get(i);
                }
            }
        }
        return null;
    }
}
