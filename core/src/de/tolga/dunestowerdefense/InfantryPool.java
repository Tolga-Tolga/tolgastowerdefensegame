package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class InfantryPool implements Disposable {
    private ArrayList<Infantry> infantries;
    private ArrayList<ModelInstance> infantryInstaces;
    private ArrayList<ModelInstance> activeInfantryInstaces;
    private ArrayList<Infantry> activeInfantries;
    private ArrayList<AnimationController> infantryAnimations;
    private ArrayList<Integer> healthpoints;
    private int n;
    private Portal start;
    private Portal end;
    private boolean[][] map;

    public InfantryPool(int n, Portal start, Portal end, boolean[][] map, AssetManager assetManager, ArrayList<Integer> healthpoints){
        infantries = new ArrayList<Infantry>(n);
        infantryInstaces = new ArrayList<ModelInstance>(n);
        activeInfantryInstaces = new ArrayList<ModelInstance>(n);
        activeInfantries = new ArrayList<Infantry>(n);
        infantryAnimations = new ArrayList<AnimationController>(n);
        this.n = n;
        this.start = start;
        this.end = end;
        this.map = map;
        this.healthpoints = healthpoints;
        afterWaveResetInfantryReset();
        loadInfantriesIntoPool(assetManager);
        loadInfantryInstancesIntoInstancePool();
        loadAnimationsOfInfantriesInThePool();
    }


    /**
     * Loads the model instances into the infantry instance arraylist.
     * */
    private void loadInfantryInstancesIntoInstancePool() {
        for (int i = 0; i < n; i++) {
            infantryInstaces.add(infantries.get(i).getInfantryInstance());
        }
    }


    /**
     * Loads the gun turret into the gun turret pool.
     * @param assetManager the given asset manager which is necessary for the infantry object.
     * */
    private void loadInfantriesIntoPool(AssetManager assetManager) {
        for (int i = 0; i < n; i++) {
            infantries.add(new Infantry(new Vector3(-10000000, -100, -10000000),
                    map,
                    start,
                    end,
                    assetManager,
                    healthpoints));
        }
    }


    /**
     * Loads the animations of the instances into the gun turret animation arraylist.
     * */
    private void loadAnimationsOfInfantriesInThePool(){
        for (int i = 0; i < n; i++) {
            AnimationController infantryAnimation = new AnimationController(infantries.get(i).getInfantryInstance());
            infantryAnimation.setAnimation(infantries.get(i).getInfantryInstance().animations.first().id, -1);
            infantryAnimations.add(infantryAnimation);
        }
    }


    /**
     * Returns the gun turret at the given index from the gun turret arraylist.
     * @param index the given index.
     * */
    public Infantry getInfantry(int index) {
        return infantries.get(index);
    }


    /**
     * Returns an unused infantry.
     * If every infantry is used the first used infantry gets returned.
     * */
    public Infantry getUnusedInfantry(){
        for (int i = 0; i < infantries.size(); i++) {
            if (!infantries.get(i).isUsed){
                return infantries.get(i);
            }
        }
        return infantries.get(0);
    }


    /**
     * Returns all active infantries.
     * */
    public ArrayList<Infantry> getActiveInfatries() {
        for (int i = 0; i < infantries.size(); i++) {
            if (infantries.get(i).isUsed){
                activeInfantries.add(infantries.get(i));
            }
        }
        return activeInfantries;
    }


    /**
     * Clears active infantries and their instances.
     * */
    public void afterWaveResetInfantryReset() {
        activeInfantries.clear();
        activeInfantryInstaces.clear();
    }


    /**
     * Returns all infantry instances.
     * */
    public ArrayList<ModelInstance> getInfantryInstaces(){
        return infantryInstaces;
    }


    /**
     * Returns all active infantry instances.
     * */
    public ArrayList<ModelInstance> getActiveInfantryInstaces(){
        return activeInfantryInstaces;
    }


    /**
     * Returns an arraylist which contains every infantry.
     * */
    public ArrayList<Infantry> getInfantries(){
        return infantries;
    }


    /**
     * Returns all infantry instance animations.
     * */
    public ArrayList<AnimationController> getInfantryAnimations() {
        return infantryAnimations;
    }


    /**
     * Updates the infantry animations with the given delta time.
     * @param delta the given delta time.
     * */
    public void updateAnimations(float delta) {
        for (int i = 0; i < n; i++) {
            infantryAnimations.get(i).update(delta);
        }
    }


    @Override
    public void dispose() {
        for(Infantry infantry : infantries){
            infantry.dispose();
        }
    }
}
