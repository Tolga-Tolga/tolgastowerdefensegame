package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class Harvester extends EnemyUnit implements Disposable {

    private Model harvesterModel;
    private AssetManager asset;
    private ModelInstance harvesterInstance;
    private static final int health = 100;
    public final static Vector3 harvesterScale = new Vector3(1f,1f,1f);

    public Harvester(Vector3 position, boolean[][] map, Portal start, Portal end, AssetManager assetManager, ArrayList<Integer> healthpoints) {
        super(position, 1.0f, map, start, end, healthpoints, health);
        harvesterModel = assetManager.get("harvester/source/harvester.g3db", Model.class);
        harvesterInstance = new ModelInstance(harvesterModel);
        harvesterInstance.transform.set(position, GameScreen.emptyQuaternion, harvesterScale);
    }


    /**
     * Sets the position at the given vector values.
     * @param x the given x vector
     * @param y the given y(2d)/z(3d) vector
     * */
    public void setPosition(float x, float y) {
        this.position = position.set(x, 25, y);
        harvesterInstance.transform.set(position, GameScreen.emptyQuaternion, harvesterScale);
    }


    /**
     * Returns the model instance.
     * */
    public ModelInstance getHarvesterInstance() {
        return harvesterInstance;
    }


    /**
     * Overrides the setSlowed function of enemy unit, because harvester can`t be slowed.
     * */
    public void setSlowed() {

    }


    /**
     * Overrides the setUnslowed function of enemy unit, because harvester can`t be speed up.
     * */
    public void setUnslowed() {

    }


    public void dispose() {
        super.dispose();
        harvesterModel.dispose();
        asset.dispose();
    }
}
