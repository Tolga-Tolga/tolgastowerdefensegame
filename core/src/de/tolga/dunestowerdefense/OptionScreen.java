package de.tolga.dunestowerdefense;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;


public class OptionScreen extends ScreenAdapter {
    DunesTowerDefense dunesTowerDefense;
    Music music;

    //controller
    Vector2 cursorController;
    Array<Controller> everyController;
    Controller controller;

    //stage&skin
    Skin mySkin;
    Stage stage;
    Camera camera;
    protected Slider volume;
    protected SelectBox resolution;
    protected TextButton fullscreen;

    //background
    protected static Texture BACKGROUND_TEXTURE;
    protected static Sprite BACKGROUND_SPRITE;
    protected SpriteBatch spriteBatch;

    //clock
    protected static int clock;

    protected static int MAX_VELOCITY;

    public OptionScreen() {

    }

    public OptionScreen(DunesTowerDefense dunesTowerDefense){
        this.dunesTowerDefense = dunesTowerDefense;
        cursorController = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        everyController = Controllers.getControllers();
        controller = everyController.first();

        mySkin = new Skin(Gdx.files.internal("skins/craftacular/skin/craftacular-ui.json"));
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        clock = 0;

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        volume = new Slider(0f, 100f, 1f,false, mySkin);
        //resolution = new SelectBox(mySkin);
        fullscreen = new TextButton("Exit Game", mySkin);

        volume.setSize(Gdx.graphics.getWidth()*0.5f, Gdx.graphics.getHeight()*0.1f);
        //resolution.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        fullscreen.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);

        volume.setPosition(Gdx.graphics.getWidth()*0.125f,Gdx.graphics.getHeight()*0.85f);
        //resolution.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.60f);
        fullscreen.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.10f);


        Group group = new Group();
        group.addActor(volume);
        //group.addActor(resolution);
        group.addActor(fullscreen);
        stage.addActor(group);

        BACKGROUND_TEXTURE = new Texture("Dirt_Background.png");
        BACKGROUND_SPRITE = new Sprite(BACKGROUND_TEXTURE);
        spriteBatch = new SpriteBatch();
        music = dunesTowerDefense.getMusic();
        music.play();

    }


    /**
     * Function which gets called every millisecond and updates everything which needs to be update every tick.
     * @param delta the delta time.
     * */
    public void render(float delta) {
        clock++;
        if(clock == 20){
            cursorController.x = Gdx.input.getX();
            cursorController.y = Gdx.input.getY();
            clock = 0;
        }

        updateVolume();
        changeFullscreen();
        background();

        stage.act();
        stage.draw();
        input(delta);
        System.out.println(dunesTowerDefense.getMusic().getPosition());
    }


    /**
     * Changes the window to full screen mode.
     * */
    private void changeFullscreen() {
        if (fullscreen.isPressed()||(fullscreen.isOver() && controller.getButton(ControllerButton.A))){
            Gdx.app.exit();
        }
    }


    /**
     * Updates the volume.
     * */
    private void updateVolume(){

    }


    /**
     * Reads input of mouse, keyboard and gamepad.
     * @param delta the delta time.
     * */
    private void input(float delta){
        if(controller.isConnected()){
            Gdx.input.setCursorCatched(false);
            if(controller.getAxis(1) >= 0.05f || controller.getAxis(1) <= -0.05f){
                cursorController.y += MAX_VELOCITY * delta * controller.getAxis(1);
                Gdx.input.setCursorPosition((int) cursorController.x, (int) cursorController.y);
            }
            if(controller.getAxis(0) >= 0.05f || controller.getAxis(0) <= -0.05f){
                cursorController.x += MAX_VELOCITY * delta * controller.getAxis(0);
                Gdx.input.setCursorPosition((int) cursorController.x, (int) cursorController.y);
            }
        }
    }


    /**
     * Creates the background wallpaper.
     * */
    private void background(){
        spriteBatch.begin();
        BACKGROUND_SPRITE.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        BACKGROUND_SPRITE.draw(spriteBatch);
        spriteBatch.end();
    }
}
