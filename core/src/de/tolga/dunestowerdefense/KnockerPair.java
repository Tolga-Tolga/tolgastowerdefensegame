package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.Vector3;

public class KnockerPair {
    private Knocker knocker1;
    private Knocker knocker2;
    private AnimationController knocker1Animation;
    private AnimationController knocker2Animation;
    private AssetManager assetManager;
    private boolean[][] map;
    private boolean knocker1Active = false;
    private boolean knocker2Active = false;
    private Vector3 tempVec3 = new Vector3();
    boolean knocker1Renderable = false;
    boolean knocker2Renderable = false;
    int a;
    int b;

    public KnockerPair(AssetManager assetManager, boolean[][] map) {
        this.assetManager = assetManager;
        this.map = map;
        createKnockers();
        loadKnockerAnimations();
    }


    /**
     * Creates the knockers.
     * */
    public void createKnockers() {
        knocker1 = new Knocker(assetManager, map);
        knocker2 = new Knocker(assetManager, map);
    }


    /**
     * Loads the knocker animations.
     * */
    public void loadKnockerAnimations() {
        knocker1Animation = new AnimationController(knocker1.getKnockerModelInstance());
        knocker1Animation.setAnimation(knocker1.getKnockerModelInstance().animations.first().id, -1);
        knocker2Animation = new AnimationController(knocker2.getKnockerModelInstance());
        knocker2Animation.setAnimation(knocker2.getKnockerModelInstance().animations.first().id, -1);
    }


    /**
     * Returns the first knocker.
     * */
    public Knocker getKnocker1() {
        return knocker1;
    }


    /**
     * Returns the second knocker.
     * */
    public Knocker getKnocker2() {
        return knocker2;
    }


    /**
     * Sets the first knocker active.
     * */
    public void setKnocker1Active() {
        knocker1Active = true;
    }


    /**
     * Sets the second knocker active.
     * */
    public void setKnocker2Active() {
        knocker2Active = true;
    }


    /**
     * Sets the first knocker inactive.
     * */
    public void setKnocker1Inactive() {
        knocker1Active = false;
    }


    /**
     * Sets the second knocker inactive.
     * */
    public void setKnocker2Inactive() {
        knocker2Active = false;
    }


    /**
     * Returns true if the first knocker is active.
     * */
    public boolean isKnocker1Active() {
        return knocker1Active;
    }


    /**
     * Returns true if the second knocker is active.
     * */
    public boolean isKnocker2Active() {
        return knocker2Active;
    }


    /**
     * Sets the first knocker renderable.
     * */
    public void setKnocker1Renderable() {
        knocker1Renderable = true;
    }


    /**
     * Sets the second knocker renderable.
     * */
    public void setKnocker2Renderable() {
        knocker2Renderable = true;
    }


    /**
     * Sets the first knocker not renderable.
     * */
    public void setKnocker1NotRenderable() {
        knocker1Renderable = false;
    }


    /**
     * Sets the second knocker not renderable.
     * */
    public void setKnocker2NotRenderable() {
        knocker2Renderable = false;
    }


    /**
     * Returns true if the first knocker is renderable.
     * */
    public boolean isKnocker1Renderable() {
        return knocker1Renderable;
    }


    /**
     * Returns true if the second knocker is renderable.
     * */
    public boolean isKnocker2Renderable() {
        return knocker2Renderable;
    }


    /**
     * Sets the position of the first knocker at the given vector values.
     * @param x the given x vector.
     * @param y the given (3d:z/2d:y) vector
     * */
    public void setKnocker1Position(float x, float y) {
        knocker1.setPosition(x,y);
    }


    /**
     * Sets the position of the second knocker at the given vector values.
     * @param x the given x vector.
     * @param y the given (3d:z/2d:y) vector
     * */
    public boolean setKnocker2Position(float x, float y) {
        if (isKnocker2PositionValid(x,y)) {
            knocker2.setPosition(x,y);
            return true;
        }
        return false;
    }


    /**
     * Returns the model instance of the first knocker.
     * */
    public ModelInstance getKnocker1ModelInstance() {
        return knocker1.getKnockerModelInstance();
    }


    /**
     * Returns the model instance of the second knocker.
     * */
    public ModelInstance getKnocker2ModelInstance() {
        return knocker2.getKnockerModelInstance();
    }


    /**
     * Updates the knocker animations with the given delta time.
     * @param delta the given delta time.
     * */
    public void updateKnockerAnimations(float delta) {
        knocker1Animation.update(delta);
        knocker2Animation.update(delta);
    }


    /**
     * Checks if the position of the 2nd Knocker is valid.
     * @param x the given x vector.
     * @param y the given (3d:z/2d:y) vector
     * */
    private boolean isKnocker2PositionValid(float x, float y) {
        a = (int) x/100;
        b = (int) y/100;
        a *= 100;
        b *= 100;
        if (x > 0){
            a += 50;
        }
        else {
            a -= 50;
        }
        if (y > 0){
            b += 50;
        }
        else {
            b -= 50;
        }
        return a == knocker1.getPosition().x || b == knocker1.getPosition().z;
    }


    /**
     * Returns true if both knockers are active.
     * */
    public boolean bothKnockerActive() {
        return knocker1Active && knocker2Active;
    }


    /**
     * Calculates the path of the shai hulud and saves them in the path pair.
     * */
    public void getPath(Pair<Vector3, Vector3> path) {
        path.clear();
        int x1 = (int) knocker1.getPosition().x;
        int x2 = (int) knocker2.getPosition().x;
        int z1 = (int) knocker1.getPosition().z;
        int z2 = (int) knocker2.getPosition().z;
        if (knocker1.getPosition().x > 0){
            x1 += 50;
        }
        else {
            x1 -= 50;
        }
        if (knocker2.getPosition().x > 0){
            x2 += 50;
        }
        else {
            x2 -= 50;
        }
        if (knocker1.getPosition().z > 0){
            z1 += 50;
        }
        else {
            z1 -= 50;
        }
        if (knocker2.getPosition().z > 0){
            z2 += 50;
        }
        else {
            z2 -= 50;
        }
        x1 /= 100;
        x2 /= 100;
        z1 /= 100;
        z2 /= 100;

        x1 += 5;
        x2 += 5;
        z1 += 5;
        z2 += 5;

        if (x1 == x2) {
            path.setFirst(new Vector3(knocker1.getPosition().x, 10, -450));
            path.setSecond(new Vector3(knocker1.getPosition().x, 10, 450));
            /*
            for (int i = 0; i < 10; i++) {
                int z = i - 5;
                z *= 100;
                z += 50;
                path.add(new Vector3(knocker1.getPosition().x, 0, z));
            }
            */
        }
        else if (z1 == z2) {
            path.setFirst(new Vector3(-450, 10, knocker1.getPosition().z));
            path.setSecond(new Vector3(450, 10, knocker1.getPosition().z));
        }
    }


    /**
     * Sets the knocker pair inactive and not renderable.
     * */
    public void reset() {
        setKnocker1Inactive();
        setKnocker1NotRenderable();
        setKnocker2Inactive();
        setKnocker2NotRenderable();
    }
}
