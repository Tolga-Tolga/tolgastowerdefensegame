package de.tolga.dunestowerdefense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalShadowLight;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * @author Tolga Oezyuerek
 * A little tower defense game.
 * */

public class GameScreen extends ScreenAdapter {
    //controller
    Array<Controller> everyController;
    Controller controller;
    Vector2 cursorController;
    static int clock;
    private boolean controllerClickedOnPause = false;

    //movement
    private final static float MAX_VELOCITY_CONTROLLER = 100.0f;

    //stage&skin
    Skin mySkin;

    //camera
    private PerspectiveCamera camera;
    private static int FOV;
    private static float CAMERA_HEIGHT;
    private static float VIEW_DISTANCE;
    private static float ZOOM;
    final private static Vector3 LOOKAT = new Vector3(0, 0, 0);
    final private static Vector3 XAXIS = Vector3.X;
    final private static Vector3 ZAXIS = Vector3.Z;
    Ray ray;
    static private Vector3 mouseRayOnGround;

    //model
    private ModelBatch modelBatch;
    private ModelBuilder modelBuilder;
    private ModelInstance modelInstance;
    private ModelInstance modelInstance2;
    private ModelInstance modelInstanceTower;
    private Environment environment;
    private Model ground;
    private Model groundMesh;
    private Model tower;
    private Model crossbowtower;
    private Model crossbowtowerarrow;
    private AssetManager assets = new AssetManager();
    private Array<ModelInstance> instances = new Array<ModelInstance>();
    private boolean loading;
    private static BoundingBox boundingBox;
    private final static float GROUND_DEPTH = 1000f;
    private final static float GROUND_WIDTH = 1000f;
    static private Vector3 towerPosition;
    public final static Quaternion emptyQuaternion = new Quaternion();
    static private Vector3 crossbowturretScale;

    //groundtiles
    private ArrayList<ModelInstance> groundTilesDirt = new ArrayList<ModelInstance>();
    private ArrayList<ModelInstance> groundTilesGrass = new ArrayList<ModelInstance>();
    private ArrayList<ModelInstance> activeGroundTilesDirt = new ArrayList<ModelInstance>();
    private ArrayList<ModelInstance> activeGroundTilesGrass = new ArrayList<ModelInstance>();

    //cubemap
    SkyBox skyBox;

    //tower
    private Tower[][] towers = new Tower[10][10];

    //gunTurret
    GunTurret gunTurret = new GunTurret(new Vector3(10000000000f, 100000000000f, 10000000000f));

    //bombTower
    BombTower bombTower;

    private boolean grabGunTurret = false;
    private int [][] blockShooting = new int[10][10];
    private Tower tempTower;

    //bombTower
    private boolean grabBombTower = false;

    //acousticTower
    private boolean grabAcousticTower = false;

    //shai hulud
    private boolean grabKnocker = false;

    //ammoPool
    AmmoPool ammoPool;

    //2D UI
    private Stage stage;
    private Window window;
    private final float priceGunTurret = GunTurret.cost;
    private final float priceBombTower = BombTower.cost;
    private final float priceAcousticTower = AcousticTower.cost;
    private short shaiHuludCount = 1;
    private int spiceAmount = 0;
    TextButton buyGunTurret;
    TextButton buyBombTower;
    TextButton buyAcousticTower;
    TextButton shaiHuludButton;
    TextButton spiceAmountVisual;
    TextButton healthButton;
    TextButton scoreButton;
    Slider flex;

    //2D UI Pause
    private Stage pauseStage;
    private final String resumeGameS = "Resume";
    private final String optionsS = "Options";
    private final String backToMainMenuS = "Main Menu";
    private final String exitGameS = "Exit";
    TextButton resumeGameT;
    TextButton optionsT;
    TextButton backToMainMenuT;
    TextButton exitGameT;
    private final static Texture BACKGROUND_TEXTURE = new Texture("Dirt_Background.png");
    private final static Texture GAMEWON_TEXTURE = new Texture("Game_Won.png");
    private final static Texture GAMELOST_TEXTURE = new Texture("Game_Lost.png");
    private final static Sprite BACKGROUND_SPRITE = new Sprite(BACKGROUND_TEXTURE);
    private final static Sprite BACKGROUND_SPRITE_GAMEWON = new Sprite(GAMEWON_TEXTURE);
    private final static Sprite BACKGROUND_SPRITE_GAMELOST = new Sprite(GAMELOST_TEXTURE);
    SpriteBatch spriteBatch;

    //pause game
    private boolean pause = false;

    //2D UI game over
    private Stage gameOverStage;
    private final String newGameS = "Start new Game";
    private TextButton newGameT;
    private TextButton optionsTgo;
    private TextButton backToMainmenuTgo;
    private TextButton exitGameTgo;

    //game over
    private boolean gameOver = false;

    //cursor
    Vector2 rayPositon;
    private boolean rayOnPlayboard = false;
    private boolean mouseClickedOnUIField = false;
    private boolean controllerClickedOnUIField = false;

    //Pools
    private InfantryPool infantryPool;
    private BossUnitPool bossUnitPool;
    private HarvesterPool harvesterPool;
    private GunTurretTowerPool gunTurretTowerPool;
    private ArrayList<GunTurret> activeGunTurretTowerPool;
    private ArrayList<BombTower> activeBombTowerPool;
    private BombTowerPool bombTowerPool;
    private AcousticTowerPool acousticTowerPool;

    //gamelogic
    private int healthPoints = 100;
    //values 0 = health, 1 = score, 2 = money
    private ArrayList<Integer> healthPointScoreAndMoneyValues = new ArrayList<Integer>(2);
    private int money = 200;
    //126
    private int score = 0;
    private boolean gameWon = false;
    private boolean gameField[][] = new boolean[10][10];
    private short towerField[][] = new short[10][10];
    private StartPortal startPortal;
    private EndPortal endPortal;
    private ShaiHulud shaiHulud;
    private KnockerPair knockerPair;

    //animation
    AnimationController animationControllerStartPortal;
    AnimationController animationControllerEndPortal;
    AnimationController infantryAnimation;
    AnimationController gunTurretAnimation;


    //light
    DirectionalShadowLight directionalShadowLight;
    ModelBatch shadowBatch;

    //ground
    ModelInstance groundInstances[][];
    ArrayList<ModelInstance> groundInstancesArrayList;
    Vector3 groundVector = new Vector3();
    Vector3 modelGroundScale = new Vector3(100,20,100);

    //shooting
    int shootingtimer = 1000;
    int gunTurretShootingTimer;
    int bombTowerShootingTimer;
    int shootingClockGunTurret = 0;
    int shootingClockBombTower = 0;
    private ArrayList<Ammonation> shots;
    private Vector3 shootDirectionVector = new Vector3();
    private Vector3 enemyPosition = new Vector3();
    private Vector3 bulletPosition = new Vector3();
    private Vector2 directionVectorShoot = new Vector2();
    private Quaternion shootDirection = new Quaternion();

    //portal
    private ArrayList<ModelInstance> portalInstances = new ArrayList<ModelInstance>();

    //movement
    Vector3 tempMovement = new Vector3();
    Quaternion tempNextNodeDirection = new Quaternion();
    Vector2 enemyVectorPosition = new Vector2();
    Vector2 nextNodePosition = new Vector2();
    private Infantry tempInfantry;
    private BossUnit tempBossunit;
    private Harvester tempHarvester;
    ArrayList<Infantry> activeInfantries = new ArrayList<Infantry>();

    //wave
    private Wave wave;
    private ArrayList<EnemyUnit> waveUnits;
    private int waveClock = 0;
    private EnemyUnit tempEnemyUnit;
    private Vector2 tempEnemyUnitPosition = new Vector2();
    private boolean waveOngoing = false;
    private boolean waveCleared = false;
    private int inactiveTimer = 0;
    private int knockerPairDespawnTimer = 500;
    private boolean shaiHuludActive = false;

    //waveui
    com.badlogic.gdx.scenes.scene2d.ui.Label label;
    Stage waveStage;

    //shai hulud
    private Pair<Vector3, Vector3> shaiHuludPath = new Pair<Vector3, Vector3>();
    private Vector3 shaiHuludPosition = new Vector3();

    //Dunes Tower Defense Instance
    private DunesTowerDefense dunesTowerDefense;

    //fast game up
    boolean fastMode = false;

    //testharvester
    private Harvester testharvester;

    //temp
    private ModelInstance tempInstance;

    //sounds
    private Sound backgroundSound;
    private Sound buySound;

    public GameScreen(DunesTowerDefense dunesTdInstance){
        dunesTowerDefense = dunesTdInstance;
        createController();
        createCamera();
        createModels();
        loadGunTurretsIntoTowerPool();
        loadBombTowersIntoTowerPool();
        loadAcousticTowersIntoTowerPool();
        createSkybox();
        createLight();
        createUi();
        createPauseUi();
        createGameOverUi();
        loadSounds();
        placePortals();
        loadInfantries();
        loadBossUnits();
        loadHarvester();
        loadBlockShootingArrays();
        loadAmmonation();
        loadShaiHulud();
        loadKnocker();
        createWave();
        loadGroundTiles();
        createGroundmodels();
        loadWaveIntoTowerPools();
        rayPositon = new Vector2();
        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField[0].length; j++) {
                gameField[i][j] = false;
            }
        }
        //portalgamefieldcoords
        Model tempmodel = assets.get("cannonball/source/Cannonball.obj", Model.class);
    }

    /**
     * Function which gets called every millisecond and updates everything which needs to be update every tick.
     * @param delta the delta time.
     * */
    public void render(float delta) {
        if (pause){
            Gdx.input.setInputProcessor(pauseStage);
            pauseInput(delta);
            spriteBatch.begin();
            BACKGROUND_SPRITE.draw(spriteBatch);
            spriteBatch.end();
            pauseStage.act();
            pauseStage.draw();
            if (controllerClickedOnPause && !controller.getButton(ControllerButton.START)){
                controllerClickedOnPause = false;
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE) || Gdx.input.isKeyJustPressed(Input.Keys.P) || controller.getButton(ControllerButton.START) && !controllerClickedOnPause){
                pause = false;
                controllerClickedOnPause = true;
                Gdx.input.setInputProcessor(stage);
            }
        }
        else if (gameWon) {
            gameOverInput(delta);
            Gdx.input.setInputProcessor(gameOverStage);
            spriteBatch.begin();
            BACKGROUND_SPRITE_GAMEWON.draw(spriteBatch);
            spriteBatch.end();
            gameOverStage.act();
            gameOverStage.draw();
        }
        else if (gameOver) {
            gameOverInput(delta);
            Gdx.input.setInputProcessor(gameOverStage);
            spriteBatch.begin();
            BACKGROUND_SPRITE_GAMELOST.draw(spriteBatch);
            spriteBatch.end();
            gameOverStage.act();
            gameOverStage.draw();
        }
        else {
            //if (fastMode) delta *= 2;
            updateUi();

            Gdx.gl.glClearColor(0,0,0,1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT|GL20.GL_DEPTH_BUFFER_BIT);

            buyGunTurret();
            buyBombTower();
            buyAcousticTower();
            placeKnocker();

            acousticTowerCalculations();
            //camera.near = 0.01f;
            skyBox.render(camera);
            //camera.near = 100f;
            modelBatch.begin(camera);
            //modelBatch.render(instances, environment);
            modelBatch.render(groundInstancesArrayList, environment);
            modelBatch.render(portalInstances, environment);
            modelBatch.render(gunTurretTowerPool.getActiveGunTurretInstances());
            //System.out.println(gunTurretTowerPool.getActiveGunTurretInstances().size());
            //modelBatch.render(gunTurretTowerPool.getGunTurretInstances(), environment);
            //modelBatch.render(infantryPool.getInfantryInstaces(), environment);
            modelBatch.render(wave.getUnitInstances(), environment);
            modelBatch.render(bombTowerPool.getActiveBombTowerInstances(), environment);
            modelBatch.render(acousticTowerPool.getActiveAcousticTowerInstances(), environment);
            modelBatch.render(activeGroundTilesDirt, environment);
            modelBatch.render(activeGroundTilesGrass, environment);
            if (shots!=null) {
                for (int i = 0; i < shots.size(); i++) {
                    if (shots.get(i).isActive()) {
                        modelBatch.render(shots.get(i).getModelInstance(), environment);
                    }
                }
            }
            if (knockerPair.isKnocker1Renderable()) {
                modelBatch.render(knockerPair.getKnocker1ModelInstance(), environment);
            }
            if (knockerPair.isKnocker2Renderable()) {
                modelBatch.render(knockerPair.getKnocker2ModelInstance(), environment);
            }
            if (shaiHulud.isShaiHuludActive()) {
                modelBatch.render(shaiHulud.getModelInstance(), environment);
            }
            modelBatch.end();
            stage.act();
            stage.draw();
            if (waveStage != null) {
                waveStage.act();
                waveStage.draw();
            }

            input(delta);
            rayCoordinatesGround();
            setMouseRayTo2dVector();
            isGameOver();
            isGameWon();
            updateEnemyUnitAnimations(delta);
            updateKnockerAnimations(delta);
            updateShaiHuludAnimation(delta);
            moveCharacters(delta);
            shootAtEnemies(delta);
            waveManager();
            checkForShaiHulud();
            shaiHuludMovement(delta);
            if (shaiHulud.isShaiHuludActive()) {
                shaiHulud.checkIfEnemiesAreInRangeAndKillThem(wave);
                shaiHulud.checkIfTowerAreInRangeAndDeleteThem(bombTowerPool);
                shaiHulud.checkIfTowerAreInRangeAndDeleteThem(acousticTowerPool);
                shaiHulud.checkIfTowerAreInRangeAndDeleteThem(gunTurretTowerPool);
            }
        }
    }


    /**
     * Function which places portals on the matchfield.
     * x1 and y1 can be declared between -4 and 4
     * */
    private void placePortals() {
        short x1 = -4;
        short y1 = 4;
        x1 *= 100;
        y1 *= 100;
        if (x1 > 0){
            x1 += 50;
        }
        else {
            x1 -= 50;
        }
        if (y1 > 0){
            y1 += 50;
        }
        else {
            y1 -= 50;
        }
        short x2 = 4;
        short y2 = -4;
        x2 *= 100;
        y2 *= 100;
        if (x2 > 0){
            x2 += 50;
        }
        else {
            x2 -= 50;
        }
        if (y2 > 0){
            y2 += 50;
        }
        else {
            y2 -= 50;
        }

        startPortal = new StartPortal(new Vector3(x1,12,y1));
        endPortal = new EndPortal(new Vector3(x2,12,y2));
        portalInstances.add(startPortal.getPortalInstance());
        portalInstances.add(endPortal.getPortalInstance());
    }


    /**
     * If called it starts the first wave and initializes a new Object of the class Wave.
     * Needs to be called before "startNextWave()".
     * */
    private void createWave() {
        wave = new Wave(infantryPool, bossUnitPool, harvesterPool, startPortal, endPortal, ammoPool, healthPointScoreAndMoneyValues, 1);
        createWaveUI();
        shots = wave.getShots();
    }

    private void createWaveUI() {
        waveStage = new Stage(new ScreenViewport());
        label = new com.badlogic.gdx.scenes.scene2d.ui.Label(String.valueOf(wave.getRound()), mySkin);
        label.setSize(Gdx.graphics.getWidth()*0.01f, Gdx.graphics.getHeight()*0.01f);
        label.setPosition(Gdx.graphics.getWidth()*0.01f, Gdx.graphics.getHeight()*0.01f);
        waveStage.addActor(label);
    }

    private void updateWaveUI() {
        label.setText(String.valueOf(wave.getRound()));
    }

    /**
    * Loads the wave data into the towerpool.
    * */
    private void loadWaveIntoTowerPools() {
        acousticTowerPool.loadWaveintoTowerPool(wave);
    }

    /**
     * Starts the next Wave. Can not be called as a first wave.
     * */
    private void startNextWave() {
        wave.startNextRound();
        waveUnits = wave.getUnits();
        updateWaveUI();
    }

    /**
     * Player can buy a gun turret with this function.
     * This function needs to be called every render tick.
     * It checks if the player pressed the ui button and waits for the release of it before you can buy a gun turret.
     * If the player presses on the buy gun turret button (left click or A on XBOX controller) and doesnt want to realize the purchase
     * then he/she can cancel the purchase with right click or B on the XBOX controller.
     * Covers mouse and controller interactions.
     * Can only be bought if the wave isn`t ongoing.
     * */
    private void buyGunTurret() {
        if ((buyGunTurret.isOver()) && (healthPointScoreAndMoneyValues.get(2) >= gunTurret.getCost()) && (Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)) && !waveOngoing){
            grabGunTurret = true;
            mouseClickedOnUIField = true;
        }
        if ((buyGunTurret.isOver()) && (healthPointScoreAndMoneyValues.get(2) >= gunTurret.getCost()) && controller.getButton(ControllerButton.A) && !waveOngoing){
            grabGunTurret = true;
            controllerClickedOnUIField = true;
            System.out.println(controller.getButton(ControllerButton.A));
        }
        if (mouseClickedOnUIField && !Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)){
            mouseClickedOnUIField = false;
        }
        if (controllerClickedOnUIField && !controller.getButton(ControllerButton.A)){
            controllerClickedOnUIField = false;
        }
        if (grabGunTurret){
            if (!gunTurretTowerPool.isGunTurretInstanceActive(gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance())){
                gunTurretTowerPool.addActiveGunTurretInstance(gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance());
            }
            gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance().transform.set(mouseRayOnGround.x, 0, mouseRayOnGround.z,
                    0,0,0,0,
                    gunTurret.getGunTurretScale().x, gunTurret.getGunTurretScale().y, gunTurret.getGunTurretScale().z);
            if ((Gdx.input.isButtonPressed(MouseEvent.BUTTON1)||controller.getButton(ControllerButton.B))&&!mouseClickedOnUIField&&!controllerClickedOnUIField){
                gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance().transform.set(1000000,1000000, 1000000, 0,0,0,0);
                gunTurretTowerPool.deleteActiveGunTurretInstace(gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance());
                grabGunTurret = false;
            }
            if ((Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)||controller.getButton(ControllerButton.A)) && !gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] && !mouseClickedOnUIField && !controllerClickedOnUIField){
                gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance().transform.set(1000000,1000000, 1000000, 0,0,0,0);
                gunTurretTowerPool.deleteActiveGunTurretInstace(gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance());
                if (towerPositionValid(getSecondCoordinateOnGround(), getFirstCoordinateOnGround())) {
                    GunTurret buyedGunTurret = gunTurretTowerPool.getUnusedTower();
                    buyedGunTurret.setPosition(rayPositon.x, rayPositon.y);
                    buyedGunTurret.getGunTurretModelInstance().transform.set(rayPositon.x, 0, rayPositon.y,
                            0,0,0, 0,
                            gunTurret.getGunTurretScale().x, gunTurret.getGunTurretScale().y, gunTurret.getGunTurretScale().z);
                    //instances.get(4).transform.set(rayPositon.x, 50, rayPositon.y,
                    //        0,0,0, 0,
                    //        gunTurret.getGunTurretScale().x, gunTurret.getGunTurretScale().y, gunTurret.getGunTurretScale().z);
                    gunTurretTowerPool.addActiveGunTurretInstance(buyedGunTurret.getGunTurretModelInstance());
                    System.out.println(getFirstCoordinateOnGround() + " || " + getSecondCoordinateOnGround());
                    gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = true;
                    towerField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = 1;
                    towers[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = buyedGunTurret;
                    healthPointScoreAndMoneyValues.set(2, healthPointScoreAndMoneyValues.get(2) - gunTurret.getCost());
                    grabGunTurret = false;
                    updateGround();
                    long id = buySound.play();
                    buySound.setVolume(id, 0.06f);
                }
                else {
                    gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance().transform.set(1000000,1000000, 1000000, 0,0,0,0);
                    gunTurretTowerPool.deleteActiveGunTurretInstace(gunTurretTowerPool.getUnusedTower().getGunTurretModelInstance());
                    grabGunTurret = false;
                }
            }
        }
    }

    /**
     * Player can buy a bomb tower with this function.
     * This function needs to be called every render tick.
     * It checks if the player pressed the ui button and waits for the release of it before you can buy a bomb tower.
     * If the player presses on the buy bomb tower button (left click or A on XBOX controller) and doesnt want to realize the purchase
     * then he/she can cancel the purchase with right click or B on the XBOX controller.
     * Covers mouse and controller interactions.
     * Can only be bought if the wave isn`t ongoing.
     * */
    private void buyBombTower() {
        if ((buyBombTower.isOver()) && (healthPointScoreAndMoneyValues.get(2) >= BombTower.getTowerCost()) && (Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)) && !waveOngoing){
            grabBombTower = true;
            mouseClickedOnUIField = true;
        }
        if ((buyBombTower.isOver()) && (healthPointScoreAndMoneyValues.get(2) >= BombTower.getTowerCost()) && controller.getButton(ControllerButton.A) && !waveOngoing){
            grabBombTower = true;
            controllerClickedOnUIField = true;
            //System.out.println(controller.getButton(ControllerButton.A));
        }
        if (mouseClickedOnUIField && !Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)){
            mouseClickedOnUIField = false;
        }
        if (controllerClickedOnUIField && !controller.getButton(ControllerButton.A)){
            controllerClickedOnUIField = false;
        }
        if (grabBombTower){
            if (!bombTowerPool.isBombTowerInstanceActive(bombTowerPool.getUnusedTower().getBombTowerModelInstance())){
                bombTowerPool.addActiveBombTowerInstance(bombTowerPool.getUnusedTower().getBombTowerModelInstance());
            }
            bombTowerPool.getUnusedTower().getBombTowerModelInstance().transform.set(mouseRayOnGround.x, 0, mouseRayOnGround.z,
                    0,0,0,0,
                    BombTower.bombTowerScale.x, BombTower.bombTowerScale.y, BombTower.bombTowerScale.z);
            if ((Gdx.input.isButtonPressed(MouseEvent.BUTTON1)||controller.getButton(ControllerButton.B))&&!mouseClickedOnUIField&&!controllerClickedOnUIField){
                bombTowerPool.getUnusedTower().getBombTowerModelInstance().transform.set(1000000,1000000, 1000000, 0,0,0,0);
                bombTowerPool.deleteActiveBombTowerInstance(bombTowerPool.getUnusedTower().getBombTowerModelInstance());
                grabBombTower = false;
            }
            if ((Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)||controller.getButton(ControllerButton.A)) && !gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] && !mouseClickedOnUIField && !controllerClickedOnUIField){
                if (towerPositionValid(getSecondCoordinateOnGround(), getFirstCoordinateOnGround())) {
                    BombTower buyedBombTower = bombTowerPool.getUnusedTower();
                    buyedBombTower.setPosition(rayPositon.x, rayPositon.y);
                    buyedBombTower.getBombTowerModelInstance().transform.set(rayPositon.x, 0, rayPositon.y,
                            0,0,0, 0,
                            BombTower.bombTowerScale.x, BombTower.bombTowerScale.y, BombTower.bombTowerScale.z);
                    //instances.get(4).transform.set(rayPositon.x, 50, rayPositon.y,
                    //        0,0,0, 0,
                    //        gunTurret.getGunTurretScale().x, gunTurret.getGunTurretScale().y, gunTurret.getGunTurretScale().z);
                    bombTowerPool.addActiveBombTowerInstance(buyedBombTower.getBombTowerModelInstance());
                    //System.out.println(getFirstCoordinateOnGround() + " || " + getSecondCoordinateOnGround());
                    gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = true;
                    towers[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = buyedBombTower;
                    towerField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = 2;
                    healthPointScoreAndMoneyValues.set(2, healthPointScoreAndMoneyValues.get(2) - bombTowerPool.getTower(0).getCost());
                    grabBombTower = false;
                    updateGround();
                    long id = buySound.play();
                    buySound.setVolume(id, 0.06f);
                }
                else {
                    bombTowerPool.getUnusedTower().getBombTowerModelInstance().transform.set(1000000,1000000, 1000000, 0,0,0,0);
                    bombTowerPool.deleteActiveBombTowerInstance(bombTowerPool.getUnusedTower().getBombTowerModelInstance());
                    grabBombTower = false;
                }
            }
        }
    }


    /**
     * Player can buy a acoustic tower with this function.
     * This function needs to be called every render tick.
     * It checks if the player pressed the ui button and waits for the release of it before you can buy a acoustic tower.
     * If the player presses on the buy acoustic tower button (left click or A on XBOX controller) and doesnt want to realize the purchase
     * then he/she can cancel the purchase with right click or B on the XBOX controller.
     * Covers mouse and controller interactions.
     * Can only be bought if the wave isn`t ongoing.
     * */
    private void buyAcousticTower() {
        if ((buyAcousticTower.isOver()) && (healthPointScoreAndMoneyValues.get(2) >= acousticTowerPool.getTower(0).getCost()) && (Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)) && !waveOngoing){
            grabAcousticTower = true;
            mouseClickedOnUIField = true;
        }
        if ((buyAcousticTower.isOver()) && (healthPointScoreAndMoneyValues.get(2) >= acousticTowerPool.getTower(0).getCost()) && controller.getButton(ControllerButton.A) && !waveOngoing){
            grabAcousticTower = true;
            controllerClickedOnUIField = true;
            //System.out.println(controller.getButton(ControllerButton.A));
        }
        if (mouseClickedOnUIField && !Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)){
            mouseClickedOnUIField = false;
        }
        if (controllerClickedOnUIField && !controller.getButton(ControllerButton.A)){
            controllerClickedOnUIField = false;
        }
        if (grabAcousticTower){
            if (!acousticTowerPool.isAcousticTowerInstanceActive(acousticTowerPool.getUnusedTower().getAcousticTowerInstance())){
                acousticTowerPool.addActiveAcousticTowerInstance(acousticTowerPool.getUnusedTower().getAcousticTowerInstance());
            }
            acousticTowerPool.getUnusedTower().getAcousticTowerInstance().transform.set(mouseRayOnGround.x, 0, mouseRayOnGround.z,
                    0,0,0,0,
                    AcousticTower.acousticTowerScale.x, AcousticTower.acousticTowerScale.y, AcousticTower.acousticTowerScale.z);
            if ((Gdx.input.isButtonPressed(MouseEvent.BUTTON1)||controller.getButton(ControllerButton.B))&&!mouseClickedOnUIField&&!controllerClickedOnUIField){
                acousticTowerPool.getUnusedTower().getAcousticTowerInstance().transform.set(1000000,1000000, 1000000, 0,0,0,0);
                acousticTowerPool.deleteActiveAcousticTowerInstace(acousticTowerPool.getUnusedTower().getAcousticTowerInstance());
                grabAcousticTower = false;
            }
            if ((Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)||controller.getButton(ControllerButton.A)) && !gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] && !mouseClickedOnUIField && !controllerClickedOnUIField){
                if (towerPositionValid(getSecondCoordinateOnGround(), getFirstCoordinateOnGround())) {
                    AcousticTower buyedAcousticTower = acousticTowerPool.getUnusedTower();
                    buyedAcousticTower.setPosition(rayPositon.x, rayPositon.y);
                    buyedAcousticTower.getAcousticTowerInstance().transform.set(rayPositon.x, 25, rayPositon.y,
                            0,0,0, 0,
                            AcousticTower.acousticTowerScale.x, AcousticTower.acousticTowerScale.y, AcousticTower.acousticTowerScale.z);
                    //instances.get(4).transform.set(rayPositon.x, 50, rayPositon.y,
                    //        0,0,0, 0,
                    //        gunTurret.getGunTurretScale().x, gunTurret.getGunTurretScale().y, gunTurret.getGunTurretScale().z);
                    acousticTowerPool.addActiveAcousticTowerInstance(buyedAcousticTower.getAcousticTowerInstance());
                    //System.out.println(getFirstCoordinateOnGround() + " || " + getSecondCoordinateOnGround());
                    gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = true;
                    towerField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = 3;
                    towers[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] = buyedAcousticTower;
                    healthPointScoreAndMoneyValues.set(2, healthPointScoreAndMoneyValues.get(2) - acousticTowerPool.getTower(0).getCost());
                    //money -= acousticTowerPool.getTower(0).getCost();
                    grabAcousticTower = false;
                    updateGround();
                    long id = buySound.play();
                    buySound.setVolume(id, 0.06f);
                }
                else {
                    acousticTowerPool.getUnusedTower().getAcousticTowerInstance().transform.set(1000000,1000000, 1000000, 0,0,0,0);
                    acousticTowerPool.deleteActiveAcousticTowerInstace(acousticTowerPool.getUnusedTower().getAcousticTowerInstance());
                    grabAcousticTower = false;
                }
            }
        }
    }


    /**
     * Player can place a knocker with this function.
     * This function needs to be called every render tick.
     * It checks if the player pressed the ui button and waits for the release of it before you can buy a acoustic tower.
     * If the player presses on the buy acoustic tower button (left click or A on XBOX controller) and doesnt want to realize the purchase
     * then he/she can cancel the purchase with right click or B on the XBOX controller.
     * Covers mouse and controller interactions.
     * Can only be used midwave.
     * */
    private void placeKnocker() {
        if ((shaiHuludButton.isOver()) && (Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)) && waveOngoing && !shaiHuludActive){
            grabKnocker = true;
            mouseClickedOnUIField = true;
        }
        if ((shaiHuludButton.isOver()) && controller.getButton(ControllerButton.A) && waveOngoing && !shaiHuludActive){
            grabKnocker = true;
            controllerClickedOnUIField = true;
            //System.out.println(controller.getButton(ControllerButton.A));
        }
        if (mouseClickedOnUIField && !Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)){
            mouseClickedOnUIField = false;
        }
        if (controllerClickedOnUIField && !controller.getButton(ControllerButton.A)){
            controllerClickedOnUIField = false;
        }
        if (grabKnocker){
            try {
                if (!knockerPair.isKnocker1Active()) {
                    knockerPair.setKnocker1Renderable();
                    knockerPair.getKnocker1ModelInstance().transform.set(mouseRayOnGround.x, 0, mouseRayOnGround.z,
                            0,0,0,0,
                            Knocker.knockerScale.x, Knocker.knockerScale.y, Knocker.knockerScale.z);
                    if ((Gdx.input.isButtonPressed(MouseEvent.BUTTON1)||controller.getButton(ControllerButton.B))&&!mouseClickedOnUIField&&!controllerClickedOnUIField){
                        knockerPair.setKnocker1Inactive();
                        knockerPair.setKnocker1NotRenderable();
                        grabKnocker = false;
                    }
                    if ((Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)||controller.getButton(ControllerButton.A)) && !gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] && !mouseClickedOnUIField && !controllerClickedOnUIField){
                        knockerPair.setKnocker1Active();
                        knockerPair.setKnocker1Position(rayPositon.x, rayPositon.y);
                        knockerPair.getKnocker1ModelInstance().transform.set(rayPositon.x, 0, rayPositon.y,
                                0,0,0, 0,
                                Knocker.knockerScale.x, Knocker.knockerScale.y, Knocker.knockerScale.z);
                        //System.out.println(getFirstCoordinateOnGround() + " || " + getSecondCoordinateOnGround());
                        grabKnocker = false;
                    }
                }
                else if (!knockerPair.isKnocker2Active()) {
                    knockerPair.setKnocker2Renderable();
                    knockerPair.getKnocker2ModelInstance().transform.set(mouseRayOnGround.x, 0, mouseRayOnGround.z,
                            0,0,0,0,
                            Knocker.knockerScale.x, Knocker.knockerScale.y, Knocker.knockerScale.z);
                    if ((Gdx.input.isButtonPressed(MouseEvent.BUTTON1)||controller.getButton(ControllerButton.B))&&!mouseClickedOnUIField&&!controllerClickedOnUIField){
                        knockerPair.setKnocker2Inactive();
                        grabKnocker = false;
                        knockerPair.setKnocker2NotRenderable();
                    }
                    if ((Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)||controller.getButton(ControllerButton.A)) && !gameField[getSecondCoordinateOnGround()][getFirstCoordinateOnGround()] && !mouseClickedOnUIField && !controllerClickedOnUIField){
                        boolean isKnockerValid = knockerPair.setKnocker2Position(rayPositon.x, rayPositon.y);
                        if (isKnockerValid) {
                            knockerPair.setKnocker2Active();
                            knockerPair.getKnocker2ModelInstance().transform.set(rayPositon.x, 0, rayPositon.y,
                                    0,0,0, 0,
                                    Knocker.knockerScale.x, Knocker.knockerScale.y, Knocker.knockerScale.z);
                            //System.out.println(getFirstCoordinateOnGround() + " || " + getSecondCoordinateOnGround());
                        }
                        else {
                            knockerPair.setKnocker2Inactive();
                            knockerPair.setKnocker2NotRenderable();
                        }
                        grabKnocker = false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                grabKnocker = false;
            }
        }
    }


    /**
     * Checks every render tick if the game is lost.
     * If the hp drops to 0 hp or lower this function calls the endGame() function.
     * */
    private void isGameOver() {
        if(healthPointScoreAndMoneyValues.get(0) <= 0){
            sendHighscore();
            endGame();
        }
    }


    /**
     * Checks every render tick if the game is won.
     * If the player survives the last wave this function calls the endGame() function with the game won parameter.
     * */
    private void isGameWon() {
        if(wave.getRound() == 97 && waveCleared){
            gameWon = true;
            sendHighscore();
            endGame();
        }
    }

    private void sendHighscore() {

    }


    /**
     * Gets called after the game is over.
     * Changes the boolean value gameOver to true.
     * */
    private void endGame() {
        gameOver = true;
    }


    /**
     * Creates a simple UI with which the player can interact with the game.
     * Needs to be used to buy and sell turrets and to place the knocker.
     * The UI is a Window with buttons which can be pressed with the mouse or with the gamepad.
     * */
    private void createUi() {
        mySkin = new Skin(Gdx.files.internal("skins/craftacular/skin/craftacular-ui.json"));
        stage = new Stage(new ScreenViewport());

        healthPointScoreAndMoneyValues.add(healthPoints);
        healthPointScoreAndMoneyValues.add(score);
        healthPointScoreAndMoneyValues.add(money);

        window = new Window("HUD", mySkin);
        window.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight()*0.3f);
        window.setPosition(Gdx.graphics.getWidth() * 0.875f, 0);
        buyGunTurret = new TextButton("gun turret: " + priceGunTurret, mySkin);
        buyBombTower = new TextButton("bomb tower: " + buyBombTower, mySkin);
        buyAcousticTower = new TextButton("acoustic tower: " + buyAcousticTower, mySkin);
        shaiHuludButton = new TextButton("knocker count: " + shaiHuludCount, mySkin);
        spiceAmountVisual = new TextButton("spice: " + spiceAmount, mySkin);
        healthButton = new TextButton("health: " + healthPointScoreAndMoneyValues.get(0), mySkin);
        scoreButton = new TextButton("score: " + healthPointScoreAndMoneyValues.get(1), mySkin);
        //flex = new Slider(0f, 100f, 0.1f, false, mySkin);
        buyGunTurret.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight() * 0.05f);
        buyBombTower.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight() * 0.05f);
        buyAcousticTower.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight() * 0.05f);
        shaiHuludButton.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight() * 0.05f);
        healthButton.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight() * 0.05f);
        //flex.setSize(Gdx.graphics.getWidth() * 0.25f, Gdx.graphics.getHeight() * 0.1f);
        spiceAmountVisual.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight() * 0.05f);
        scoreButton.setSize(Gdx.graphics.getWidth() * 0.175f, Gdx.graphics.getHeight() * 0.05f);
        buyGunTurret.setPosition(0,0);
        buyBombTower.setPosition(0, window.getHeight() * 0.142857143f);
        buyAcousticTower.setPosition(0, window.getHeight() * 0.285714286f);
        shaiHuludButton.setPosition(0, window.getHeight() * 0.428571429f);
        //flex.setPosition(0, window.getHeight() * 0.8f);
        healthButton.setPosition(0, window.getHeight() * 0.571428572f);
        spiceAmountVisual.setPosition(0, window.getHeight() * 0.714285715f);
        scoreButton.setPosition(0, window.getHeight() * 0.8571422858f);
        window.addActor(buyGunTurret);
        window.addActor(buyBombTower);
        window.addActor(buyAcousticTower);
        window.addActor(shaiHuludButton);
        window.addActor(healthButton);
        //window.addActor(flex);
        window.addActor(spiceAmountVisual);
        window.addActor(scoreButton);
        stage.addActor(window);
        Gdx.input.setInputProcessor(stage);
    }


    /**
     * A Pause UI which opens if the player is pausing the game.
     * The UI is a Window with buttons which can be pressed with the mouse or with the gamepad.
     * */
    private void createPauseUi() {
        spriteBatch = new SpriteBatch();
        pauseStage = new Stage(new ScreenViewport());

        resumeGameT = new TextButton(resumeGameS, mySkin);
        optionsT = new TextButton(optionsS, mySkin);
        backToMainMenuT = new TextButton(backToMainMenuS, mySkin);
        exitGameT = new TextButton(exitGameS, mySkin);

        resumeGameT.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        optionsT.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        backToMainMenuT.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        exitGameT.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);

        resumeGameT.setPosition(Gdx.graphics.getWidth()*0.125f,Gdx.graphics.getHeight()*0.775f);
        optionsT.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.55f);
        backToMainMenuT.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.325f);
        exitGameT.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.1f);

        Group group = new Group();
        group.addActor(resumeGameT);
        group.addActor(optionsT);
        group.addActor(backToMainMenuT);
        group.addActor(exitGameT);

        BACKGROUND_SPRITE.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        pauseStage.addActor(group);
    }


    /**
     * A Game Over UI which opens after the game is over.
     * The UI is a Window with buttons which can be pressed with the mouse or with the gamepad.
     * */
    private void createGameOverUi() {
        gameOverStage = new Stage(new ScreenViewport());

        newGameT = new TextButton(newGameS, mySkin);
        optionsTgo = new TextButton(optionsS, mySkin);
        backToMainmenuTgo = new TextButton(backToMainMenuS, mySkin);
        exitGameTgo = new TextButton(exitGameS, mySkin);

        newGameT.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        optionsTgo.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        backToMainmenuTgo.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        exitGameTgo.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);

        newGameT.setPosition(Gdx.graphics.getWidth()*0.125f,Gdx.graphics.getHeight()*0.775f);
        optionsTgo.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.55f);
        backToMainmenuTgo.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.325f);
        exitGameTgo.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.1f);

        Group group = new Group();
        group.addActor(newGameT);
        group.addActor(optionsTgo);
        group.addActor(backToMainmenuTgo);
        group.addActor(exitGameTgo);

        BACKGROUND_SPRITE.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        BACKGROUND_SPRITE_GAMELOST.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        BACKGROUND_SPRITE_GAMEWON.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        gameOverStage.addActor(group);
    }


    /**
     * Loads the game music and the buy sound.
     * */
    public void loadSounds() {
        backgroundSound = Gdx.audio.newSound( Gdx.files.internal("itty-bitty-8-bit-kevin-macleod-main-version-03-13-7983.mp3"));
        long id = backgroundSound.play();
        backgroundSound.setLooping(id, true);
        backgroundSound.setVolume(id, 0.03f);
        buySound = Gdx.audio.newSound(Gdx.files.internal("sfx/moneybag/338260__philsavlem__money-bag.wav"));
    }

    /**
     * Needs to be called every render tick and updates the UI information.
     * This needs to be called to visualize the current amount of spice which the player holds.
     * */
    private void updateUi(){
        spiceAmountVisual.setText("spice amount: " + healthPointScoreAndMoneyValues.get(2));
        buyGunTurret.setText("gun turret: " + priceGunTurret);
        buyBombTower.setText("bomb tower: " + priceBombTower);
        buyAcousticTower.setText("acoustic tower: " + priceAcousticTower);
        shaiHuludButton.setText("knocker count: " + shaiHuludCount);
        healthButton.setText("health: " + healthPointScoreAndMoneyValues.get(0));
        scoreButton.setText("score: " + healthPointScoreAndMoneyValues.get(1));
        //System.out.println(healtPointsValues.get(0));
    }

    /**
     * This function creates models which are simple enough for the ModelBuilder to cover them.
     * These 2 models that are getting generated are a box which is the actual playground and a grid
     * which shows all fields that can be used for tower/portals.
     * These models wont get rendered because the grid isn`t needed anymore and the box is used as the main
     * bounding box.
     * */
    private void createModels() {
        modelBatch = new ModelBatch();
        modelBuilder = new ModelBuilder();
        ground = modelBuilder.createBox(GROUND_WIDTH, 1, GROUND_DEPTH,
                new Material(ColorAttribute.createDiffuse(Color.WHITE)),
                VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
        loading = true;
        groundMesh = modelBuilder.createLineGrid(10, 10, GROUND_WIDTH/10.0f, GROUND_DEPTH/10.0f,
                new Material(ColorAttribute.createDiffuse(Color.BLACK)),
                VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
        tower = modelBuilder.createLineGrid(1, 1, GROUND_WIDTH/10.0f, GROUND_DEPTH/10.0f,
                new Material(ColorAttribute.createDiffuse(Color.GREEN)),
                VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
        towerPosition = new Vector3();
        modelInstance = new ModelInstance(ground, 0,0,0);
        modelInstance2 = new ModelInstance(groundMesh, 0,2,0);
        modelInstanceTower = new ModelInstance(tower, 1000000, 10, 1000000);
        mouseRayOnGround = new Vector3();
        boundingBox = new BoundingBox();
        instances.add(modelInstance, modelInstance2, modelInstanceTower);
    }

    /**
     * If called a new skybox gets generated which can be bound to the player camera and rendered with.
     * */
    private void createSkybox(){
        skyBox = new SkyBox(new Pixmap(Gdx.files.internal("cubemap/skybox.png")));
    }


    /**
     * Creates the ground which gets rendered.
     * The ground is only visual and not game relevant.
     * */
    private void createGroundmodels() {
        assets.load("tdassets/Models/OBJformat/tile.obj", Model.class);
        assets.load("tdassets/Models/OBJformat/tile_dirt.obj", Model.class);
        assets.finishLoading();
        groundInstances = new ModelInstance[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                tempInstance = getUnusedGroundTileGrass();
                tempInstance.transform.set(new Vector3(((i-5)*100) + 50, 0.1f, ((j-5)*100 + 50)), emptyQuaternion, modelGroundScale);
                activeGroundTilesGrass.add(tempInstance);
                groundInstances[i][j] = tempInstance;
            }
        }

    }

    /**
     * If called the function searchs for fields which are used from tower and changes
     * the ground beneath them to dirt.
     * */
    private void updateGround(){
        activeGroundTilesGrass.clear();
        activeGroundTilesDirt.clear();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                groundInstances[i][j] = null;
            }
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (gameField[j][i]) {
                    tempInstance = getUnusedGroundTileDirt();
                    tempInstance.transform.set(groundVector.set(((i - 5) * 100) + 50, 0.1f, ((j - 5) * 100 + 50)), emptyQuaternion, modelGroundScale);
                    activeGroundTilesDirt.add(tempInstance);
                    groundInstances[j][i] = tempInstance;
                }
                else {
                    tempInstance = getUnusedGroundTileGrass();
                    tempInstance.transform.set(groundVector.set(((i - 5) * 100) + 50, 0.1f, ((j - 5) * 100 + 50)), emptyQuaternion, modelGroundScale);
                    activeGroundTilesGrass.add(tempInstance);
                    groundInstances[j][i] = tempInstance;
                }
            }
        }
    }


    /**
     * Loads ground tiles into the groundInstancesArrayList, groundTilesGrass and the groundTilesDirt array lists.
     * */
    private void loadGroundTiles() {
        groundInstancesArrayList = new ArrayList<ModelInstance>();
        assets.load("tdassets/Models/OBJformat/tile.obj", Model.class);
        assets.load("tdassets/Models/OBJformat/tile_dirt.obj", Model.class);
        assets.finishLoading();
        for (int i = 0; i < 100; i++) {
            Model groundTile = assets.get("tdassets/Models/OBJformat/tile.obj", Model.class);
            groundTilesGrass.add(new ModelInstance(groundTile));
            groundTile = assets.get("tdassets/Models/OBJformat/tile_dirt.obj", Model.class);
            groundTilesDirt.add(new ModelInstance(groundTile));
        }
    }


    /**
     * Returns an unused dirt ground tile.
     * */
    private ModelInstance getUnusedGroundTileDirt() {
        for (int i = 0; i < groundTilesDirt.size(); i++) {
            if (!activeGroundTilesDirt.contains(groundTilesDirt.get(i))) {
                activeGroundTilesDirt.add(groundTilesDirt.get(i));
                return groundTilesDirt.get(i);
            }
        }
        return groundTilesDirt.get(0);
    }


    /**
     * Returns an unused grass ground tile.
     * */
    private ModelInstance getUnusedGroundTileGrass() {
        for (int i = 0; i < groundTilesGrass.size(); i++) {
            System.out.println(groundTilesGrass.size());
            if (!activeGroundTilesGrass.contains(groundTilesGrass.get(i))) {
                activeGroundTilesGrass.add(groundTilesGrass.get(i));
                return groundTilesGrass.get(i);
            }
        }
        return groundTilesGrass.get(0);
    }


    /**
     * Creates a light source.
     * Needs to be created to get shadows.
     * */
    private void createLight() {
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.01f, 0.01f, 0.01f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        directionalShadowLight = new DirectionalShadowLight(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 60, 60, 0.1f, 100);
        environment.add(directionalShadowLight);
        directionalShadowLight.set(1f,1f,1f,40f, -35f,-35f);
    }

    /**
     * Creates the player camera.
     * */
    private void createCamera() {
        FOV = 10;
        VIEW_DISTANCE = 100000.0f;
        CAMERA_HEIGHT = 6000.0f;
        ZOOM = 100.0f;
        camera = new PerspectiveCamera(FOV, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, CAMERA_HEIGHT, 0f);
        camera.lookAt(LOOKAT);
        camera.near = 100f;
        camera.far = VIEW_DISTANCE;
    }

    /**
     * Function which let the programm detect a gamepad and saves it to register its inputs.
     * */
    private void createController() {
        everyController = Controllers.getControllers();
        try {
            controller = everyController.first();
        }catch (Exception e) {
            controller = new EmptyController();
        }
        cursorController = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        clock = 0;
    }



    /**
     * Checks if enemies are in the range of the acoustic tower.
     * */
    private void acousticTowerCalculations() {
        acousticTowerPool.checkIfEnemyUnitsAreInTowerzone();
    }


    /**
     * Loads gun turrets into the tower pool.
     * */
    private void loadGunTurretsIntoTowerPool() {
        assets.load("crossbow_tower/crossbowtower.g3db", Model.class);
        assets.load("crossbow_tower/arrow/crossbowtowerarrow.g3db", Model.class);
        assets.finishLoading();
        gunTurretTowerPool = new GunTurretTowerPool(100, assets);
        activeGunTurretTowerPool = gunTurretTowerPool.getActiveGunTurretTowerPool();
    }


    /**
     * Loads bomb tower into the tower pool.
     * */
    private void loadBombTowersIntoTowerPool() {
        assets.load("tdassets/Models/OBJformat/weapon_cannon.obj", Model.class);
        assets.finishLoading();
        bombTowerPool = new BombTowerPool(100, assets);
        activeBombTowerPool = bombTowerPool.getActiveBombTower();
    }


    /**
     * Loads acoustic tower into the tower pool.
     * */
    private void loadAcousticTowersIntoTowerPool() {
        assets.load("acousticTower/source/jbl.obj", Model.class);
        assets.finishLoading();
        acousticTowerPool = new AcousticTowerPool(100, assets);
    }


    /**
     * Loads infantries into the infantry pool.
     * */
    private void loadInfantries() {
        assets.load("infantry/infantry.g3db", Model.class);
        assets.finishLoading();
        infantryPool = new InfantryPool(10000, startPortal, endPortal, gameField, assets, healthPointScoreAndMoneyValues);
    }


    /**
     * Loads bosses into the boss pool.
     * */
    private void loadBossUnits() {
        assets.load("bossunit/source/bossunitModel.g3db", Model.class);
        assets.finishLoading();
        bossUnitPool = new BossUnitPool(100, startPortal, endPortal, gameField, assets, healthPointScoreAndMoneyValues);
    }


    /**
     * Loads bosses into the boss pool.
     * */
    private void loadHarvester() {
        assets.load("harvester/source/harvester.g3db", Model.class);
        assets.finishLoading();
        harvesterPool = new HarvesterPool(1000, startPortal, endPortal, gameField, assets, healthPointScoreAndMoneyValues);
    }


    /**
     * Fills the 2 dimensional array blockshooting with the value 0.
     * If blockshooting[x][y] = 0 then the tower on this field can shoot.
     * */
    private void loadBlockShootingArrays() {
        for (int i = 0; i < blockShooting.length; i++) {
            Arrays.fill(blockShooting[i], 0);
        }
    }


    /**
     * Loads the ammonation model and creates the ammo pool.
     * */
    private void loadAmmonation() {
        assets.load("crossbow_tower/arrow/crossbowtowerarrow.g3db", Model.class);
        assets.load("cannonball/source/Cannonball.obj", Model.class);
        assets.finishLoading();
        ammoPool = new AmmoPool(assets);
    }


    /**
     * Loads the shai hulud and his model.
     * */
    private void loadShaiHulud() {
        assets.load("shai-hulud/source/worm.g3db", Model.class);
        assets.finishLoading();
        shaiHulud = new ShaiHulud(assets, gameField, healthPointScoreAndMoneyValues, towers);
    }


    /**
     * Loads the knockers and their model.
     * */
    private void loadKnocker() {
        assets.load("Knocker/source/knocker.g3db", Model.class);
        assets.finishLoading();
        knockerPair = new KnockerPair(assets, gameField);
    }


    /**
     * Reads the collision coordinates between the mouse ray and the bounding box of the ground box
     * and simplifies them to 50 unit steps. This means that if the coordinates are -469 and 10 they
     * will get to -450 and 0 which can be easily transfered to a 10x10 array.
     * -450 is 0 and +450 is 9.
     * */
    private int setMouseRayTo2dVector() {
        rayPositon.x =(int) mouseRayOnGround.x / 100;
        rayPositon.y =(int) mouseRayOnGround.z / 100;
        rayPositon.x *= 100;
        rayPositon.y *= 100;
        if (mouseRayOnGround.x > 0){
            rayPositon.x += 50;
        }
        else {
            rayPositon.x -= 50;
        }
        if (mouseRayOnGround.z > 0){
            rayPositon.y += 50;
        }
        else {
            rayPositon.y -= 50;
        }

        //System.out.println(rayPositon.x + " , " + rayPositon.y);
        return 0;
    }


    /**
     * Input method for camera movement and to pause the game.
     * Reads input of mouse, keyboard and gamepad.
     * @param delta the delta time.
     * */
    private void input(float delta){
        //controller
        clock++;
        if (!controller.isConnected()) {
            if(clock == 1){
                cursorController.x = Gdx.input.getX();
                cursorController.y = Gdx.input.getY();
                clock = 0;
            }
        }
        if(controller.isConnected()){
            clock++;
            System.out.println("test");
            if(clock == 20){
                cursorController.x = Gdx.input.getX();
                cursorController.y = Gdx.input.getY();
                clock = 0;
            }
            Gdx.input.setCursorCatched(false);

            controllerToMouse(delta);
            if (controllerClickedOnPause && !controller.getButton(ControllerButton.START)){
                controllerClickedOnPause = false;
            }
            if(controller.getAxis(2) >= 0.05f || controller.getAxis(2) <= -0.05f){
                camera.rotateAround(LOOKAT, ZAXIS, 20 * controller.getAxis(2) * delta);
                camera.lookAt(LOOKAT);
                camera.update();
            }
            if(controller.getAxis(3) >= 0.05f || controller.getAxis(3) <= -0.05f){
                camera.rotateAround(LOOKAT, XAXIS, 20 * controller.getAxis(3) * delta);
                camera.lookAt(LOOKAT);
                camera.update();
            }
            if (controller.getButton(ControllerButton.RBMUMPER)){
                rotateAroundYAxisPositive(delta);
            }
            if (controller.getButton(ControllerButton.LBUMPER)){
                rotateAroundYAxisNegative(delta);
            }
            if (controller.getButton(ControllerButton.UP)){
                zoomIn(delta);
            }
            if (controller.getButton(ControllerButton.DOWN)){
                zoomOut(delta);
            }
            if (controller.getButton(ControllerButton.BACK)){
                resetCamera();
            }
            if (controller.getButton(ControllerButton.START)){
                if (!controllerClickedOnPause){
                    controllerClickedOnPause = true;
                    pauseGame();
                }
            }

        }
        //Keyboard & Mouse
        if (Gdx.input.isKeyPressed(Input.Keys.F)) {
            if (fastMode) {
                fastMode = false;
            }
            else if (!fastMode) {
                fastMode = true;
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.W)){
            camera.rotateAround(LOOKAT, XAXIS, 20 * delta);
            camera.lookAt(LOOKAT);
            camera.update();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)){
            camera.rotateAround(LOOKAT, XAXIS, -20 * delta);
            camera.lookAt(LOOKAT);
            camera.update();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A)){
            camera.rotateAround(LOOKAT, ZAXIS, -20 * delta);
            camera.lookAt(LOOKAT);
            camera.update();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)){
            camera.rotateAround(LOOKAT, ZAXIS, 20 * delta);
            camera.lookAt(LOOKAT);
            camera.update();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)){
            zoomIn(delta);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            zoomOut(delta);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            rotateAroundYAxisPositive(delta);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            rotateAroundYAxisNegative(delta);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.R)){
            resetCamera();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.P) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            pauseGame();
        }
        camera.normalizeUp();
        camera.update();
    }


    /**
     * Input method which gets called if the game is paused.
     * Reads input of mouse, keyboard and gamepad.
     * @param delta the delta time.
     * */
    private void pauseInput(float delta){
        //controller
        clock++;
        if(controller.isConnected()){
            clock++;
            if(clock == 20){
                cursorController.x = Gdx.input.getX();
                cursorController.y = Gdx.input.getY();
                clock = 0;
            }
            Gdx.input.setCursorCatched(false);
            controllerToMouse(delta);
            if (resumeGameT.isOver() && controller.getButton(ControllerButton.A)) {
                resumeGame();
            }
            if (optionsT.isOver() && controller.getButton(ControllerButton.A)) {
                openOptionsScreen(false);
            }
            if (backToMainMenuT.isOver() && controller.getButton(ControllerButton.A)) {
                backToMainMenu();
            }
            if (exitGameT.isOver() && controller.getButton(ControllerButton.A)) {
                exitGame();
            }
        }
        if (resumeGameT.isOver() && Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)) {
            resumeGame();
        }
        if (optionsT.isPressed()) {
            openOptionsScreen(false);
        }
        if (backToMainMenuT.isPressed()) {
            backToMainMenu();
        }
        if (exitGameT.isPressed()) {
            exitGame();
        }
    }


    /**
     * Input method which gets called if the game is over.
     * Reads input of mouse, keyboard and gamepad.
     * @param delta the delta time.
     * */
    private void gameOverInput(float delta){
        //controller
        clock++;
        if(controller.isConnected()){
            clock++;
            if(clock == 20){
                cursorController.x = Gdx.input.getX();
                cursorController.y = Gdx.input.getY();
                clock = 0;
            }
            Gdx.input.setCursorCatched(false);
            controllerToMouse(delta);
            if (newGameT.isOver() && controller.getButton(ControllerButton.A)) {
                startNewGame();
            }
            if (optionsTgo.isOver() && controller.getButton(ControllerButton.A)) {
                openOptionsScreen(true);
            }
            if (backToMainmenuTgo.isOver() && controller.getButton(ControllerButton.A)) {
                backToMainMenu();
            }
            if (exitGameTgo.isOver() && controller.getButton(ControllerButton.A)) {
                exitGame();
            }
        }
        if (newGameT.isOver() && Gdx.input.isButtonPressed(MouseEvent.NOBUTTON)) {
            startNewGame();
        }
        if (optionsTgo.isPressed()) {
            openOptionsScreen(true);
        }
        if (backToMainmenuTgo.isPressed()) {
            backToMainMenu();
        }
        if (exitGameTgo.isPressed()) {
            exitGame();
        }
    }


    /**
     * Calculates the collision coordinates between the mouse ray and the ground.
     * */
    private void rayCoordinatesGround() {

        ray = camera.getPickRay(cursorController.x,cursorController.y);
        System.out.println(ray.origin.x + " "+ ray.origin.z);
        rayOnPlayboard = Intersector.intersectRayBounds(ray, ground.calculateBoundingBox(boundingBox), mouseRayOnGround);
        //System.out.println(mouseRayOnGround.x + " "+ mouseRayOnGround.z);
    }


    /**
     * If called the game will pause
     * */
    private void pauseGame() {
        pause = true;
    }


    /**
     * Resets the position of the camera.
     * Won`t reset the rotation.
     * */
    private void resetCamera() {
        camera.position.set(0, CAMERA_HEIGHT, 0);
        camera.lookAt(LOOKAT);
        camera.update();
    }


    /**
     * Reads the controller cursor movements and overwrites the mouse coordinates.
     * @param delta the delta time.
     * */
    private void controllerToMouse(float delta) {
        if (!(controller instanceof EmptyController)) {
            if(controller.getAxis(1) >= 0.05f || controller.getAxis(1) <= -0.05f){
                cursorController.y += MAX_VELOCITY_CONTROLLER * delta * controller.getAxis(1)*20;
                Gdx.input.setCursorPosition((int) cursorController.x, (int) cursorController.y);
            }
            if(controller.getAxis(0) >= 0.05f || controller.getAxis(0) <= -0.05f){
                cursorController.x += MAX_VELOCITY_CONTROLLER * delta * controller.getAxis(0)*20;
                Gdx.input.setCursorPosition((int) cursorController.x, (int) cursorController.y);
            }
        }
    }


    /**
     * Rotates the camera around the Y axis.
     * Is a method which operates as long as the bound button is pressed.
     * @param delta the delta time.
     **/
    private void rotateAroundYAxisNegative(float delta) {
        camera.rotateAround(LOOKAT, Vector3.Y, 20 * delta * -1);
        camera.lookAt(LOOKAT);
        camera.update();
    }


    /**
     * Rotates the camera around the Y axis.
     * Is a method which operates as long as the bound button is pressed.
     * @param delta the delta time.
     **/
    private void rotateAroundYAxisPositive(float delta) {
        camera.rotateAround(LOOKAT, Vector3.Y, 20 * delta);
        camera.lookAt(LOOKAT);
        camera.update();
    }


    /**
     * Zooms the camera to the middle of the gamefield.
     * Is a method which operates as long as the bound button is pressed.
     * @param delta the delta time.
     **/
    private void zoomIn(float delta) {
        camera.position.add(camera.position.x * -1 * 0.8f * delta, camera.position.y * -1 * 0.8f * delta, camera.position.z * -1 * 0.8f * delta);
        camera.lookAt(LOOKAT);
        camera.update();
    }


    /**
     * Zooms the camera to the opposite of the middle of the gamefield.
     * Is a method which operates as long as the bound button is pressed.
     * @param delta the delta time.
     **/
    private void zoomOut(float delta) {
        camera.position.add(camera.position.x * 0.8f * delta, camera.position.y * 0.8f * delta, camera.position.z * 0.8f * delta);
        camera.lookAt(LOOKAT);
        camera.update();
    }


    /**
     * Returns the x coordinate of the collision point from the mouse ray and the gameground bounding box.
     * */
    private int getFirstCoordinateOnGround(){
        System.out.println((int) (((rayPositon.x/50)/2)+0.5f) +4);
        return (int) (((rayPositon.x/50)/2)+0.5f) +4;
    }


    /**
     * Returns the (2D:y / 3D:z) coordinate of the collision point from the mouse ray and the gameground bounding box.
     * */
    private int getSecondCoordinateOnGround(){
        System.out.println((int) (((rayPositon.y/50)/2)+0.5f) +4);
        return (int) (((rayPositon.y/50)/2)+0.5f) +4;
    }


    /**
     * Moves characters to their given next node Position.
     * @param delta the delta time.
     * */
    private void moveCharacters(float delta) {
        if (!(waveUnits == null || waveUnits.isEmpty())){
            for (int i = 0; i < waveUnits.size(); i++) {
                if (!waveUnits.get(i).hasNodes()) continue;
                inactiveTimer = 0;
                waveUnits.get(i).setDamagable();
                if (waveUnits.get(i).getClass().equals(Infantry.class)){
                    tempInfantry = (Infantry) waveUnits.get(i);
                    tempInfantry.checkIfEnemyReachedEndPortal();
                    tempInfantry.getPosition().add(tempInfantry.getMovement().x * tempInfantry.getVelocity() * delta, 0, tempInfantry.getMovement().z * tempInfantry.getVelocity() * delta);
                    tempInfantry.getInfantryInstance().transform.set(tempInfantry.getPosition(), tempInfantry.getNextNodeDirection(), Infantry.infantryScale);
                }
                else if (waveUnits.get(i).getClass().equals(BossUnit.class)) {
                    tempBossunit = (BossUnit) waveUnits.get(i);
                    tempBossunit.checkIfEnemyReachedEndPortal();
                    tempBossunit.getPosition().add(tempBossunit.getMovement().x * tempBossunit.getVelocity() * delta, 0, tempBossunit.getMovement().z * tempBossunit.getVelocity() * delta);
                    tempBossunit.getBossInstance().transform.set(tempBossunit.getPosition(), tempBossunit.getNextNodeDirection(), BossUnit.bossScale);
                }
                else if (waveUnits.get(i).getClass().equals(Harvester.class)) {
                    tempHarvester = (Harvester) waveUnits.get(i);
                    tempHarvester.checkIfEnemyReachedEndPortal();
                    tempHarvester.getPosition().add(tempHarvester.getMovement().x * tempHarvester.getVelocity() * delta, 0, tempHarvester.getMovement().z * tempHarvester.getVelocity() * delta);
                    tempHarvester.getHarvesterInstance().transform.set(tempHarvester.getPosition(), tempHarvester.getNextNodeDirection(), Harvester.harvesterScale);
                }
            }
        }
        if (!(waveUnits == null || waveUnits.isEmpty())) {
            for (int i = 0; i < waveUnits.size(); i++) {
                if (!waveUnits.get(i).isUsed){
                    wave.deleteUnitInstance(waveUnits.get(i));
                }
            }
        }
    }


    /**
     * Detects the enemies which are in the distance of each tower and shoots at them.
     * @param delta the delta time.
     * */
    private void shootAtEnemies(float delta) {
        wave.checkIfBulletsHit();
        for (int i = 0; i < blockShooting.length; i++) {
            for (int j = 0; j < blockShooting[i].length; j++) {
                if (towerField[i][j] == 1) {
                    blockShooting[i][j] -= GunTurret.getCadence();
                }
                if (towerField[i][j] == 2) {
                    blockShooting[i][j] -= BombTower.getCadence();
                }
            }
        }
        //checks if the enemies are in tower range and if the tower may shoot because of the blockshooting int.
        if (wave != null && shots != null) {
            for (int j = 0; j < activeGunTurretTowerPool.size(); j++) {
                tempTower = activeGunTurretTowerPool.get(j);
                for (int i = 0; i < wave.getUnits().size(); i++) {
                    if (wave.getUnits().get(i).getPosition() != null) {
                        tempEnemyUnit = wave.getUnits().get(i);
                        tempEnemyUnitPosition.set(tempEnemyUnit.getPosition().x, tempEnemyUnit.getPosition().z);
                        if (activeGunTurretTowerPool.get(j).targetInShootingRange(tempEnemyUnitPosition)) {
                            //System.out.println("success");
                            if (blockShooting[(int) (((tempTower.position.z/50)/2)+0.5f)+4][(int) (((tempTower.position.x/50)/2)+0.5f)+4] <= 0){
                                wave.shootAt(tempEnemyUnit, activeGunTurretTowerPool.get(j));
                                blockShooting [(int) (((tempTower.position.z/50)/2)+0.5f)+4][(int) (((tempTower.position.x/50)/2)+0.5f)+4] = 1000;
                                break;
                            }
                        }
                    }
                }
            }
            for (int j = 0; j < activeBombTowerPool.size(); j++) {
                tempTower = activeBombTowerPool.get(j);
                for (int i = 0; i < wave.getUnits().size(); i++) {
                    if (wave.getUnits().get(i).getPosition() != null) {
                        tempEnemyUnit = wave.getUnits().get(i);
                        tempEnemyUnitPosition.set(tempEnemyUnit.getPosition().x, tempEnemyUnit.getPosition().z);
                        if (activeBombTowerPool.get(j).targetInShootingRange(tempEnemyUnitPosition)) {
                            //System.out.println("success");
                            if (blockShooting[(int) (((tempTower.position.z/50)/2)+0.5f)+4][(int) (((tempTower.position.x/50)/2)+0.5f)+4] <= 0){
                                wave.shootAt(tempEnemyUnit, activeBombTowerPool.get(j));
                                blockShooting [(int) (((tempTower.position.z/50)/2)+0.5f)+4][(int) (((tempTower.position.x/50)/2)+0.5f)+4] = 1000;
                                break;
                            }
                        }
                    }
                }
            }
        }
        //iterates through every stored shot and checks if the ammonation can be disposed.
        if (wave != null && shots != null) {
            for (int i = 0; i < shots.size(); i++) {
                if (shots.get(i).isFollowingTarget()) {
                    shootDirectionVector.set(shots.get(i).getTarget().getPosition());
                    if ((int) (((shots.get(i).getPosition().x/50)/2)+0.5f)+4 < 0 || (int) (((shots.get(i).getPosition().z/50)/2)+0.5f)+4 < 0 || (int) (((shots.get(i).getPosition().x/50)/2)+0.5f)+4 > 9 || (int) (((shots.get(i).getPosition().z/50)/2)+0.5f)+4 > 9) {
                        shots.get(i).setInactive();
                        shots.remove(shots.get(i));
                    }
                    else {
                        wave.checkIfAmmoTargetIsDead(shots.get(i));
                    }
                }
            }
        }
        //iterates through every shot and moves them a bit to their target.
        if (shots != null) {
            for (int i = 0; i < shots.size(); i++) {
                if (shots.get(i).isFollowingTarget()) {
                    enemyPosition = shots.get(i).getTarget().position;
                    bulletPosition = shots.get(i).getPosition();
                    shootDirectionVector.set(enemyPosition.x - bulletPosition.x, enemyPosition.y - bulletPosition.y, enemyPosition.z - bulletPosition.z);
                    shootDirectionVector.nor();
                    directionVectorShoot.set(shootDirectionVector.x, shootDirectionVector.z);
                    float degree = directionVectorShoot.angleDeg() - 90;
                    bulletPosition.add(shootDirectionVector.x * shots.get(i).getVelocity() * delta * 200, shootDirectionVector.y * shots.get(i).getVelocity() * delta * 200, shootDirectionVector.z * shots.get(i).getVelocity() * delta * 200);
                    shots.get(i).getModelInstance().transform.set(bulletPosition, shootDirection.set(Vector3.Y,-degree), GunTurret.gunTurretScale);
                    if (shots.get(i).getTower().getClass().equals(GunTurret.class)) {
                        //gunTurret = (GunTurret) (towers[(int) (((shots.get(i).getTower().position.z/50)/2)+0.5f)+4][(int) (((shots.get(i).getTower().position.x/50)/2)+0.5f)+4]);
                        gunTurret = (GunTurret) shots.get(i).getTower();
                        gunTurret.getGunTurretModelInstance().transform.set(gunTurret.position, shootDirection.set(Vector3.Y, -degree), GunTurret.gunTurretScale);
                    }
                }
                else {
                    enemyPosition = shots.get(i).getBombTarget();
                    bulletPosition = shots.get(i).getPosition();
                    shootDirectionVector.set(enemyPosition.x - bulletPosition.x, enemyPosition.y - bulletPosition.y, enemyPosition.z - bulletPosition.z);
                    shootDirectionVector.nor();
                    directionVectorShoot.set(shootDirectionVector.x, shootDirectionVector.z);
                    float degree = directionVectorShoot.angleDeg() - 90;
                    bulletPosition.add(shootDirectionVector.x * shots.get(i).getVelocity() * delta * 200, shootDirectionVector.y * shots.get(i).getVelocity() * delta * 200, shootDirectionVector.z * shots.get(i).getVelocity() * delta * 200);
                    shots.get(i).getModelInstance().transform.set(bulletPosition, shootDirection.set(Vector3.Y,-degree), BombTower.bombScale);
                    if (shots.get(i).getTower().getClass().equals(BombTower.class)) {
                        //gunTurret = (GunTurret) (towers[(int) (((shots.get(i).getTower().position.z/50)/2)+0.5f)+4][(int) (((shots.get(i).getTower().position.x/50)/2)+0.5f)+4]);
                        bombTower = (BombTower) shots.get(i).getTower();
                        bombTower.getBombTowerModelInstance().transform.set(bombTower.position, shootDirection.set(Vector3.Y, -degree), BombTower.bombTowerScale);
                    }
                }
            }
        }
    }


    /**
     * Updates all enemyunit animations which are on the gamefield.
     * @param delta the delta time.
     * */
    private void updateEnemyUnitAnimations(float delta) {
        wave.updateAnimations(delta);
    }


    /**
     * Updates the knocker animations.
     * @param delta the delta time.
     * */
    private void updateKnockerAnimations(float delta) {
        knockerPair.updateKnockerAnimations(delta);
    }


    /**
     * Updates the shai hulud animation.
     * @param delta the delta time.
     * */
    private void updateShaiHuludAnimation(float delta) {
        shaiHulud.updateAnimation(delta);
    }


    /**
     * Manages the wave. Checks if the player wants to start a new wave,
     * if the wave is over or if a new unit can be spawned during a wave.
     * */
    private void waveManager() {
        if ((Gdx.input.isKeyJustPressed(Input.Keys.L)||controller.getButton(ControllerButton.Y)) && !waveOngoing){
            startNextWave();
            waveOngoing = true;
            waveClock = 0;
            inactiveTimer = 0;
        }
        if (waveOngoing && waveClock == 100 - wave.getRound() * 2  && !wave.waveOver() && !wave.queueEmpty()) {
            waveClock = 0;
            wave.spawnNextUnit();
        }
        if (waveOngoing && 100 - wave.getRound() * 2 < 0 && !wave.waveOver() && !wave.queueEmpty()) {
            waveClock = 0;
            wave.spawnNextUnit();
        }
        if (waveOngoing) {
            waveClock++;
        }
        if (waveOngoing && wave.waveOver() || waveOngoing && inactiveTimer == 2000) {
            waveCleared = true;
            waveOngoing = false;
            knockerPair.reset();
            shaiHuludActive = false;
            updateGround();
        }
        if (waveCleared) {
            //successUi
        }
        if (inactiveTimer == 4000) inactiveTimer = 0;
        inactiveTimer ++;
    }


    /**
     * Resumes the game.
     * */
    private void resumeGame() {
        pause = false;
        Gdx.input.setInputProcessor(stage);
    }


    /**
     * Opens the option screen.
     * @param startNewGame the given parameter to check if the player opens the option screen
     *                     during the game or if he opens the screen after he lost the game.
     * */
    private void openOptionsScreen(boolean startNewGame) {

    }


    /**
     * Changes the screen to the main menu screen.
     * */
    private void backToMainMenu() {
        dunesTowerDefense.changeScreen(0);
    }


    /**
     * Closes the game.
     * */
    private void exitGame() {
        Gdx.app.exit();
    }


    /**
     * Restarts the game.
     * */
    private void startNewGame() {
        dunesTowerDefense.changeScreen(new GameScreen(dunesTowerDefense));
    }


    /**
     * Checks if the tower which the player intends to buy is legally placed.
     * Return false if the enemy cant find any way to the goal anymore.
     * */
    private boolean towerPositionValid(int y, int x) {
        try {
            if (testharvester == null) {
                testharvester = new Harvester(new Vector3(startPortal.getPosition().x, 0 , startPortal.getPosition().z), gameField,startPortal,endPortal, assets, healthPointScoreAndMoneyValues);
            }
            gameField[y][x] = true;
            testharvester.setPosition(startPortal.getPosition().x, startPortal.getPosition().z);
            testharvester.findPath();
            System.out.println(testharvester.getPath().getCount());
            if (testharvester.getPath().getCount() == 0) {
                gameField[y][x] = false;
                return false;
            }
            else{
                gameField[y][x] = false;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * Checks if the shai hulud can spawn.
     * */
    private void checkForShaiHulud() {
        if (knockerPair.bothKnockerActive() && !shaiHuludActive) {
            shaiHulud.setShaiHuludActive();
            knockerPair.getPath(shaiHuludPath);
            knockerPair.reset();
            shaiHulud.setPosition(shaiHuludPath.getFirst());
            shaiHulud.setShaiHuludActive();
            shaiHuludActive = true;
        }
    }


    /**
     * If the shai hulud is active, the shai hulud gets moved to a given direction.
     * @param delta the given delta time.
     * */
    private void shaiHuludMovement(float delta) {
        if (shaiHulud.isShaiHuludActive()) {
            if ((int) (shaiHuludPath.getSecond().x/10) == (int) (shaiHuludPosition.x/10) && (int) (shaiHuludPath.getSecond().z/10) == (int) (shaiHuludPosition.z/10)) {
                shaiHuludPath.clear();
                shaiHulud.setShaiHuludInactive();
            }
            else {
                enemyPosition = shaiHuludPath.getSecond();
                shaiHuludPosition = shaiHulud.getPosition();
                shootDirectionVector.set(enemyPosition.x - shaiHuludPosition.x, enemyPosition.y - shaiHuludPosition.y, enemyPosition.z - shaiHuludPosition.z);
                shootDirectionVector.nor();
                directionVectorShoot.set(shootDirectionVector.x, shootDirectionVector.z);
                float degree = directionVectorShoot.angleDeg() - 90;
                shaiHuludPosition.add(shootDirectionVector.x * shaiHulud.getVelocity() * delta * 200, 0, shootDirectionVector.z * shaiHulud.getVelocity() * delta * 200);
                shaiHulud.getModelInstance().transform.set(shaiHuludPosition, shootDirection.set(Vector3.Y,-degree), ShaiHulud.shaiHuludScale);
                shaiHulud.updateDamageCircle();
            }
        }
    }


    /**
     * Called when this screen should release all resources.
     * */
    public void dispose(){
        super.dispose();
        assets.dispose();
        modelBatch.dispose();
        groundMesh.dispose();
        ground.dispose();
        stage.dispose();
        skyBox.dispose();
        gunTurretTowerPool.dispose();
        bombTowerPool.dispose();
        infantryPool.dispose();
        buySound.dispose();
        backgroundSound.dispose();
    }
}