package de.tolga.dunestowerdefense;

import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;


/**
 * inspired from https://happycoding.io/tutorials/libgdx/pathfinding#graphs-connections-and-heuristics
 */
public class DunesTdGraph implements IndexedGraph<Node> {
    Heuristic heuristic = new Heuristic();
    Array<Node> nodes = new Array<Node>();
    Array<Connection> connections = new Array<Connection>();

    ObjectMap<Node, Array<com.badlogic.gdx.ai.pfa.Connection<Node>>> connectionMap = new ObjectMap<Node, Array<com.badlogic.gdx.ai.pfa.Connection<Node>>>();

    private int lastNodeIndex = 0;


    /**
     * Adds a node to this graph.
     * @param node the given node.
     * */
    public void addNode(Node node) {
        node.index = lastNodeIndex;
        lastNodeIndex++;
        nodes.add(node);
    }


    /**
     * Creates a connection from one node to another node.
     * @param fromNode the given node which points at toNode.
     * @param toNode the given node which gets pointed at from fromNode.
     * */
    public void connectNodes(Node fromNode, Node toNode) {
        Connection connection = new Connection(fromNode, toNode);
        if (!connectionMap.containsKey(fromNode)) {
            connectionMap.put(fromNode, new Array<com.badlogic.gdx.ai.pfa.Connection<Node>>());
        }
        connectionMap.get(fromNode).add(connection);
        connections.add(connection);
    }


    /**
     * Calculates a path from the start node to the goal node with the A* algorithm.
     * Returns the calculated path.
     * */
    public GraphPath<Node> findPath(Node startNode, Node goalNode) {
        GraphPath<Node> nodePath = new DefaultGraphPath<Node>();
        new IndexedAStarPathFinder<>(this).searchNodePath(startNode, goalNode, heuristic, nodePath);
        return nodePath;
    }


    /**
     * Returns the index of the given node.
     * @param node the given node.
     * */
    public int getIndex(Node node) {
        return node.index;
    }


    /**
     * Returns the node count of this graph.
     * */
    public int getNodeCount() {
        return lastNodeIndex;
    }


    /**
     * Returns all connections of the given node in this graph.
     * @param fromNode the given node.
     * */
    public Array<com.badlogic.gdx.ai.pfa.Connection<Node>> getConnections(Node fromNode) {
        if (connectionMap.containsKey(fromNode)) {
            return connectionMap.get(fromNode);
        }
        return new Array<>(0);
    }
}
