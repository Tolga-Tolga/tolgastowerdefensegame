package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class BombTowerPool implements Disposable {
    private ArrayList<BombTower> bombTowerPool;
    private ArrayList<ModelInstance> bombTowerInstances;
    private ArrayList<BombTower> activeBombTower;
    private ArrayList<ModelInstance> activeBombTowerInstances;
    private final int n;


    public BombTowerPool(int n, AssetManager assetManager){
        bombTowerPool = new ArrayList<BombTower>(n);
        bombTowerInstances = new ArrayList<ModelInstance>(n);
        activeBombTowerInstances = new ArrayList<ModelInstance>(n);
        activeBombTower = new ArrayList<BombTower>(n);
        this.n = n;
        loadTurretsIntoPool(assetManager);
        loadBombTowerInstancesIntoInstancePool();
    }


    /**
     * Loads the tower into the bomb tower pool.
     * @param assetManager the given asset manager which is necessary for the BombTower object.
     * */
    private void loadTurretsIntoPool(AssetManager assetManager) {
        for (int i = 0; i < n; i++) {
            bombTowerPool.add(new BombTower(new Vector3(10000000, -100, 10000000), assetManager));
        }
    }


    /**
     * Loads the bomb tower instances into the bomb tower pool.
     * */
    private void loadBombTowerInstancesIntoInstancePool(){
        for (int i = 0; i < n; i++) {
            bombTowerInstances.add(bombTowerPool.get(i).getBombTowerModelInstance());
        }
    }


    /**
     * Getter for a bomb tower at the given index.
     * @param index the given bomb tower.
     * */
    public BombTower getTower(int index) {
        return bombTowerPool.get(index);
    }


    /**
     * Returns an unused tower.
     * Returns the used tower at the first index if every tower is used.
     * */
    public BombTower getUnusedTower(){
        for (int i = 0; i < bombTowerPool.size(); i++) {
            if (!bombTowerPool.get(i).isUsed){
                return bombTowerPool.get(i);
            }
        }
        return bombTowerPool.get(0);
    }


    /**
     * Getter for all bomb tower instances.
     * */
    public ArrayList<ModelInstance> getBombTowerInstances(){
        return bombTowerInstances;
    }


    /**
     * Adds a given model instance to the active bomb tower instances pool.
     * Automatically checks afterwards if other tower could be added to the pools and adds them if he finds some.
     * @param modelInstance the given model instance.
     * */
    public void addActiveBombTowerInstance(ModelInstance modelInstance) {
        activeBombTowerInstances.add(modelInstance);
        for (int i = 0; i < bombTowerPool.size(); i++) {
            if ((int) (((bombTowerPool.get(i).position.x/50)/2)+0.5f)+4 >= 0 && (int) (((bombTowerPool.get(i).position.x/50)/2)+0.5f)+4 <= 10 && !activeBombTower.contains(bombTowerPool.get(i))) {
                activeBombTower.add(bombTowerPool.get(i));
            }
        }
    }


    /**
     * Returns all active tower.
     * */
    public ArrayList<BombTower> getActiveBombTower() {
        return activeBombTower;
    }


    /**
     * Returns all bomb tower.
     * */
    public ArrayList<BombTower> getTower() {
        return bombTowerPool;
    }


    /**
     * Deletes the given model instance out of the active tower instance pool.
     * @param modelInstance the given model instance.
     * */
    public void deleteActiveBombTowerInstance(ModelInstance modelInstance){
        activeBombTowerInstances.remove(modelInstance);
    }


    /**
     * Deletes the given tower out of the active tower pool.
     * @param tower the given tower.
     * */
    public void deleteActiveBombTower(BombTower tower) {
        activeBombTower.remove(tower);
        tower.setPosition(-10000000, 10000000);
        activeBombTowerInstances.remove(tower.getBombTowerModelInstance());
    }


    /**
     * Returns all active bomb tower instances.
     * */
    public ArrayList<ModelInstance> getActiveBombTowerInstances() {
        return activeBombTowerInstances;
    }


    /**
     * Returns true if the active tower instance pool contains the given instance.
     * @param instance the given instance.
     * */
    public boolean isBombTowerInstanceActive(ModelInstance instance){
        return activeBombTowerInstances.contains(instance);
    }

    @Override
    public void dispose() {
        for (BombTower bombTower : bombTowerPool) {
            bombTower.dispose();
        }
    }
}
