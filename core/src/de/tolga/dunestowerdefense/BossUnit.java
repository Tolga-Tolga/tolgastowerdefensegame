package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class BossUnit extends EnemyUnit implements Disposable {
    private Model bossModel;
    private AssetManager asset;
    private ModelInstance bossInstance;
    private final static int health = 4500;
    public final static Vector3 bossScale = new Vector3(0.25f,0.25f,0.25f);

    public BossUnit(Vector3 position, boolean[][] map, Portal start, Portal end, AssetManager assetManager, ArrayList<Integer> healthpoints) {
        super(position, 0.5f, map, start, end, healthpoints, health);
        bossModel = assetManager.get("bossunit/source/bossunitModel.g3db", Model.class);
        bossInstance = new ModelInstance(bossModel);
        bossInstance.transform.set(position, GameScreen.emptyQuaternion, bossScale);
    }


    /**
     * Sets the position at the given vector values.
     * @param x the given x vector
     * @param y the given y(2d)/z(3d) vector
     * */
    public void setPosition(float x, float y){
        this.position = position.set(x, 25, y);
        bossInstance.transform.set(position, GameScreen.emptyQuaternion, bossScale);
    }


    /**
     * Returns the model instance.
     * */
    public ModelInstance getBossInstance (){
        return bossInstance;
    }

    public void dispose() {
        super.dispose();
        bossModel.dispose();
        asset.dispose();
    }
}
