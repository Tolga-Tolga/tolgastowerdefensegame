package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class GunTurretTowerPool implements Disposable {
    private ArrayList<GunTurret> gunTurretTowerPool;
    private ArrayList<ModelInstance> gunTurretInstances;
    private ArrayList<ModelInstance> activeGunTurretInstances;
    private ArrayList<GunTurret> activeGunTurretTowerPool;
    private final int n;
    private Wave wave;
    private EnemyUnit enemyUnit;
    private Vector2 enemyUnitPosition = new Vector2();


    public GunTurretTowerPool(int n, AssetManager assetManager){
        gunTurretTowerPool = new ArrayList<GunTurret>(n);
        gunTurretInstances = new ArrayList<ModelInstance>(n);
        activeGunTurretInstances = new ArrayList<ModelInstance>(n);
        activeGunTurretTowerPool = new ArrayList<GunTurret>();
        this.n = n;
        loadTurretsIntoPool(assetManager);
        loadGunTurretInstancesIntoInstancePool();
    }


    /**
     * Loads the turrets into the tower pool arraylist.
     * @param assetManager the given asset manager which is necessary for the gun turret object.
     * */
    private void loadTurretsIntoPool(AssetManager assetManager) {
        for (int i = 0; i < n; i++) {
            gunTurretTowerPool.add(new GunTurret(new Vector3(100, 100, 100), assetManager));
        }
    }


    /**
     * Loads the gun turret instances into the bomb tower pool.
     * */
    private void loadGunTurretInstancesIntoInstancePool(){
        for (int i = 0; i < n; i++) {
            gunTurretInstances.add(gunTurretTowerPool.get(i).getGunTurretModelInstance());
        }
    }


    /**
     * Getter for a gun turret at the given index.
     * @param index the given gun turret.
     * */
    public GunTurret getTower(int index) {
        return gunTurretTowerPool.get(index);
    }


    /**
     * Returns an unused tower.
     * Returns the used tower at the first index if every tower is used.
     * */
    public GunTurret getUnusedTower(){
        for (int i = 0; i < gunTurretTowerPool.size(); i++) {
            if (!gunTurretTowerPool.get(i).isUsed){
                return gunTurretTowerPool.get(i);
            }
        }
        return gunTurretTowerPool.get(0);
    }


    /**
     * Getter for all gun turret instances.
     * */
    public ArrayList<ModelInstance> getGunTurretInstances(){
        return gunTurretInstances;
    }


    /**
     * Adds a given model instance to the active gun turret instances pool.
     * Automatically checks afterwards if other tower could be added to the pools and adds them if he finds some.
     * @param modelInstance the given model instance.
     * */
    public void addActiveGunTurretInstance(ModelInstance modelInstance) {
        activeGunTurretInstances.add(modelInstance);
        for (int i = 0; i < gunTurretTowerPool.size(); i++) {
            if ((int) (((gunTurretTowerPool.get(i).position.x/50)/2)+0.5f)+4 >= 0 && (int) (((gunTurretTowerPool.get(i).position.x/50)/2)+0.5f)+4 <= 10 && !activeGunTurretTowerPool.contains(gunTurretTowerPool.get(i))) {
                activeGunTurretTowerPool.add(gunTurretTowerPool.get(i));
            }
        }
    }


    /**
     * Deletes the given model instance out of the active turret instance pool.
     * @param modelInstance the given model instance.
     * */
    public void deleteActiveGunTurretInstace(ModelInstance modelInstance){
        activeGunTurretInstances.remove(modelInstance);
    }


    /**
     * Returns all active gun turret instances.
     * */
    public ArrayList<ModelInstance> getActiveGunTurretInstances() {
        return activeGunTurretInstances;
    }


    /**
     * Returns all active gun turrets.
     * */
    public ArrayList<GunTurret> getActiveGunTurretTowerPool() {
        return activeGunTurretTowerPool;
    }


    /**
     * Returns true if the active turret instance pool contains the given instance.
     * @param instance the given instance.
     * */
    public boolean isGunTurretInstanceActive(ModelInstance instance){
        for (int i = 0; i < activeGunTurretInstances.size(); i++) {
            if (activeGunTurretInstances.get(i).equals(instance)) return true;
        }
        return false;
    }


    /**
     * Loads the given wave into the tower pool.
     * @param wave the given wave.
     * */
    public void loadWaveIntoTowerPool(Wave wave) {
        this.wave = wave;
    }


    /**
     * Returns the size of the gun turret pool.
     * */
    public int getTowerPoolSize() {
        return gunTurretTowerPool.size();
    }


    /**
     * Returns the gun turret pool arraylist which holds every gun turret.
     * */
    public ArrayList<GunTurret> getTowers() {
        return gunTurretTowerPool;
    }


    /**
     * Deletes the given turret out of the active turret pool.
     * @param gunTurret the given tower.
     * */
    public void deleteActiveGunTurret(GunTurret gunTurret) {
        activeGunTurretTowerPool.remove(gunTurret);
        gunTurret.setPosition(-10000000, 10000000);
        activeGunTurretInstances.remove(gunTurret.getGunTurretModelInstance());
    }

    @Override
    public void dispose() {
        for (GunTurret gunTurret : gunTurretTowerPool) {
            gunTurret.dispose();
        }
    }
}
