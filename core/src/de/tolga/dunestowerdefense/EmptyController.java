package de.tolga.dunestowerdefense;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.ControllerMapping;
import com.badlogic.gdx.controllers.ControllerPowerLevel;

public class EmptyController implements Controller {
    @Override
    public boolean getButton(int buttonCode) {
        return false;
    }

    @Override
    public float getAxis(int axisCode) {
        return 0;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getUniqueId() {
        return null;
    }

    @Override
    public int getMinButtonIndex() {
        return 0;
    }

    @Override
    public int getMaxButtonIndex() {
        return 0;
    }

    @Override
    public int getAxisCount() {
        return 0;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public boolean canVibrate() {
        return false;
    }

    @Override
    public boolean isVibrating() {
        return false;
    }

    @Override
    public void startVibration(int duration, float strength) {

    }

    @Override
    public void cancelVibration() {

    }

    @Override
    public boolean supportsPlayerIndex() {
        return false;
    }

    @Override
    public int getPlayerIndex() {
        return 0;
    }

    @Override
    public void setPlayerIndex(int index) {

    }

    @Override
    public ControllerMapping getMapping() {
        return null;
    }

    @Override
    public ControllerPowerLevel getPowerLevel() {
        return null;
    }

    @Override
    public void addListener(ControllerListener listener) {

    }

    @Override
    public void removeListener(ControllerListener listener) {

    }
}
