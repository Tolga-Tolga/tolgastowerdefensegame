package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

public class BombTower extends Tower implements Disposable {
    private final static float range = 300;
    private final static float bombDamageRange = 130;
    private final static int damage = 20;
    public final static float cost = 200;
    private final static int cadence = 3;
    private final static float velocity = 0.8f;
    private final Model bombTowerModel;
    private final ModelInstance bombTowerModelInstance;
    public final static Vector3 bombTowerScale = new Vector3(100f,100f,100f);
    public final static Vector3 bombScale = new Vector3(10f,10f,10f);
    private Quaternion enemyDirection;

    protected BombTower(Vector3 position, AssetManager assetManager) {
        super(range, damage, cadence, cost, position);
        this.position = position;
        bombTowerModel = assetManager.get("tdassets/Models/OBJformat/weapon_cannon.obj", Model.class);
        bombTowerModelInstance = new ModelInstance(bombTowerModel);
        bombTowerModelInstance.transform.set(position, GameScreen.emptyQuaternion, bombTowerScale);
    }


    /**
     * Getter for the bomb tower model instance.
     * */
    public ModelInstance getBombTowerModelInstance(){
        return this.bombTowerModelInstance;
    }


    /**
     * Getter for the scaling of the bomb tower as a 3 dimensional vector.
     * */
    public Vector3 getBombTowerScale(){
        return bombTowerScale;
    }


    /**
     * Returns true if the given enemy position is in the shooting range.
     * @param enemy the given enemy position as a 2d vector.
     * */
    public boolean targetInShootingRange(Vector2 enemy) {
        return rangeCircle.contains(enemy);
    }


    /**
     * Returns the cost of the tower.
     * */
    public static float getTowerCost() {
        return cost;
    }


    /**
     * Returns the splash damage range of the bomb.
     * */
    public static float getBombDamageRange() {
        return bombDamageRange;
    }


    /**
     * Getter for the bomb damage.
     * */
    public static int getBombDamage() {
        return damage;
    }


    /**
     * Getter for the cadence.
     * */
    public static int getCadence() {
        return cadence;
    }

    public void dispose() {
        bombTowerModel.dispose();
    }
}
