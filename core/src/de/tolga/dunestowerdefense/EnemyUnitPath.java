package de.tolga.dunestowerdefense;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;


public class EnemyUnitPath {

    private DunesTdGraph dunesTdGraph;
    private GraphPath<Node> nodePath;
    private ArrayList<Node> nodeArrayList;

    /**
     * Calculates the path of the enemy unit.
     * */
    EnemyUnitPath(Portal start, Portal end, boolean[][] map, Vector3 position) {
        //int positionX = (int) (((position.x/50)/2)+0.5f) +4;
        //int positionY = (int) (((position.z/50)/2)+0.5f) +4;
        //int endPositionX = (int) (((end.getPosition().x/50)/2)+0.5f) +4;
        //int endPositionY = (int) (((end.getPosition().z/50)/2)+0.5f) +4;
        int positionX = 0;
        int positionY = 9;
        int endPositionX = 9;
        int endPositionY = 0;
        //System.out.println(positionX);
        //System.out.println(positionY);
        //System.out.println(endPositionX);
        //System.out.println(endPositionY);
        nodeArrayList = new ArrayList<Node>();
        dunesTdGraph = new DunesTdGraph();

        int nodeNumber = 0;

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                Node node = new Node(j,i, nodeNumber);
                nodeArrayList.add(j + (i * 10), node);
            }
        }

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (!map[i][j]) {
                    dunesTdGraph.addNode(nodeArrayList.get(j + (i * 10)));
                    nodeNumber ++;
                }
            }
        }


        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (!map[i][j]) {
                    if (((j >=1) && !(map[i][j-1]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get((j - 1) + 10 * i));
                    }
                    if (((j <= 8) && !(map[i][j+1]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get((j + 1) + 10 * i));
                    }
                    if (((i >= 1) && !(map[i-1][j]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get(j + 10 * (i - 1)));
                    }
                    if (((i <= 8) && !(map[i+1][j]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get(j + 10 * (i + 1)));
                    }
                }
            }
        }
        nodePath = dunesTdGraph.findPath(nodeArrayList.get(positionX + positionY * 10), nodeArrayList.get(endPositionX + endPositionY * 10));
    }

    /**
     * for testing purposes only!
     * */
    EnemyUnitPath(boolean[][] map) {
        //int positionX = (int) (((position.x/50)/2)+0.5f) +4;
        //int positionY = (int) (((position.z/50)/2)+0.5f) +4;
        //int endPositionX = (int) (((end.getPosition().x/50)/2)+0.5f) +4;
        //int endPositionY = (int) (((end.getPosition().z/50)/2)+0.5f) +4;
        int positionX = 0;
        int positionY = 9;
        int endPositionX = 9;
        int endPositionY = 0;
        //System.out.println(positionX);
        //System.out.println(positionY);
        //System.out.println(endPositionX);
        //System.out.println(endPositionY);
        nodeArrayList = new ArrayList<Node>();
        dunesTdGraph = new DunesTdGraph();

        int nodeNumber = 0;

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                Node node = new Node(j,i, nodeNumber);
                nodeArrayList.add(j + (i * 10), node);
            }
        }

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (!map[i][j]) {
                    dunesTdGraph.addNode(nodeArrayList.get(j + (i * 10)));
                    nodeNumber ++;
                }
            }
        }


        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (!map[i][j]) {
                    if (((j >=1) && !(map[i][j-1]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get((j - 1) + 10 * i));
                    }
                    if (((j <= 8) && !(map[i][j+1]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get((j + 1) + 10 * i));
                    }
                    if (((i >= 1) && !(map[i-1][j]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get(j + 10 * (i - 1)));
                    }
                    if (((i <= 8) && !(map[i+1][j]))){
                        dunesTdGraph.connectNodes(nodeArrayList.get(j + 10 * i), nodeArrayList.get(j + 10 * (i + 1)));
                    }
                }
            }
        }
        nodePath = dunesTdGraph.findPath(nodeArrayList.get(positionX + positionY * 10), nodeArrayList.get(endPositionX + endPositionY * 10));
    }

    /**
     * Calculates the path from the given parameters.
     * @param start the given entry point.
     * @param end the given end point.
     * @param map the given boolean map, where true mains a blocked node.
     * @param position the given position.
     * */
    public static GraphPath<Node> getPath(Portal start, Portal end, boolean[][] map, Vector3 position){
        EnemyUnitPath temp = new EnemyUnitPath(start, end, map, position);
        return temp.nodePath;
    }

    /**
     * for testing purposes only!
     * @param map the given boolean map, where true mains a blocked node.
     * */
    public static GraphPath<Node> getPath(boolean[][] map) {
        EnemyUnitPath temp = new EnemyUnitPath(map);
        return temp.nodePath;
    }
}
