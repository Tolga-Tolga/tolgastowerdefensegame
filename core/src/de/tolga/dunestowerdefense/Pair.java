package de.tolga.dunestowerdefense;

public class Pair <T,K>{
    private T first;
    private K second;

    public Pair(T first, K second) {
        this.first = first;
        this.second = second;
    }

    public Pair() {

    }


    /**
     * Sets the first object of the pair.
     * */
    public void setFirst(T first) {
        this.first = first;
    }


    /**
     * Sets the second object of the pair.
     * */
    public void setSecond(K second) {
        this.second = second;
    }


    /**
     * Gets the first object of the pair.
     * */
    public T getFirst() {
        return first;
    }


    /**
     * Gets the second object of the pair.
     * */
    public K getSecond() {
        return second;
    }


    /**
     * Clears the pair.
     * */
    public void clear() {
        first = null;
        second = null;
    }
}
