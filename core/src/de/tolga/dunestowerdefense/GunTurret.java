package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.PauseableThread;

public class GunTurret extends Tower implements Disposable{
    private final static float range = 150;
    private final static int damage = 5;
    public final static float cost = 80;
    private final static int cadence = 32;
    private final static float velocity = 0.8f;
    private final Model gunTurretTowerModel;
    //private final Model bulletModel;
    private AssetManager asset;
    private final ModelInstance gunTurretModelInstance;
    //private final ModelInstance bulletModelInstance;
    public static final Vector3 gunTurretScale = new Vector3(0.1f, 0.1f, 0.1f);
    private Quaternion enemyDirection;
    private PauseableThread bulletThread;

    public GunTurret(Vector3 position) {
        super(range, damage, cadence, cost, position);
        this.position = position;
        asset = new AssetManager();
        asset.load("crossbow_tower/crossbowtower.g3db", Model.class);
        asset.load("crossbow_tower/arrow/crossbowtowerarrow.g3db", Model.class);
        asset.finishLoading();
        gunTurretTowerModel = asset.get("crossbow_tower/crossbowtower.g3db", Model.class);
        gunTurretModelInstance = new ModelInstance(gunTurretTowerModel);
        gunTurretModelInstance.transform.set(position, GameScreen.emptyQuaternion, gunTurretScale);
        }

    protected GunTurret(Vector3 position, AssetManager assetManager) {
        super(range, damage, cadence, cost, position);
        this.position = position;
        gunTurretTowerModel = assetManager.get("crossbow_tower/crossbowtower.g3db", Model.class);
        gunTurretModelInstance = new ModelInstance(gunTurretTowerModel);
        gunTurretModelInstance.transform.set(position, GameScreen.emptyQuaternion, gunTurretScale);
    }


    /**
     * Returns the model instance.
     * */
    public ModelInstance getGunTurretModelInstance() {
        return gunTurretModelInstance;
    }


    /**
     * Returns the scaling of the gun turret.
     * */
    public Vector3 getGunTurretScale(){
        return gunTurretScale;
    }


    /**
     * Returns true if the given enemy is in shooting range.
     * @param enemy the given enemy.
     * */
    public boolean targetInShootingRange(Vector2 enemy){
        return rangeCircle.contains(enemy);
    }


    /**
     * Returns the damage which the gun turret deals with each arrow.
     * */
    public static int getBulletDamage() {
        return damage;
    }


    /**
     * Returns the cadence of the gun turret.
     * */
    public static int getCadence() {
        return cadence;
    }

    @Override
    public void dispose() {
        //bulletModel.dispose();
        gunTurretTowerModel.dispose();
    }

    public static void main(String[] args) {
        Circle circle = new Circle(0,0,10);
        Vector2 vector2 = new Vector2(10f,0.1f);
        //System.out.println(circle.contains(vector2));
    }
}
