package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class Infantry extends EnemyUnit implements Disposable {
    private Model infantryModel;
    private AssetManager asset;
    private ModelInstance infantryInstance;
    private static int health = 20;
    public final static Vector3 infantryScale = new Vector3(3f,3f,3f);

    public Infantry(Vector3 position, boolean[][] map, Portal start, Portal end, AssetManager assetManager, ArrayList<Integer> healthpoints){
        super(position, 2.0f, map, start, end, healthpoints, health);
        infantryModel = assetManager.get("infantry/infantry.g3db", Model.class);
        infantryInstance = new ModelInstance(infantryModel);
        infantryInstance.transform.set(position, GameScreen.emptyQuaternion, infantryScale);
    }


    /**
     * Sets the position at the given vector values.
     * @param x the given x vector
     * @param y the given y(2d)/z(3d) vector
     * */
    public void setPosition(float x, float y){
        this.position = position.set(x, 8, y);
        infantryInstance.transform.set(position, GameScreen.emptyQuaternion, infantryScale);
    }


    /**
     * Returns the model instance.
     * */
    public ModelInstance getInfantryInstance(){
        return infantryInstance;
    }


    public void dispose(){
        super.dispose();
        infantryModel.dispose();
        asset.dispose();
    }
}
