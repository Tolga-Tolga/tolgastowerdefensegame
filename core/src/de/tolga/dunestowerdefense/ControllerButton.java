package de.tolga.dunestowerdefense;

/**
 * Controller button integer values.
 * */
public class ControllerButton {
    //button 0: A
    //button 1: B
    //button 2: X
    //button 3: Y
    //button 4: BACK
    //button 5: ?
    //button 6: START
    //button 7: LSTICK
    //button 8: RSTICK
    //button 9: LBUMPER
    //button 10: RBUMPER
    //button 11: UP
    //button 12: DOWN
    //button 13: LEFT
    //button 14: RIGHT
    //button 15: ?
    public final static int A = (0);
    public final static int B = (1);
    public final static int X = (2);
    public final static int Y = (3);
    public final static int BACK = (4);
    public final static int START = (6);
    public final static int LSTICK = (7);
    public final static int RSTICK = (8);
    public final static int LBUMPER = (9);
    public final static int RBMUMPER = (10);
    public final static int UP = (11);
    public final static int DOWN = (12);
    public final static int LEFT = (13);
    public final static int RIGHT = (14);
}
