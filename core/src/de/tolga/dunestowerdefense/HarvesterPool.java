package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class HarvesterPool implements Disposable {
    private ArrayList<Harvester> harvester;
    private ArrayList<ModelInstance> harvesterInstances;
    private ArrayList<AnimationController> harversterAnimations;
    private ArrayList<Integer> healthpoints;
    private int n;
    private Portal start;
    private Portal end;
    private boolean[][] map;

    public HarvesterPool(int n, Portal start, Portal end, boolean[][] map, AssetManager assetManager, ArrayList<Integer> healthpoints) {
        this.n = n;
        this.start = start;
        this.end = end;
        this.map = map;
        this.healthpoints = healthpoints;
        harvester = new ArrayList<Harvester>(n);
        harvesterInstances = new ArrayList<ModelInstance>(n);
        harversterAnimations = new ArrayList<AnimationController>(n);
        loadHarvesterIntoPool(assetManager);
        loadHarvesterInstacesIntoPool();
        loadAnimationsOfBossUnitsInThePool();
    }


    /**
     * Loads the harvester into the harvester pool.
     * @param assetManager the given asset manager which is necessary for the harvester object.
     * */
    private void loadHarvesterIntoPool(AssetManager assetManager) {
        for (int i = 0; i < n; i++) {
            harvester.add(new Harvester(new Vector3(-1000000, -100, -1000000),
                    map,
                    start,
                    end,
                    assetManager,
                    healthpoints));
        }
    }


    /**
     * Loads the model instances into the harvester instance arraylist.
     * */
    private void loadHarvesterInstacesIntoPool() {
        for (int i = 0; i < n; i++) {
            harvesterInstances.add(harvester.get(i).getHarvesterInstance());
        }
    }


    /**
     * Loads the animations of the instances into the harvester animation arraylist.
     * */
    private void loadAnimationsOfBossUnitsInThePool() {
        for (int i = 0; i < n; i++) {
            AnimationController harvesterAnimation = new AnimationController(harvester.get(i).getHarvesterInstance());
            harvesterAnimation.setAnimation(harvester.get(i).getHarvesterInstance().animations.first().id, -1);
            harversterAnimations.add(harvesterAnimation);
        }
    }


    /**
     * Returns an arraylist which contains every harvester.
     * */
    public ArrayList<Harvester> getHarvester() {
        return harvester;
    }


    /**
     * Returns the harvester at the given index from the harvester arraylist.
     * @param index the given index.
     * */
    private Harvester getOneHarvester(int index) {
        return harvester.get(index);
    }


    /**
     * Updates the harvester animations with the given delta time.
     * @param delta the given delta time.
     * */
    public void updateAnimations(float delta) {
        for (int i = 0; i < n; i++) {
            harversterAnimations.get(i).update(delta);
        }
    }


    /**
     * Returns all harvester instance animations.
     * */
    public ArrayList<AnimationController> getHarvesterAnimations() {
        return harversterAnimations;
    }


    @Override
    public void dispose() {
        for (Harvester harvester : harvester) {
            harvester.dispose();
        }
    }
}
