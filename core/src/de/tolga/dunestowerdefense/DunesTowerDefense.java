package de.tolga.dunestowerdefense;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;

public class DunesTowerDefense extends Game {
	private DunesTowerDefense INSTANCE;
	private Music backgroundMusic;
	private MainMenuScreen mainMenuScreen;
	//private OptionScreen optionScreen = new OptionScreen();
	private GameScreen gameScreen;
	//private PauseScreen pauseScreen = new PauseScreen();

	public DunesTowerDefense() {
		INSTANCE = this;
	}


	/**
	 * Creates the game.
	 * */
	@Override
	public void create () {

		mainMenuScreen = new MainMenuScreen(INSTANCE);
		setScreen(mainMenuScreen);
	}


	/**
	 * Returns the music.
	 * */
	public Music getMusic(){
		return this.backgroundMusic;
	}


	/**
	 * Changes the screen to the given screen.
	 * @param screen the given screen.
	 * */
	public void changeScreen(Screen screen) {
		setScreen(screen);
	}


	/**
	 * Changes the screeen to the given index.
	 * Index 0: Main menu screen.
	 * @param index the given index.
	 * */
	public void changeScreen(int index) {
		if (index == 0) {
			mainMenuScreen = new MainMenuScreen(INSTANCE);
			setScreen(mainMenuScreen);
		}
	}

	public void dispose(){
	}
}
