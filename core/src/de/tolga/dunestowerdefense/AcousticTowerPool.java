package de.tolga.dunestowerdefense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class AcousticTowerPool implements Disposable {
    private ArrayList<AcousticTower> acousticTowers;
    private ArrayList<ModelInstance> acousticTowerInstances;
    private ArrayList<ModelInstance> activeAcousticTowerInstances;
    private final int n;
    private Wave wave;
    private EnemyUnit enemyUnit;
    private Vector2 enemyUnitPosition;
    private Sound slowSound;
    private Sound unslowSound;

    public AcousticTowerPool(int n, AssetManager assetManager){
        acousticTowers = new ArrayList<AcousticTower>(n);
        acousticTowerInstances = new ArrayList<ModelInstance>(n);
        activeAcousticTowerInstances = new ArrayList<ModelInstance>(n);
        this.n = n;
        enemyUnitPosition = new Vector2();
        loadTowerIntoPool(assetManager);
        loadAcousticTowerInstancesIntoPool();
        loadSounds();
    }


    /**
     * Loads tower models into the tower pool.
     * @param assetManager the asset manager which holds the tower model.
     * */
    private void loadTowerIntoPool(AssetManager assetManager) {
        for (int i = 0; i < n; i++) {
            acousticTowers.add(new AcousticTower(new Vector3(10000000, -100, 10000000), assetManager));
        }
    }


    /**
     * Loads tower instances into the tower pool.
     * */
    private void loadAcousticTowerInstancesIntoPool() {
        for (int i = 0; i < n; i++) {
            acousticTowerInstances.add(acousticTowers.get(i).getAcousticTowerInstance());
        }
    }

    /**
     * Loads the given wave into the tower pool.
     * @param wave the given Wave.
     * */
    public void loadWaveintoTowerPool(Wave wave) {
        this.wave = wave;
    }


    /**
     * Getter for a tower at a given index.
     * @param index the given index.
     * */
    public AcousticTower getTower(int index){
        return acousticTowers.get(index);
    }


    /**
     * Getter for every acoustic tower.
     * */
    public ArrayList<AcousticTower> getTowers() {
        return acousticTowers;
    }


    /**
     * Getter for an unused tower.
     * If every tower is used it returns the first used tower.
     * */
    public AcousticTower getUnusedTower() {
        for (int i = 0; i < acousticTowers.size(); i++) {
            if (!acousticTowers.get(i).isUsed){
                return acousticTowers.get(i);
            }
        }
        return acousticTowers.get(0);
    }


    /**
     * Getter for every tower instance.
     * */
    public ArrayList<ModelInstance> getAcousticTowerInstances() {
        return acousticTowerInstances;
    }


    /**
     * Adds the given model instance to the active tower instances.
     * @param modelInstance the given model instance.
     * */
    public void addActiveAcousticTowerInstance(ModelInstance modelInstance) {
        activeAcousticTowerInstances.add(modelInstance);
    }


    /**
     * Deletes the given model instance from the active tower instances.
     * @param modelInstance the given model instance.
     * */
    public void deleteActiveAcousticTowerInstace(ModelInstance modelInstance){
        activeAcousticTowerInstances.remove(modelInstance);
    }


    /**
     * Deletes the given tower from the active tower pool.
     * @param tower the given tower.
     * */
    public void deleteActiveAcousticTower(AcousticTower tower) {
        acousticTowers.remove(tower);
        tower.setPosition(-10000000, 10000000);
        activeAcousticTowerInstances.remove(tower.getAcousticTowerInstance());
    }


    /**
     * Getter for active tower instances.
     * */
    public ArrayList<ModelInstance> getActiveAcousticTowerInstances() {
        return activeAcousticTowerInstances;
    }

    /**
     * Returns true if the given model instance is in the active tower instance pool.
     * @param instance the given model instance.
     * */
    public boolean isAcousticTowerInstanceActive(ModelInstance instance){
        for (int i = 0; i < activeAcousticTowerInstances.size(); i++) {
            if (activeAcousticTowerInstances.get(i).equals(instance)) return true;
        }
        return false;
    }


    /**
     * Checks if the enemy units are in slowing range and slows or cancels the slow.
     * */
    public void checkIfEnemyUnitsAreInTowerzone() {
        boolean shouldBeSlowed = true;

        if (wave != null) {
            for (int i = 0; i < wave.getUnits().size(); i++) {
                if (wave.getUnits().get(i).getPosition() != null && !wave.getUnits().get(i).isSlowed) {
                    enemyUnit = wave.getUnits().get(i);
                    enemyUnitPosition.set(enemyUnit.getPosition().x, enemyUnit.getPosition().z);
                    for (int j = 0; j < acousticTowers.size(); j++) {
                        if (acousticTowers.get(j).targetInSlowingRange(enemyUnitPosition) && !(enemyUnit instanceof Harvester)) {
                            //System.out.println("slowed!");
                            enemyUnit.setSlowed();
                            long id = slowSound.play();
                            slowSound.setVolume(id, 0.01f);
                            break;
                        }
                    }
                }
            }
            for (int i = 0; i < wave.getUnits().size(); i++) {
                shouldBeSlowed = false;
                enemyUnit = wave.getUnits().get(i);
                enemyUnitPosition.set(enemyUnit.getPosition().x, enemyUnit.getPosition().z);
                if (enemyUnit.isSlowed) {
                    for (int j = 0; j < acousticTowers.size(); j++) {
                        if (acousticTowers.get(j).targetInSlowingRange(enemyUnitPosition)) {
                            shouldBeSlowed = true;
                            break;
                        }
                    }
                    if (!shouldBeSlowed && !(enemyUnit instanceof Harvester)) {
                        enemyUnit.setUnslowed();
                        long id = unslowSound.play();
                        unslowSound.setVolume(id, 0.01f);
                    }
                }
            }

        }
    }

    /**
     * Loads the sounds of slow enemies and of unslow enemies.
     * */
    private void loadSounds() {
        slowSound = Gdx.audio.newSound(Gdx.files.internal("sfx/slow/62412__fons__steek-1.ogg"));
        unslowSound = Gdx.audio.newSound(Gdx.files.internal("sfx/slow/62412__fons__steek-1-reversed.ogg"));
    }

    /**
     * Getter for active towers.
     * */
    public ArrayList<AcousticTower> getActiveAcousticTowers() {
        return acousticTowers;
    }

    @Override
    public void dispose() {
        for(AcousticTower acousticTower : acousticTowers){
            acousticTower.dispose();
        }
    }
}
