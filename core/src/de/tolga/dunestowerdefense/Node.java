package de.tolga.dunestowerdefense;

/**
 * inspired from https://happycoding.io/tutorials/libgdx/pathfinding#graphs-connections-and-heuristics
 */

public class Node {
    public float x;
    public float y;
    String name;

    int index;

    public Node(float x, float y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public Node(float x, float y, int name) {
        this.x = x;
        this.y = y;
        this.name = "" + name;
    }


    /**
     * Sets the index of the node.
     * @param index the given index.
     * */
    public void setIndex(int index) {
        this.index = index;
    }
}
