package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class BossUnitPool implements Disposable {
    private ArrayList<BossUnit> bossUnits;
    private ArrayList<ModelInstance> bossInstances;
    private ArrayList<AnimationController> bossUnitAnimations;
    private ArrayList<Integer> healthpoints;
    private int n;
    private Portal start;
    private Portal end;
    private boolean[][] map;

    public BossUnitPool(int n, Portal start, Portal end, boolean[][] map, AssetManager assetManager, ArrayList<Integer> healthpoints) {
        this.n = n;
        this.start = start;
        this.end = end;
        this.map = map;
        this.healthpoints = healthpoints;
        bossUnits = new ArrayList<BossUnit>(n);
        bossInstances = new ArrayList<ModelInstance>(n);
        bossUnitAnimations = new ArrayList<AnimationController>(n);
        loadBossUnitsIntoPool(assetManager);
        loadBossUnitInstancesIntoPool();
        loadAnimationsOfBossUnitsInThePool();
    }

    /**
     * Loads the boss units into the boss units pool.
     * @param assetManager the given asset manager which is necessary for the boss unit object.
     * */
    private void loadBossUnitsIntoPool(AssetManager assetManager) {
        for (int i = 0; i < n; i++) {
            bossUnits.add(new BossUnit(new Vector3(-1000000, -100, -10000000),
                    map,
                    start,
                    end,
                    assetManager,
                    healthpoints));
        }
    }


    /**
     * Loads the model instances into the boss instance arraylist.
     * */
    private void loadBossUnitInstancesIntoPool() {
        for (int i = 0; i < n; i++) {
            bossInstances.add(bossUnits.get(i).getBossInstance());
        }
    }


    /**
     * Loads the animations of the instances into the boss unit animation arraylist.
     * */
    private void loadAnimationsOfBossUnitsInThePool() {
        for (int i = 0; i < n; i++) {
            AnimationController bossAnimation = new AnimationController(bossUnits.get(i).getBossInstance());
            bossAnimation.setAnimation(bossUnits.get(i).getBossInstance().animations.get(1).id, -1);
            bossUnitAnimations.add(bossAnimation);
        }
    }


    /**
     * Returns an arraylist which contains every boss unit.
     * */
    public ArrayList<BossUnit> getBossUnits() {
        return bossUnits;
    }


    /**
     * Returns the boss unit at the given index from the boss unit arraylist.
     * @param index the given index.
     * */
    public BossUnit getBossUnit(int index) {
        return bossUnits.get(index);
    }


    /**
     * Updates the boss unit animations with the given delta time.
     * @param delta the given delta time.
     * */
    public void updateAnimations(float delta) {
        for (int i = 0; i < n; i++) {
            bossUnitAnimations.get(i).update(delta);
        }
    }


    /**
     * Returns all boss unit instance animations.
     * */
    public ArrayList<AnimationController> getBossAnimations() {
        return this.bossUnitAnimations;
    }

    @Override
    public void dispose() {
        for (BossUnit bossUnit : bossUnits) {
            bossUnit.dispose();
        }
    }
}
