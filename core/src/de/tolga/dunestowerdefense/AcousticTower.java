package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

public class AcousticTower extends Tower implements Disposable {
    private final static float range = 200;
    private final static float damage = 0;
    public final static float cost = 100;
    private final static float cadence = 0;
    private final Circle acousticRange = new Circle(position.x, position.z, range);
    private final Model acousticTowerModel;
    private final ModelInstance acousticTowerModelInstance;
    public final static Vector3 acousticTowerScale = new Vector3(300f,300f,300f);

    public AcousticTower(Vector3 position, AssetManager assetManager) {
        super(range, damage, cadence, cost, position);
        this.position = position;
        acousticTowerModel = assetManager.get("acousticTower/source/jbl.obj", Model.class);
        acousticTowerModelInstance = new ModelInstance(acousticTowerModel);
        acousticTowerModelInstance.transform.set(position, GameScreen.emptyQuaternion, acousticTowerScale);
    }


    public ModelInstance getAcousticTowerInstance(){
        return acousticTowerModelInstance;
    }

    /**
     * Returns true if the given enemy is in slowing range.
     * @param enemy the given enemy position.
     * */
    public boolean targetInSlowingRange(Vector2 enemy) {
        return rangeCircle.contains(enemy.x, enemy.y);
    }

    /**
     * Called when this screen should release all resources.
     * */
    @Override
    public void dispose() {
        acousticTowerModel.dispose();
    }
}
