package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

public class Portal {
    private Vector3 position;
    private Model portalModel;
    private AssetManager asset;
    private ModelInstance portalInstance;
    public final static Vector3 portalScale = new Vector3(0.125f,0.125f,0.125f);
    private Quaternion portalDirection;
    private int round;

    public Portal(Vector3 position){
        this.position = position;
        position.x -= 25;
        position.z -= 25;
        position.y -= 12;
        round = 0;
        asset = new AssetManager();
        asset.load("portal2/portal.g3db", Model.class);
        asset.finishLoading();
        while (true){
            if (asset.update()){
                portalModel = asset.get("portal2/portal.g3db", Model.class);
                portalInstance = new ModelInstance(portalModel);
                portalInstance.transform.set(position, new Quaternion(Vector3.Y, -45), portalScale);
                break;
            }
        }
    }


    /**
     * Returns the position of the portal.
     * */
    public Vector3 getPosition(){
        return position;
    }


    /**
     * Returns the model instance of the portal.
     * */
    public ModelInstance getPortalInstance(){
        return portalInstance;
    }
}
