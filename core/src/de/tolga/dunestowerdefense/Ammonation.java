package de.tolga.dunestowerdefense;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector3;

public class Ammonation {
    private final ModelInstance modelInstance;
    private boolean isActive;
    private EnemyUnit target;
    private Vector3 bombTarget = new Vector3();
    private Tower tower;
    private float velocity;
    private Vector3 position = new Vector3();
    private Vector3 tempVector = new Vector3();
    private boolean followTarget;
    private Circle damageRange;


    public Ammonation(ModelInstance modelInstance, boolean second){
        this.modelInstance = modelInstance;
        this.isActive = second;
    }


    /**
     * Sets the given target for the ammunition of a gun turret.
     * @param target the given target.
     * */
    public void setTarget(EnemyUnit target) {
        this.target = target;
        followTarget = true;
    }


    /**
     * Sets the given target for ammunition of a bomb tower.
     * @param bombTarget the given target.
     * */
    public void setTarget(Vector3 bombTarget) {
        this.bombTarget.set(bombTarget.x, bombTarget.y, bombTarget.z);
        followTarget = false;
        damageRange = new Circle(bombTarget.x, bombTarget.z, BombTower.getBombDamageRange());
    }


    /**
     * Returns the damage area at the given target position.
     * */
    public Circle getDamageRange() {
        return damageRange;
    }


    /**
     * Sets the tower which shot the bullet/bomb and assigns the bullet the right velocity.
     * @param tower the given tower.
     * */
    public void setTower(Tower tower) {
        this.tower = tower;
        if (tower.getClass().equals(GunTurret.class)) {
            velocity = 5;
        }
        else {
            velocity = 7f;
        }
    }


    /**
     * Sets the position of the ammunition at the given vector.
     * Deep copies the vector.
     * @param position the given vector position.
     * */
    public void setPosition(Vector3 position) {
        this.position.set(position.x, position.y, position.z);
    }


    /**
     * Sets the instance of the ammunition at the given values.
     * @param x the given x vector
     * @param y the given x vector
     * @param z the given x vector
     * */
    public void setInstance(float x, float y, float z) {
        modelInstance.transform.set(tempVector.set(x,y,z), GameScreen.emptyQuaternion);
    }


    /**
     * Getter of the ammunition target if the ammunition is assigned to a gun turret.
     * */
    public EnemyUnit getTarget() {
        return target;
    }


    /**
     * Getter of the ammunition target if the ammunition is assigned to a bomb tower.
     * */
    public Vector3 getBombTarget() {
        return bombTarget;
    }


    /**
     * Getter for the assigned tower.
     * */
    public Tower getTower() {
        return tower;
    }


    /**
     * Getter for the assigned velocity.
     * */
    public float getVelocity() {
        return velocity;
    }


    /**
     * Getter of the given position.
     * */
    public Vector3 getPosition() {
        return position;
    }


    /**
     * Sets the ammunition active.
     * */
    public void setActive(){
        this.isActive = true;
    }


    /**
     * Sets the ammunition inactive.
     * */
    public void setInactive(){
        this.isActive = false;
    }


    /**
     * Getter for the assigned model instance.
     * */
    public ModelInstance getModelInstance(){
        return modelInstance;
    }


    /**
     * Returns if the ammunition is active.
     * */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Returns if the ammunition is following its target.
     * */
    public boolean isFollowingTarget() {
        return followTarget;
    }
}
