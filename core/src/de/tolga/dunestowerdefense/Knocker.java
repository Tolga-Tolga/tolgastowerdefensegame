package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;

public class Knocker {
    private Vector3 position = new Vector3();
    private Model knockerModel;
    private ModelInstance knockerModelInstance;
    private AssetManager assetManager;
    private boolean[][] map;
    public static Vector3 knockerScale = new Vector3(0.0025f,0.0025f,0.0025f);

    public Knocker(AssetManager assetManager, boolean[][] map) {
        this.assetManager = assetManager;
        this.map = map;
        knockerModel = assetManager.get("Knocker/source/knocker.g3db", Model.class);
        knockerModelInstance = new ModelInstance(knockerModel);
    }


    /**
     * Sets the position of the knocker at the given vector.
     * Attention! It doesn't create a deep copy of the given object.
     * */
    public void setPosition(Vector3 position) {
        this.position = position;
    }


    /**
     * Sets the position of the knocker at the given vector values.
     * @param x the given x vector.
     * @param y the given (2d:y/3d:z) vector.
     * */
    public void setPosition(float x, float y) {
        position.set(x, 0, y);
    }


    /**
     * Returns the position of this knocker.
     * */
    public Vector3 getPosition() {
        return position;
    }


    /**
     * Returns the model instance of this knocker.
     * */
    public ModelInstance getKnockerModelInstance() {
        return knockerModelInstance;
    }
}
