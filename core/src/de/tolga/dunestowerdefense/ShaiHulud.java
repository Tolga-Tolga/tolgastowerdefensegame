package de.tolga.dunestowerdefense;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Stack;

public class ShaiHulud {
    private Vector3 position = new Vector3();
    private final static float velocity = 2;
    private Model shaiHuludModel;
    private ModelInstance shaiHuludModelInstance;
    private AssetManager assetManager;
    private boolean[][] map;
    private boolean isActive = false;
    private AnimationController animationController;
    public final static Vector3 shaiHuludScale = new Vector3(0.3f,0.3f,0.3f);
    private static int range = 50;
    private Circle damageRange = new Circle();
    private ArrayList<Integer> money;
    private Tower[][] towers;
    private Stack<EnemyUnit> killedEnemyUnits;

    public ShaiHulud(AssetManager assetManager, boolean[][] map, ArrayList<Integer> money, Tower[][] towers){
        this.towers = towers;
        this.money = money;
        this.assetManager = assetManager;
        this.map = map;
        shaiHuludModel = assetManager.get("shai-hulud/source/worm.g3db", Model.class);
        //ModelBuilder modelBuilder = new ModelBuilder();
        //shaiHuludModel = modelBuilder.createBox(30, 10, 10, new Material(ColorAttribute.createDiffuse(Color.WHITE)),
        //        VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
        shaiHuludModelInstance = new ModelInstance(shaiHuludModel);
        animationController = new AnimationController(shaiHuludModelInstance);
        animationController.setAnimation(shaiHuludModelInstance.animations.first().id, -1);
        killedEnemyUnits = new Stack<EnemyUnit>();
    }


    /**
     * Updates the shai hulud animation with the given delta time.
     * @param delta the given delta time.
     * */
    public void updateAnimation(float delta) {
        animationController.update(delta);
    }


    /**
     * Sets the position of the shai hulud at the given position.
     * Attention! The given position won`t get deepcopied.
     * @param position the given position.
     * */
    public void setPosition(Vector3 position) {
        this.position = position;
        damageRange = new Circle(this.position.x, this.position.z, range);
    }


    /**
     * Returns the position of the shai hulud.
     * */
    public Vector3 getPosition(){
        return position;
    }


    /**
     * Sets the shai hulud active.
     * */
    public void setShaiHuludActive() {
        isActive = true;
    }


    /**
     * Sets the shai hulud inactive.
     * */
    public void setShaiHuludInactive() {
        isActive = false;
    }


    /**
     * Returns true if the shai hulud is active.
     * */
    public boolean isShaiHuludActive() {
        return isActive;
    }


    /**
     * Returns the velocity of the shai hulud.
     * */
    public float getVelocity() {
        return velocity;
    }


    /**
     * Checks if the given enemies of the given wave are in the range of the shai hulud and deletes them if they are.
     * @param wave the given wave.
     * */
    public void checkIfEnemiesAreInRangeAndKillThem(Wave wave) {
        killedEnemyUnits.clear();
        for (int i = 0; i < wave.getUnits().size(); i++) {
            if (damageRange.contains(wave.getUnits().get(i).getPosition().x, wave.getUnits().get(i).getPosition().z)) {
                killedEnemyUnits.push(wave.getUnits().get(i));
            }
        }
        while (!killedEnemyUnits.isEmpty()) {
            wave.killEnemy(killedEnemyUnits.pop());
        }
    }


    /**
     * Checks if the given tower are in the range of the shai hulud and deletes them if they are.
     * */
    public void checkIfTowerAreInRangeAndDeleteThem(AcousticTowerPool acousticTowerPool) {
        for (int i = 0; i < acousticTowerPool.getTowers().size(); i++) {
            if (damageRange.contains(acousticTowerPool.getTowers().get(i).getPosition().x, acousticTowerPool.getTowers().get(i).getPosition().z)) {
                setGroundFalse(acousticTowerPool.getTowers().get(i));
                acousticTowerPool.deleteActiveAcousticTowerInstace(acousticTowerPool.getTowers().get(i).getAcousticTowerInstance());
                acousticTowerPool.deleteActiveAcousticTower(acousticTowerPool.getTowers().get(i));
                money.set(2, money.get(2) + (int) (AcousticTower.cost * 0.3f));
            }
        }
    }


    /**
     * Checks if the given tower are in the range of the shai hulud and deletes them if they are.
     * */
    public void checkIfTowerAreInRangeAndDeleteThem(BombTowerPool bombTowerPool) {
        for (int i = 0; i < bombTowerPool.getTower().size(); i++) {
            if (damageRange.contains(bombTowerPool.getTower().get(i).getPosition().x, bombTowerPool.getTower().get(i).getPosition().z)) {
                setGroundFalse(bombTowerPool.getTower().get(i));
                bombTowerPool.deleteActiveBombTowerInstance(bombTowerPool.getTower().get(i).getBombTowerModelInstance());
                bombTowerPool.deleteActiveBombTower(bombTowerPool.getTower().get(i));
                money.set(2, money.get(2) + (int) (BombTower.cost * 0.3f));
            }
        }
    }


    /**
     * Checks if the given tower are in the range of the shai hulud and deletes them if they are.
     * */
    public void checkIfTowerAreInRangeAndDeleteThem(GunTurretTowerPool gunTurretTowerPool) {
        for (int i = 0; i < gunTurretTowerPool.getTowers().size(); i++) {
            if (damageRange.contains(gunTurretTowerPool.getTowers().get(i).getPosition().x, gunTurretTowerPool.getTowers().get(i).getPosition().z)) {
                setGroundFalse(gunTurretTowerPool.getTowers().get(i));
                gunTurretTowerPool.deleteActiveGunTurretInstace(gunTurretTowerPool.getTowers().get(i).getGunTurretModelInstance());
                gunTurretTowerPool.deleteActiveGunTurret(gunTurretTowerPool.getTowers().get(i));
                money.set(2, money.get(2) + (int) (GunTurret.cost * 0.3f));
            }
        }
    }


    /**
     * Sets the ground on the map at the given position tower false.
     * @param tower the given destroyed tower.
     * */
    private void setGroundFalse(Tower tower) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (towers[i][j] == tower) map[i][j] = false;
            }
        }
    }


    /**
     * Updates the position of the damage circle.
     * */
    public void updateDamageCircle() {
        damageRange.set(position.x, position.z, range);
    }


    /**
     * Returns the model instance of the shai hulud.
     * */
    public ModelInstance getModelInstance() {
        return this.shaiHuludModelInstance;
    }
}
