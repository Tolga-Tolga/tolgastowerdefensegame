package de.tolga.dunestowerdefense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;
import java.util.Stack;

public class Wave implements Disposable {
    private int roundCounter;
    private int roundUnitCount;
    private InfantryPool infantryPool;
    private BossUnitPool bossUnitPool;
    private HarvesterPool harvesterPool;
    private StartPortal startPortal;
    private EndPortal endPortal;
    private ArrayList<EnemyUnit> units;
    private ArrayList<ModelInstance> unitInstances;
    private ArrayList<Ammonation> shots;
    private ArrayList<Integer> score;
    private Stack<EnemyUnit> enemyUnitQueue;
    private AmmoPool ammoPool;
    private Ammonation shotAmmonation;
    private EnemyUnit enemyUnit;
    private Infantry infantry;
    private Harvester harvester;
    private BossUnit bossUnit;
    private int x1;
    private int x2;
    private int z1;
    private int z2;
    private ArrayList<AnimationController> infantryAnimations;
    private ArrayList<AnimationController> harvesterAnimations;
    private ArrayList<AnimationController> bossAnimations;
    private ArrayList<AnimationController> activeAnimationControllers = new ArrayList<AnimationController>();
    private Stack<AnimationController> waitingAnimationControllers = new Stack<AnimationController>();
    private Pair<EnemyUnit, AnimationController> enemyPair = new Pair<EnemyUnit, AnimationController>();
    private Sound arrowSound;
    private Sound cannonSound;
    private Sound slowSound;

    //difficulty which needs to be at least 1 which is the easiest diff.
    private int difficulty = 1;

    public Wave(InfantryPool infantryPool, BossUnitPool bossUnitPool, HarvesterPool harvesterPool,StartPortal startPortal, EndPortal endPortal, AmmoPool ammoPool, ArrayList<Integer> score){
        this.infantryPool = infantryPool;
        this.bossUnitPool = bossUnitPool;
        this.harvesterPool = harvesterPool;
        this.ammoPool = ammoPool;
        this.startPortal = startPortal;
        this.endPortal = endPortal;
        roundCounter = 0;
        roundUnitCount = 2;
        this.units = new ArrayList<EnemyUnit>();
        this.unitInstances = new ArrayList<ModelInstance>();
        shots = new ArrayList<Ammonation>();
        this.enemyUnitQueue = new Stack<EnemyUnit>();
        this.score = score;
        this.infantryAnimations = infantryPool.getInfantryAnimations();
        this.harvesterAnimations = harvesterPool.getHarvesterAnimations();
        this.bossAnimations = bossUnitPool.getBossAnimations();
        createSounds();
    }

    public Wave(InfantryPool infantryPool, BossUnitPool bossUnitPool, HarvesterPool harvesterPool,StartPortal startPortal, EndPortal endPortal, AmmoPool ammoPool, ArrayList<Integer> score, int difficulty){
        this.infantryPool = infantryPool;
        this.bossUnitPool = bossUnitPool;
        this.harvesterPool = harvesterPool;
        this.ammoPool = ammoPool;
        this.startPortal = startPortal;
        this.endPortal = endPortal;
        roundCounter = 0;
        roundUnitCount = 2;
        this.units = new ArrayList<EnemyUnit>();
        this.unitInstances = new ArrayList<ModelInstance>();
        shots = new ArrayList<Ammonation>();
        this.enemyUnitQueue = new Stack<EnemyUnit>();
        this.score = score;
        this.infantryAnimations = infantryPool.getInfantryAnimations();
        this.harvesterAnimations = harvesterPool.getHarvesterAnimations();
        this.bossAnimations = bossUnitPool.getBossAnimations();
        this.difficulty = difficulty;
        createSounds();
    }


    /**
     * Starts the next round of the wave.
     * Can be called as first round.
     * */
    public void startNextRound(){
        units.clear();
        unitInstances.clear();
        shots.clear();
        activeAnimationControllers.clear();
        waitingAnimationControllers.clear();
        enemyUnitQueue.clear();
        roundCounter++;
        ArrayList<Infantry> infantries = infantryPool.getInfantries();
        ArrayList<BossUnit> bossUnits = bossUnitPool.getBossUnits();
        ArrayList<Harvester> harvester = harvesterPool.getHarvester();

        for (int i = 1; i <= roundUnitCount * difficulty + roundCounter * roundUnitCount * 0.5f; i++) {
            enemyUnitQueue.push(infantries.get(i));
            waitingAnimationControllers.push(infantryAnimations.get(i));
            if ((i % 19) == 0) {
                enemyUnitQueue.push(harvester.get(i));
                waitingAnimationControllers.push(harvesterAnimations.get(i));
            }
            if (((i % 97) == 0)) {
                enemyUnitQueue.push(bossUnits.get(i));
                waitingAnimationControllers.push(bossAnimations.get(i));
            }
        }
    }


    /**
     * Returns true if the enemy queue is empty.
     * */
    public boolean queueEmpty() {
        return enemyUnitQueue.size() == 0;
    }


    /**
     * Spawns the next enemy unit.
     * */
    public void spawnNextUnit() {
        enemyUnit = enemyUnitQueue.pop();
        units.add(enemyUnit);
        activeAnimationControllers.add(waitingAnimationControllers.pop());
        System.out.println(enemyUnit.isDead());
        if (enemyUnit.getClass().equals(Infantry.class)) {
            infantry = (Infantry) enemyUnit;
            unitInstances.add(infantry.getInfantryInstance());
            infantry.setPosition(startPortal.getPosition().x, startPortal.getPosition().z);
        }
        if (enemyUnit.getClass().equals(Harvester.class)) {
            harvester = (Harvester) enemyUnit;
            unitInstances.add(harvester.getHarvesterInstance());
            harvester.setPosition(startPortal.getPosition().x, startPortal.getPosition().z);
        }
        if (enemyUnit.getClass().equals(BossUnit.class)) {
            bossUnit = (BossUnit) enemyUnit;
            unitInstances.add(bossUnit.getBossInstance());
            bossUnit.setPosition(startPortal.getPosition().x, startPortal.getPosition().z);
        }
        enemyUnit.setUsed();
        enemyUnit.findPath();
    }


    /**
     * Returns all units which are active.
     * */
    public ArrayList<EnemyUnit> getUnits() {
        return units;
    }


    /**
     * Deletes the given enemy unit out of the active units.
     * @param enemyUnit the given enemy unit.
     * */
    public void deleteUnit(EnemyUnit enemyUnit) {
        units.remove(enemyUnit);
    }


    /**
     * Adds the given enemy unit instance to the enemy unit instances.
     * @param enemyUnit the given enemy unit.
     * */
    public void addUnitInstances(EnemyUnit enemyUnit) {
        if (enemyUnit.getClass().equals(Infantry.class)){
            unitInstances.add(((Infantry) enemyUnit).getInfantryInstance());
        }
        else if (enemyUnit.getClass().equals(BossUnit.class)) {
            unitInstances.add(((BossUnit) enemyUnit).getBossInstance());
        }
        else if (enemyUnit.getClass().equals(Harvester.class)) {
            unitInstances.add(((Harvester) enemyUnit).getHarvesterInstance());
        }
    }


    /**
     * Deletes the given enemy unit from the unit instances.
     * @param enemyUnit the given enemy unit.
     * */
    public void deleteUnitInstance(EnemyUnit enemyUnit) {
        if (enemyUnit.getClass().equals(Infantry.class)){
            unitInstances.remove(((Infantry) enemyUnit).getInfantryInstance());
        }
        else if (enemyUnit.getClass().equals(BossUnit.class)){
            unitInstances.remove(((BossUnit) enemyUnit).getBossInstance());
        }
        else if (enemyUnit.getClass().equals(Harvester.class)){
            unitInstances.remove(((Harvester) enemyUnit).getHarvesterInstance());
        }
    }


    /**
     * Returns the active unit instances.
     * */
    public ArrayList<ModelInstance> getUnitInstances() {
        return unitInstances;
    }


    /**
     * Creates a new ammunition from the given attributes and stores it into the ammunition pool.
     * @param enemyUnit the enemy unit which the tower shoots at.
     * @param tower the tower which shoots at the enemy.
     * */
    public void shootAt(EnemyUnit enemyUnit, Tower tower) {
        shotAmmonation = ammoPool.shoot(enemyUnit, tower);
        if (tower instanceof GunTurret) {
            long id = arrowSound.play();
            arrowSound.setVolume(id, 0.05f);
        }
        if (tower instanceof BombTower) {
            long id = cannonSound.play();
            cannonSound.setVolume(id, 0.03f);
        }
        if (!shots.contains(shotAmmonation)) {
            shots.add(shotAmmonation);
        }
    }


    /**
     * Loads the arrow and the cannon sounds.
     * */
    private void createSounds() {
        arrowSound = Gdx.audio.newSound(Gdx.files.internal("sfx/arrow/384912__ali-6868__arrow-impact-3.wav"));
        cannonSound = Gdx.audio.newSound(Gdx.files.internal("sfx/cannon/448002__kneeling__cannon.mp3"));
    }

    /**
     * Returns all active ammunitions.
     * */
    public ArrayList<Ammonation> getShots() {
        return shots;
    }


    /**
     * Returns true if the wave is over.
     * */
    public boolean waveOver() {
        return enemyUnitQueue.size() == 0 && unitInstances.size() == 0;
    }


    /**
     * Checks if a bullet hit the enemy and checks if the damaged enemy unit died.
     * */
    public void checkIfBulletsHit() {
        for (int i = 0; i < shots.size(); i++) {
            if (shots.get(i).getTower().getClass().equals(GunTurret.class)) {
                x1 = (int) shots.get(i).getTarget().getPosition().x/30;
                x2 = (int) shots.get(i).getPosition().x/30;
                z1 = (int) shots.get(i).getTarget().getPosition().z/30;
                z2 = (int) shots.get(i).getPosition().z/30;
                if (x1 == x2 && z1 == z2) {
                    shots.get(i).setInactive();
                    shots.get(i).getTarget().giveDamage(GunTurret.getBulletDamage());
                    if (shots.get(i).getTarget().isDead()) {
                        killEnemy(shots.get(i).getTarget());
                    }
                    shots.remove(shots.get(i));
                    //System.out.println("Hit!");
                }
            }
            else if (shots.get(i).getTower().getClass().equals(BombTower.class)) {
                x1 = (int) shots.get(i).getBombTarget().x/30;
                x2 = (int) shots.get(i).getPosition().x/30;
                z1 = (int) shots.get(i).getBombTarget().z/30;
                z2 = (int) shots.get(i).getPosition().z/30;
                if (x1 == x2 && z1 == z2) {
                    shots.get(i).setInactive();
                    for (int j = 0; j < units.size(); j++) {
                        if (shots.get(i).getDamageRange().contains(units.get(j).position.x, units.get(j).position.z) && units.get(j).isDamagable()) {
                            units.get(j).giveDamage(BombTower.getBombDamage());
                            if (units.get(j).isDead()) {
                                killEnemy(units.get(j));
                            }
                        }
                    }
                    shots.remove(shots.get(i));
                    System.out.println("Hit!");
                }
            }
        }
    }


    /**
     * Kills the given enemy unit and increases the player score/spice.
     * @param unit the given enemy unit.
     * */
    public void killEnemy(EnemyUnit unit) {
        setEnemyUnitInactive(unit);
        restoreEnemyUnitHealth(unit);
        increaseScoreAndSpice(unit);
        //deleteUnitAnimation(unit);
        deleteUnitInstance(unit);
        setAnimationInactive(unit);
        deleteUnit(unit);
    }


    /**
     * Sets the given enemy unit inactive.
     * @param unit the given enemy unit.
     * */
    private void setEnemyUnitInactive(EnemyUnit unit) {
        unit.setUnused();
        unit.setInactive();
        unit.clearNodes();
    }


    /**
     * Restores the hp of the killed enemy unit.
     * */
    private void restoreEnemyUnitHealth(EnemyUnit unit) {
        unit.resetHealth();
    }


    /**
     * Increases the player score and spice by the enemy unit value.
     * @param unit the given enemy unit.
     * */
    private void increaseScoreAndSpice(EnemyUnit unit) {
        if (unit.getClass().equals(Infantry.class)) {
            score.set(1, score.get(1) + 5);
            score.set(2, score.get(2) + 1);
        }
        if (unit.getClass().equals(Harvester.class)) {
            score.set(1, score.get(1) + 15);
            score.set(2, score.get(2) + 10);
        }
        if (unit.getClass().equals(BossUnit.class)) {
            score.set(1, score.get(1) + 40);
            score.set(2, score.get(2) + 50);
        }
    }


    /**
     * Checks if the target of an active ammunition is already dead.
     * */
    public void checkIfAmmoTargetIsDead(Ammonation ammonation) {
        if (ammonation.getTarget().isDead()) {
            shots.remove(ammonation);
        }
    }


    /**
     * Sets the animation of the given enemy unit inactive.
     * */
    private void setAnimationInactive(EnemyUnit unit) {
        try {
            activeAnimationControllers.remove(units.indexOf(unit));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Updates the animations of the enemy units with the given delta time.
     * @param delta the given delta time.
     * */
    public void updateAnimations(float delta) {
        for (int i = 0; i < units.size(); i++) {
            if (units.get(i).getClass().equals(Infantry.class)) {
                activeAnimationControllers.get(i).update(delta*8);
            }
            else {
                activeAnimationControllers.get(i).update(delta);
            }
        }
    }


    /**
     * Returns the round of the wave.
     * */
    public int getRound() {
        return roundCounter;
    }


    /**
     * Sets the round of the wave with the given number.
     * @param number the given number
     * */
    public void setRound(int number) {
        this.roundCounter = number;
    }

    @Override
    public void dispose() {
        arrowSound.dispose();
        cannonSound.dispose();
    }
}
