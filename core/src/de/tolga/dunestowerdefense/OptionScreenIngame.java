package de.tolga.dunestowerdefense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.ScreenViewport;


public class OptionScreenIngame extends OptionScreen {
    private TextButton returnToGame;

    public OptionScreenIngame(){
        cursorController = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        everyController = Controllers.getControllers();
        controller = everyController.first();

        mySkin = new Skin(Gdx.files.internal("skins/craftacular/skin/craftacular-ui.json"));
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        clock = 0;

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        volume = new Slider(0f, 10f, 100f,true, mySkin, "Volume");
        resolution = new SelectBox(mySkin, "resolution");
        fullscreen = new TextButton("Exit Game", mySkin);
        returnToGame = new TextButton("Leaderboard", mySkin);

        volume.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        resolution.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        returnToGame.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        fullscreen.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);

        volume.setPosition(Gdx.graphics.getWidth()*0.125f,Gdx.graphics.getHeight()*0.85f);
        resolution.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.60f);
        returnToGame.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.35f);
        fullscreen.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.10f);


        Group group = new Group();
        group.addActor(volume);
        group.addActor(resolution);
        group.addActor(fullscreen);
        group.addActor(returnToGame);
        stage.addActor(group);

        BACKGROUND_TEXTURE = new Texture("Dirt_Background.png");
        BACKGROUND_SPRITE = new Sprite(BACKGROUND_TEXTURE);
        spriteBatch = new SpriteBatch();
        //dunesTowerDefense = DunesTowerDefense.getInstance();
        music = dunesTowerDefense.getMusic();
        music.play();
    }


    /**
     * Returns to the game screen.
     * */
    public void returnToGame(){

    }
}
