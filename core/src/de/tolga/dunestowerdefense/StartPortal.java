package de.tolga.dunestowerdefense;

import com.badlogic.gdx.math.Vector3;

public class StartPortal extends Portal{

    public StartPortal(Vector3 position) {
        super(position);
    }
}
