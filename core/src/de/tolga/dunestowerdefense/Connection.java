package de.tolga.dunestowerdefense;

import com.badlogic.gdx.math.Vector2;

/**
 * inspired from https://happycoding.io/tutorials/libgdx/pathfinding#graphs-connections-and-heuristics
 */
public class Connection implements com.badlogic.gdx.ai.pfa.Connection<Node> {
    Node fromNode;
    Node toNode;
    float cost;

    public Connection(Node fromNode, Node toNode) {
        this.fromNode = fromNode;
        this.toNode = toNode;
        cost = Vector2.dst(fromNode.x, fromNode.y, toNode.x, toNode.y);
    }

    /**
     * Returns the cost of this node.
     * */
    @Override
    public float getCost() {
        return cost;
    }


    /**
     * Returns the node which points at this node.
     * */
    @Override
    public Node getFromNode() {
        return fromNode;
    }


    /**
     * Returns the node which this node points at.
     * */
    @Override
    public Node getToNode() {
        return toNode;
    }
}
