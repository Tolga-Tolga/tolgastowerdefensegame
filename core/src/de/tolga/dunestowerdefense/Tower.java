package de.tolga.dunestowerdefense;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector3;

public abstract class Tower {
    protected float range;
    protected float damage;
    protected float cadence;
    protected int cost;
    protected boolean isUsed;
    Vector3 position;
    protected Circle rangeCircle;

    protected Tower(float range, float damage, float cadence, float cost, Vector3 position) {
        this.range = range;
        this.damage = damage;
        this.cadence = cadence;
        this.cost = (int) cost;
        this.position = position;
        rangeCircle = new Circle(position.x, position.z, range);
    }


    /**
     * Sets the position at the given vector values.
     * @param x the given x vector
     * @param y the given y(2d)/z(3d) vector
     * */
    public void setPosition(float x, float y){
        position.set(x, 0, y);
        rangeCircle.set(x,y,range);
        setUsed();
    }


    /**
     * Returns the cost of the tower.
     * */
    public int getCost(){
        return cost;
    }


    /**
     * Returns true if the tower is used.
     * */
    public boolean isUsed(){
        return isUsed;
    }


    /**
     * Sets the tower used.
     * */
    private void setUsed(){
        isUsed = true;
    }


    /**
     * Sets the tower unused.
     * */
    private void setUnused(){
        isUsed = false;
    }


    /**
     * Returns the position of the tower.
     * */
    public Vector3 getPosition() {
        return this.position;
    }
}
