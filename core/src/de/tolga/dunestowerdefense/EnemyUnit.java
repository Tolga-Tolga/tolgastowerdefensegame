package de.tolga.dunestowerdefense;

import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public abstract class EnemyUnit implements Disposable {
    protected Vector3 position;
    protected float velocity;
    protected ArrayList<Integer> healthpoints;
    protected boolean[][] map;
    private GraphPath<Node> path;
    protected Portal start;
    protected Portal end;
    protected boolean isSlowed;
    protected boolean isActive;
    protected boolean isUsed;
    protected ArrayList<Vector3> pathNodes;
    protected Vector3 movement = new Vector3();
    protected Quaternion nextNodeDirection = new Quaternion();
    protected Vector2 directionVector2D = new Vector2();
    private float degree;
    protected Vector3 lastNodePosition;
    protected int health;
    private final int basehealth;
    private boolean isDamagable = false;

    public EnemyUnit(Vector3 position, float velocity, boolean[][] map, Portal start, Portal end, ArrayList<Integer> healthpoints, int health){
        this.position = position;
        this.velocity = velocity;
        this.map = map;
        this.start = start;
        this.end = end;
        this.pathNodes = new ArrayList<Vector3>();
        this.healthpoints = healthpoints;
        this.health = health;
        this.basehealth = health;
    }


    /**
     * Finds the shortest possible path for this enemy unit.
     * */
    public void findPath(){
        path = EnemyUnitPath.getPath(start, end, map, position);
        for (int i = 0; i < path.getCount(); i++) {
            pathNodes.add(new Vector3(path.get(i).x*100-450, 0, path.get(i).y*100-450));
            float temp1 = path.get(i).x*100-450;
            float temp2 = path.get(i).y*100-450;
            //System.out.println(temp1 + " , " + temp2);
        }
        evaluateMovementAttributes();
    }


    /**
     * Calculates the Quaternion of the enemy unit.
     * This means the enemy unit is looking to the direction which he is walking to.
     * */
    protected void evaluateMovementAttributes() {
        if (hasNodes()){
            movement.set(checkIfEnemyReachedEndPortal().x - position.x, 0, checkIfEnemyReachedEndPortal().z - position.z);
            directionVector2D.set(movement.x, movement.z);
            degree = -(directionVector2D.angleDeg()-90);
            nextNodeDirection.set(Vector3.Y, degree);
            //System.out.println(degree);
        }
    }


    /**
     * Gives the enemy unit damage by the given damage.
     * @param damage the given damage.
     * */
    public void giveDamage(int damage) {
        health -= damage;
    }


    /**
     * Checks if the enemy reached the next node position and if the enemy reched the node the node gets deleted.
     * If the enemy reached the last node (end portal) this function returns true.
     * */
    private boolean hasReachedNode(){
        if (pathNodes.isEmpty()) return false;
        int pathnodeX1 = (int) pathNodes.get(0).x / 100;
        int pathnodeZ1 = (int) pathNodes.get(0).z / 100;
        int positionX1 = (int) position.x / 100;
        int positionZ1 = (int) position.z / 100;

        pathnodeX1 *= 100;
        pathnodeZ1 *= 100;
        positionX1 *= 100;
        positionZ1 *= 100;

        if (pathNodes.get(0).x > 0){
            pathnodeX1 += 50;
        }
        else {
            pathnodeX1 -= 50;
        }
        if (pathNodes.get(0).z > 0){
            pathnodeZ1 += 50;
        }
        else {
            pathnodeZ1 -= 50;
        }
        if (position.x > 0){
            positionX1 += 50;
        }
        else {
            positionX1 -= 50;
        }
        if (position.z > 0){
            positionZ1 += 50;
        }
        else {
            positionZ1 -= 50;
        }

        if (pathNodes.size() == 1 && positionX1 == pathnodeX1 && positionZ1 == pathnodeZ1) {
            return true;
        }
        if (positionX1 == pathnodeX1 && positionZ1 == pathnodeZ1) {
            pathNodes.remove(0);
            evaluateMovementAttributes();
        }
        return false;
    }


    /**
     * Checks if the enemy unit reached the end portal or if the enemy reached a node.
     * */
    public Vector3 checkIfEnemyReachedEndPortal() {
        hasReachedNode();
        if (pathNodes.size() == 1 && hasReachedNode()){
            isUsed = false;
            isActive = false;
            position.set(10000, -100, 10000);
            if (this.getClass().equals(Infantry.class)) {
                healthpoints.set(0, healthpoints.get(0) - 3);
            }
            else if (this.getClass().equals(Harvester.class)) {
                healthpoints.set(0, healthpoints.get(0) - 25);
            }
            else {
                healthpoints.set(0, healthpoints.get(0) -100);
            }
            lastNodePosition = pathNodes.get(0);
            pathNodes.clear();

            return lastNodePosition;
        }
        return pathNodes.get(0);
    }


    /**
     * Removes all nodes of this enemy unit.
     * */
    public void clearNodes() {
        pathNodes.clear();
    }


    /**
     * Returns the next position where the enemy has to walk to.
     * */
    public Vector3 getMovement() {
        return movement;
    }


    /**
     * Returns the positon of the enemy.
     * */
    public Vector3 getPosition() {
        return position;
    }


    /**
     * The direction where the enemy is walking to.
     * */
    public Quaternion getNextNodeDirection() {
        return nextNodeDirection;
    }


    /**
     * Returns true if the path has checkpoints which weren`t visited.
     * */
    public boolean hasNodes() {
        return !pathNodes.isEmpty();
    }


    /**
     * Returns the path of the enemy unit.
     * */
    public GraphPath<Node> getPath(){
        return path;
    }


    /**
     * Returns true if the enemy is dead.
     * */
    public boolean isDead(){
        return health <= 0f;
    }


    /**
     * Update the health of the player.
     * */
    public void updateHealth(int health){
        healthpoints.set(0, healthpoints.get(0) + health);
    }


    /**
     * Slows the enemy unit.
     * */
    public void setSlowed(){
        velocity /= 2;
        isSlowed = true;
    }


    /**
     * Speeds the enemy unit up.
     * */
    public void setUnslowed() {
        velocity *= 2;
        isSlowed = false;
    }


    /**
     * Sets the enemy unit active.
     * */
    public void setActive(){
        isActive = true;
    }


    /**
     * Sets the enemy unit inactive.
     * */
    public void setInactive(){
        isActive = false;
    }


    /**
     * Returns the health of the enemy unit.
     * */
    public int getHealth(){
        return health;
    }


    public int getBasehealth() {
        return this.basehealth;
    }


    /**
     * Sets the enemy unit used.
     * */
    public void setUsed(){
        isUsed = true;
    }


    /**
     * Sets the enemy unit unused.
     * */
    public void setUnused(){
        isUsed = false;
    }


    /**
     * Returns true if the enemy unit is used.
     * */
    public boolean isUsed(){
        return isUsed;
    }


    /**
     * Returns the velocity of the enemy unit.
     * */
    public float getVelocity() {
        return this.velocity;
    }


    /**
     * Sets the enemy unit damageable.
     * */
    public void setDamagable() {
        isDamagable = true;
    }


    /**
     * Sets the enemy unit invincible.
     * */
    public void setUndamagable() {
        isDamagable = false;
    }


    /**
     * Returns true if the enemy unit is damageable.
     * */
    public boolean isDamagable() {
        return isDamagable;
    }


    /**
     * Resets the health of the enemy unit.
     * */
    public void resetHealth() {
        health = basehealth;
    }

    public void dispose() {

    }
}
