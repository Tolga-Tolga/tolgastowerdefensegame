package de.tolga.dunestowerdefense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class PauseScreen extends ScreenAdapter {
    Vector2 cursorController;
    Array<Controller> everyController;
    Controller controller;

    //stage&skin
    Skin mySkin;
    Stage stage;
    Camera camera;
    final private TextButton returnToGame;
    final private TextButton options;
    final private TextButton exitGame;
    final private TextButton leaderboard;

    //background
    private static Texture BACKGROUND_TEXTURE;
    private static Sprite BACKGROUND_SPRITE;
    final private SpriteBatch spriteBatch;

    //clock
    static int clock;


    final static int MAX_VELOCITY = 1000;

    public PauseScreen(){
        cursorController = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        everyController = Controllers.getControllers();
        controller = everyController.first();

        mySkin = new Skin(Gdx.files.internal("skins/craftacular/skin/craftacular-ui.json"));
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        clock = 0;

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        returnToGame = new TextButton("New Game", mySkin);
        options = new TextButton("Options", mySkin);
        exitGame = new TextButton("Exit Game", mySkin);
        leaderboard = new TextButton("Leaderboard", mySkin);

        returnToGame.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        options.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        leaderboard.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);
        exitGame.setSize(Gdx.graphics.getWidth()*0.75f, Gdx.graphics.getHeight()*0.1f);

        returnToGame.setPosition(Gdx.graphics.getWidth()*0.125f,Gdx.graphics.getHeight()*0.85f);
        options.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.60f);
        leaderboard.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.35f);
        exitGame.setPosition(Gdx.graphics.getWidth()*0.125f, Gdx.graphics.getHeight()*0.10f);


        Group group = new Group();
        group.addActor(returnToGame);
        group.addActor(options);
        group.addActor(exitGame);
        group.addActor(leaderboard);
        stage.addActor(group);

        BACKGROUND_TEXTURE = new Texture("Dirt_Background.png");
        BACKGROUND_SPRITE = new Sprite(BACKGROUND_TEXTURE);
        spriteBatch = new SpriteBatch();
    }


    /**
     * Function which gets called every millisecond and updates everything which needs to be update every tick.
     * @param delta the delta time.
     * */
    public void render(float delta) {
        clock++;
        if(clock == 20){
            cursorController.x = Gdx.input.getX();
            cursorController.y = Gdx.input.getY();
            clock = 0;
        }

        returnToGame();
        openOptionMenu();
        exitGame();
        background();

        stage.act();
        stage.draw();
        input(delta);
    }


    /**
     * Returns to the game screen.
     * */
    private void returnToGame(){
        if (returnToGame.isPressed()||(returnToGame.isOver() && controller.getButton(ControllerButton.A))){
            //DunesTowerDefense dunesTowerDefense = DunesTowerDefense.getInstance();
            //dunesTowerDefense.setScreen(new GameScreen());
        }
    }


    /**
     * Opens the option menu screen.
     * */
    private void openOptionMenu(){
        if (options.isPressed()||(options.isOver() && controller.getButton(ControllerButton.A))){
            //DunesTowerDefense dunesTowerDefense = DunesTowerDefense.getInstance();
            //dunesTowerDefense.setScreen(new OptionScreen());
        }
    }


    /**
     * Closes the game.
     * */
    private void exitGame() {
        if (exitGame.isPressed()||(exitGame.isOver() && controller.getButton(ControllerButton.A))){
            Gdx.app.exit();
        }
    }


    /**
     * Reads input of mouse, keyboard and gamepad.
     * @param delta the delta time.
     * */
    private void input(float delta){
        if(controller.isConnected()){
            Gdx.input.setCursorCatched(false);
            if(controller.getAxis(1) >= 0.05f || controller.getAxis(1) <= -0.05f){
                cursorController.y += MAX_VELOCITY * delta * controller.getAxis(1);
                Gdx.input.setCursorPosition((int) cursorController.x, (int) cursorController.y);
            }
            if(controller.getAxis(0) >= 0.05f || controller.getAxis(0) <= -0.05f){
                cursorController.x += MAX_VELOCITY * delta * controller.getAxis(0);
                Gdx.input.setCursorPosition((int) cursorController.x, (int) cursorController.y);
            }
        }
    }

    private void background(){
        spriteBatch.begin();
        BACKGROUND_SPRITE.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        BACKGROUND_SPRITE.draw(spriteBatch);
        spriteBatch.end();
    }
}
