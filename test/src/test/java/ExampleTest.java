import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExampleTest {

    @Test
    @DisplayName("Simple Test")
    void correctTest(){
        assertEquals(10, 10);
        assertEquals(10, 10);
    }

    @Test
    @DisplayName("Multiple Tests in one Test")
    void mulitpleTests(){
        Assertions.assertAll(
                () -> assertEquals(10, 10),
                () -> assertEquals(10, 10));
    }

    @Test
    @DisplayName("Only runs if criteria are met")
    void name() {
        Assumptions.assumeTrue(true);
        assertEquals(10, 10);
    }

    @Test
    @DisplayName("Target in slowing range test. Acoustic Tower.")
    void acousticTower() {

    }
}
