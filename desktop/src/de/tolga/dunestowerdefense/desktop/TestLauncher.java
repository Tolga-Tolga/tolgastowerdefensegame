package de.tolga.dunestowerdefense.desktop;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import de.tolga.dunestowerdefense.*;
import org.junit.Test;
import tests.TestGame;
import tests.TestScreen;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestLauncher {
    TestGame testGame;
    TestScreen testScreen;
    Vector3 testPosition = new Vector3();
    Lwjgl3ApplicationConfiguration config;

    public TestLauncher() {
        config = new Lwjgl3ApplicationConfiguration();
        Graphics.DisplayMode[] displayModes = Lwjgl3ApplicationConfiguration.getDisplayModes();
        System.out.println(1);
        config.setWindowPosition(10000, 10000);
        config.setWindowedMode(1,1);
        testScreen = null;
        testGame = new TestGame();
        Lwjgl3Application app = new Lwjgl3Application(testGame, config);
        testScreen = testGame.getTestScreen();
    }

    @Test
    public void towerRange() {
        //I only test it with the acoustic tower because the other tower work the same
        testPosition.set(0, 0,0);
        AcousticTower acousticTower = testScreen.acousticTowerPool.getTower(0);
        acousticTower.setPosition(0,0);
        GunTurret gunTurret = testScreen.gunTurretTowerPool.getTower(0);
        gunTurret.setPosition(0,0);
        BombTower bombTower = testScreen.bombTowerPool.getTower(0);
        bombTower.setPosition(0,0);
        //should be true
        boolean targetInRange = acousticTower.targetInSlowingRange(new Vector2(-200, 0));
        assertEquals(true, targetInRange);
        targetInRange = gunTurret.targetInShootingRange(new Vector2(-150, 0));
        assertEquals(true, targetInRange);
        targetInRange = bombTower.targetInShootingRange(new Vector2(-300, 0));
        assertEquals(true, targetInRange);

        targetInRange = acousticTower.targetInSlowingRange(new Vector2(0, 200));
        assertEquals(true, targetInRange);

        targetInRange = acousticTower.targetInSlowingRange(new Vector2(0, 0));
        assertEquals(true, targetInRange);
        //should be false
        targetInRange = acousticTower.targetInSlowingRange(new Vector2(-200, -200));
        assertEquals(false, targetInRange);
    }

    @Test
    public void enemyUnitPath() {
        boolean[][] map = new boolean[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                map[i][j] = true;
            }
        }
        map[9][0] = false;
        map[9][1] = false;
        map[8][1] = false;
        map[7][1] = false;
        map[6][1] = false;
        map[6][2] = false;
        map[6][3] = false;
        map[6][4] = false;
        map[6][5] = false;
        map[6][6] = false;
        map[5][6] = false;
        map[4][6] = false;
        map[3][6] = false;
        map[2][6] = false;
        map[1][6] = false;
        map[1][7] = false;
        map[1][8] = false;
        map[1][9] = false;
        map[0][9] = false;
        GraphPath<Node> temp = EnemyUnitPath.getPath(map);
        boolean checkIfPathIsCorrect = false;
        try {
            checkIfPathIsCorrect = temp.get(0).x == 0 && temp.get(0).y == 9
                    && temp.get(1).x == 1 && temp.get(1).y == 9
                    && temp.get(2).x == 1 && temp.get(2).y == 8
                    && temp.get(3).x == 1 && temp.get(3).y == 7
                    && temp.get(4).x == 1 && temp.get(4).y == 6
                    && temp.get(5).x == 2 && temp.get(5).y == 6
                    && temp.get(6).x == 3 && temp.get(6).y == 6
                    && temp.get(7).x == 4 && temp.get(7).y == 6
                    && temp.get(8).x == 5 && temp.get(8).y == 6
                    && temp.get(9).x == 6 && temp.get(9).y == 6
                    && temp.get(10).x == 6 && temp.get(10).y == 5
                    && temp.get(11).x == 6 && temp.get(11).y == 4
                    && temp.get(12).x == 6 && temp.get(12).y == 3
                    && temp.get(13).x == 6 && temp.get(13).y == 2
                    && temp.get(14).x == 6 && temp.get(14).y == 1
                    && temp.get(15).x == 7 && temp.get(15).y == 1
                    && temp.get(16).x == 8 && temp.get(16).y == 1
                    && temp.get(17).x == 9 && temp.get(17).y == 1
                    && temp.get(18).x == 9 && temp.get(18).y == 0;
        } catch (Exception e) {
            checkIfPathIsCorrect = false;
        }
        assertEquals(true, checkIfPathIsCorrect);
        assertEquals(19, temp.getCount());
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                map[i][j] = false;
            }
        }
        temp = EnemyUnitPath.getPath(map);
        assertEquals(19, temp.getCount());
    }

    @Test
    public void towerPlacement() {
        boolean[][] map = new boolean[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                map[i][j] = false;
            }
        }
        assertEquals(true, testScreen.buyAcousticTower(map, 0, 1));
        assertEquals(true, testScreen.buyAcousticTower(map, 7, 4));
        assertEquals(false, testScreen.buyAcousticTower(map, 0, 9));
        assertEquals(false, testScreen.buyAcousticTower(map, 9, 0));

        map[7][4] = true;
        assertEquals(false, testScreen.buyAcousticTower(map, 7, 4));
    }

    @Test
    public void knockerPlacement() {
        boolean[][] map = new boolean[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                map[i][j] = false;
            }
        }
        map[1][2] = true;
        assertEquals(false, testScreen.placeKnocker(map, 1, 2));
        assertEquals(true, testScreen.placeKnocker(map, 2, 2));
        assertEquals(true, testScreen.placeKnocker(map, 3, 2));
        assertEquals(false, testScreen.placeKnocker(map, 4, 2));
    }

    @Test
    public void gameLost() {
        testScreen.healthPointScoreAndMoneyValues.set(0, 1);
        assertEquals(false, testScreen.isGameOverTest());
        testScreen.healthPointScoreAndMoneyValues.set(0, 0);
        assertEquals(true, testScreen.isGameOverTest());
        testScreen.healthPointScoreAndMoneyValues.set(0, -1);
        assertEquals(true, testScreen.isGameOverTest());
    }

    @Test
    public void gameWon() {
        testScreen.wave.setRound(66);
        testScreen.waveCleared = true;
        testScreen.isGameWon();
        assertEquals(false, testScreen.gameWon());
        testScreen.gameWon = false;
        testScreen.waveCleared = true;
        testScreen.wave.setRound(97);
        testScreen.isGameWon();
        assertEquals(true, testScreen.gameWon);
        testScreen.gameWon = false;
        testScreen.waveCleared = true;
        testScreen.wave.setRound(96);
        testScreen.isGameWon();
        assertEquals(false, testScreen.gameWon);
        testScreen.gameWon = false;
        testScreen.waveCleared = true;
        testScreen.wave.setRound(98);
        testScreen.isGameWon();
        assertEquals(false, testScreen.gameWon);
    }

    @Test
    public void checkShootingAndAmmunition() {
        Infantry testInfantry = testScreen.infantryPool.getInfantry(0);
        testScreen.gunTurretTowerPool.addActiveGunTurretInstance(testScreen.gunTurretTowerPool.getTower(0).getGunTurretModelInstance());
        GunTurret testTower = testScreen.activeGunTurretTowerPool.get(0);
        testInfantry.setPosition(40,40);
        testScreen.wave.getUnits().add(testInfantry);
        testTower.setPosition(0,0);
        testScreen.shootAtEnemies(0f);
        ArrayList<Ammonation> ammunitions = testScreen.wave.getShots();
        testScreen.shootAtEnemies(0f);
        assertEquals(2, ammunitions.size());
        Ammonation ammunition = ammunitions.get(0);
        assertEquals(testInfantry, ammunition.getTarget());
        assertEquals(testTower, ammunition.getTower());
        assertEquals(true, ammunition.isActive());
        assertEquals(true, ammunition.isFollowingTarget());

        testScreen.gunTurretTowerPool.deleteActiveGunTurret(testScreen.gunTurretTowerPool.getTower(0));
        testInfantry.setPosition(100000,10000);
        testScreen.wave.getUnits().remove(testInfantry);
        testTower.setPosition(-1000000, -1000000);
        testScreen.wave.getShots().clear();
        for (int i = 0; i < testScreen.blockShooting.length; i++) {
            Arrays.fill(testScreen.blockShooting[i], 0);
        }

        Harvester testHarvester = testScreen.harvesterPool.getHarvester().get(0);
        BombTower bombTower = testScreen.bombTowerPool.getTower().get(0);
        testScreen.bombTowerPool.getActiveBombTower().add(bombTower);
        testScreen.bombTowerPool.getActiveBombTowerInstances().add(bombTower.getBombTowerModelInstance());
        testHarvester.setPosition(30,30);
        testScreen.wave.getUnits().add(testHarvester);
        bombTower.setPosition(30,30);
        testScreen.shootAtEnemies(0f);
        ammunition = ammunitions.get(0);
        assertEquals(testHarvester, ammunition.getTarget());
        assertEquals(true, ammunition.isActive());
    }

    @Test
    public void checkWaves() {
        testScreen.wave.setRound(1);
        testScreen.startNextWave();
        assertEquals(false, testScreen.wave.waveOver());
        testScreen.wave.spawnNextUnit();
        testScreen.infantryPool = new InfantryPool(1000, testScreen.startPortal, testScreen.endPortal, testScreen.gameField, testScreen.assets, testScreen.healthPointScoreAndMoneyValues);
        testScreen.harvesterPool = new HarvesterPool(100, testScreen.startPortal, testScreen.endPortal, testScreen.gameField, testScreen.assets, testScreen.healthPointScoreAndMoneyValues);
        testScreen.bossUnitPool = new BossUnitPool(100, testScreen.startPortal, testScreen.endPortal, testScreen.gameField, testScreen.assets, testScreen.healthPointScoreAndMoneyValues);
        assertEquals(1, testScreen.wave.getUnits().size());
        testScreen.wave = new Wave(testScreen.infantryPool, testScreen.bossUnitPool, testScreen.harvesterPool, testScreen.startPortal, testScreen.endPortal, testScreen.ammoPool, testScreen.healthPointScoreAndMoneyValues);
        testScreen.wave.setRound(97);
        testScreen.startNextWave();
        while (!testScreen.wave.queueEmpty()) {
            testScreen.wave.spawnNextUnit();
        }
        boolean isBossExistent = false;
        for (EnemyUnit enemyUnit: testScreen.waveUnits) {
            if (enemyUnit instanceof BossUnit) isBossExistent = true;
        }
        assertEquals(true, isBossExistent);
        testScreen.wave.setRound(96);
        testScreen.startNextWave();
        while (!testScreen.wave.queueEmpty()) {
            testScreen.wave.spawnNextUnit();
        }
        isBossExistent = false;
        assertEquals(false, isBossExistent);
    }

    @Test
    public void getUnusedTower() {
        AcousticTowerPool acousticTowerPool = testScreen.acousticTowerPool;
        BombTowerPool bombTowerPool = testScreen.bombTowerPool;
        GunTurretTowerPool gunTurretTowerPool = testScreen.gunTurretTowerPool;

        AcousticTower usedAcousticTower = acousticTowerPool.getUnusedTower();
        BombTower usedBombTower = bombTowerPool.getUnusedTower();
        GunTurret usedGunTurret = gunTurretTowerPool.getUnusedTower();

        usedAcousticTower.setPosition(0,0);
        usedBombTower.setPosition(0,0);
        usedGunTurret.setPosition(0,0);

        assertNotEquals(usedAcousticTower, acousticTowerPool.getUnusedTower());
        assertNotEquals(usedBombTower, bombTowerPool.getUnusedTower());
        assertNotEquals(usedGunTurret, gunTurretTowerPool.getUnusedTower());
    }

    @Test
    public void checkGraphValid() {
        DunesTdGraph dunesTdGraph = new DunesTdGraph();

        Node node0 = new Node(0,0,0);
        Node node1 = new Node(0,0,1);
        Node node2 = new Node(0,0,1);
        Node node3 = new Node(0,0,1);
        Node node4 = new Node(0,0,1);

        dunesTdGraph.addNode(node0);
        dunesTdGraph.addNode(node1);
        dunesTdGraph.addNode(node2);
        dunesTdGraph.addNode(node3);
        dunesTdGraph.addNode(node4);

        dunesTdGraph.connectNodes(node0, node4);
        dunesTdGraph.connectNodes(node4, node1);
        dunesTdGraph.connectNodes(node4, node2);
        dunesTdGraph.connectNodes(node4, node3);

        assertEquals(5, dunesTdGraph.getNodeCount());

        assertEquals(0, dunesTdGraph.getIndex(node0));
        assertEquals(1, dunesTdGraph.getIndex(node1));
        assertEquals(2, dunesTdGraph.getIndex(node2));
        assertEquals(3, dunesTdGraph.getIndex(node3));
        assertEquals(4, dunesTdGraph.getIndex(node4));

        Array<Connection<Node>> connections = dunesTdGraph.getConnections(node0);
        assertEquals(node0, connections.get(0).getFromNode());
        assertEquals(node4, connections.get(0).getToNode());
        assertEquals(1, connections.size);
        assertNotEquals(node0, connections.get(0).getToNode());
        assertNotEquals(node1, connections.get(0).getToNode());
        assertNotEquals(node2, connections.get(0).getToNode());
        assertNotEquals(node3, connections.get(0).getToNode());

        connections = dunesTdGraph.getConnections(node4);
        assertEquals(node4, connections.get(0).getFromNode());
        assertNotEquals(node0, connections.get(0).getToNode());
        assertEquals(node1, connections.get(0).getToNode());
        assertEquals(node2, connections.get(1).getToNode());
        assertEquals(node3, connections.get(2).getToNode());
        assertEquals(3, connections.size);
        assertNotEquals(node4, connections.get(0).getToNode());
        assertNotEquals(node4, connections.get(1).getToNode());
        assertNotEquals(node4, connections.get(2).getToNode());
    }

    @Test
    public void enemyUnit() {
        InfantryPool infantryPool = testScreen.infantryPool;
        BossUnitPool bossUnitPool = testScreen.bossUnitPool;
        HarvesterPool harvesterPool = testScreen.harvesterPool;

        Infantry infantry = infantryPool.getInfantry(1);
        BossUnit bossUnit = bossUnitPool.getBossUnit(1);
        Harvester harvester = harvesterPool.getHarvester().get(1);

        assertEquals(false, infantry.isDead());
        assertEquals(false, infantry.isUsed());
        assertEquals(false, bossUnit.isDead());
        assertEquals(false, bossUnit.isUsed());
        assertEquals(false, harvester.isDead());
        assertEquals(false, harvester.isUsed());

        testScreen.wave.startNextRound();
        while (!testScreen.wave.queueEmpty()) {
            testScreen.wave.spawnNextUnit();
        }
        assertEquals(false, infantry.isDead());
        assertEquals(true, infantry.isUsed());
        assertEquals(false, bossUnit.isDead());
        assertEquals(false, bossUnit.isUsed());
        assertEquals(false, harvester.isDead());
        assertEquals(false, harvester.isUsed());

        infantry.giveDamage(infantry.getBasehealth());
        assertEquals(true, infantry.isDead());

        harvester.giveDamage(harvester.getBasehealth()-1);
        assertEquals(false, harvester.isDead());

        bossUnit.giveDamage(bossUnit.getBasehealth() +1);
        assertEquals(true, bossUnit.isDead());
    }

    @Test
    public void shaiHulud() {
        ShaiHulud shaiHulud = testScreen.shaiHulud;
        shaiHulud.setShaiHuludActive();
        shaiHulud.setPosition(new Vector3(0,0,0));

        testScreen.wave.setRound(100);
        testScreen.wave.startNextRound();
        testScreen.wave.startNextRound();
        while (!testScreen.wave.queueEmpty()) {
            testScreen.wave.spawnNextUnit();
        }
        ArrayList<EnemyUnit> enemyUnits = testScreen.wave.getUnits();

        for (EnemyUnit enemyUnit:enemyUnits) {
            enemyUnit.getPosition().set(1,0,0);
        }

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(false, enemyUnit.isDead());
        }

        AcousticTowerPool acousticTowerPool = testScreen.acousticTowerPool;
        BombTowerPool bombTowerPool = testScreen.bombTowerPool;
        GunTurretTowerPool gunTurretTowerPool = testScreen.gunTurretTowerPool;

        AcousticTower acousticTower = acousticTowerPool.getUnusedTower();
        BombTower bombTower = bombTowerPool.getUnusedTower();
        GunTurret gunTurret = gunTurretTowerPool.getUnusedTower();


        acousticTower.setPosition(0,0);
        bombTower.setPosition(0,0);
        gunTurret.setPosition(0,0);



        shaiHulud.checkIfTowerAreInRangeAndDeleteThem(acousticTowerPool);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(true, enemyUnit.isUsed());
        }

        assertNotEquals(0, (int) acousticTower.getPosition().x);
        assertNotEquals(0, (int) acousticTower.getPosition().z);
        assertEquals(0, (int) bombTower.getPosition().x);
        assertEquals(0, (int) bombTower.getPosition().z);
        assertEquals(0, (int) gunTurret.getPosition().x);
        assertEquals(0, (int) gunTurret.getPosition().z);


        shaiHulud.checkIfTowerAreInRangeAndDeleteThem(bombTowerPool);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(true, enemyUnit.isUsed());
        }

        assertNotEquals(0, (int) acousticTower.getPosition().x);
        assertNotEquals(0, (int) acousticTower.getPosition().z);
        assertNotEquals(0, (int) bombTower.getPosition().x);
        assertNotEquals(0, (int) bombTower.getPosition().z);
        assertEquals(0, (int) gunTurret.getPosition().x);
        assertEquals(0, (int) gunTurret.getPosition().z);


        shaiHulud.checkIfTowerAreInRangeAndDeleteThem(gunTurretTowerPool);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(true, enemyUnit.isUsed());
        }

        assertNotEquals(0, (int) acousticTower.getPosition().x);
        assertNotEquals(0, (int) acousticTower.getPosition().z);
        assertNotEquals(0, (int) bombTower.getPosition().x);
        assertNotEquals(0, (int) bombTower.getPosition().z);
        assertNotEquals(0, (int) gunTurret.getPosition().x);
        assertNotEquals(0, (int) gunTurret.getPosition().z);


        shaiHulud.checkIfEnemiesAreInRangeAndKillThem(testScreen.wave);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(false, enemyUnit.isUsed());
        }

        assertNotEquals(0, (int) acousticTower.getPosition().x);
        assertNotEquals(0, (int) acousticTower.getPosition().z);
        assertNotEquals(0, (int) bombTower.getPosition().x);
        assertNotEquals(0, (int) bombTower.getPosition().z);
        assertNotEquals(0, (int) gunTurret.getPosition().x);
        assertNotEquals(0, (int) gunTurret.getPosition().z);


        config = new Lwjgl3ApplicationConfiguration();
        Graphics.DisplayMode[] displayModes = Lwjgl3ApplicationConfiguration.getDisplayModes();
        System.out.println(1);
        config.setWindowPosition(10000, 10000);
        config.setWindowedMode(1,1);
        testScreen = null;
        testGame = new TestGame();
        Lwjgl3Application app = new Lwjgl3Application(testGame, config);
        testScreen = testGame.getTestScreen();


        shaiHulud = testScreen.shaiHulud;
        shaiHulud.setShaiHuludActive();
        shaiHulud.setPosition(new Vector3(0,0,51));

        testScreen.wave.setRound(100);
        testScreen.wave.startNextRound();
        testScreen.wave.startNextRound();
        while (!testScreen.wave.queueEmpty()) {
            testScreen.wave.spawnNextUnit();
        }
        enemyUnits = testScreen.wave.getUnits();

        for (EnemyUnit enemyUnit:enemyUnits) {
            enemyUnit.getPosition().set(0,0,0);
        }

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(false, enemyUnit.isDead());
        }

        acousticTowerPool = testScreen.acousticTowerPool;
        bombTowerPool = testScreen.bombTowerPool;
        gunTurretTowerPool = testScreen.gunTurretTowerPool;

        acousticTower = acousticTowerPool.getUnusedTower();
        bombTower = bombTowerPool.getUnusedTower();
        gunTurret = gunTurretTowerPool.getUnusedTower();


        acousticTower.setPosition(0,0);
        bombTower.setPosition(0,0);
        gunTurret.setPosition(0,0);



        shaiHulud.checkIfTowerAreInRangeAndDeleteThem(acousticTowerPool);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(true, enemyUnit.isUsed());
        }

        assertEquals(0, (int) acousticTower.getPosition().x);
        assertEquals(0, (int) acousticTower.getPosition().z);
        assertEquals(0, (int) bombTower.getPosition().x);
        assertEquals(0, (int) bombTower.getPosition().z);
        assertEquals(0, (int) gunTurret.getPosition().x);
        assertEquals(0, (int) gunTurret.getPosition().z);


        shaiHulud.checkIfTowerAreInRangeAndDeleteThem(bombTowerPool);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(true, enemyUnit.isUsed());
        }

        assertEquals(0, (int) acousticTower.getPosition().x);
        assertEquals(0, (int) acousticTower.getPosition().z);
        assertEquals(0, (int) bombTower.getPosition().x);
        assertEquals(0, (int) bombTower.getPosition().z);
        assertEquals(0, (int) gunTurret.getPosition().x);
        assertEquals(0, (int) gunTurret.getPosition().z);


        shaiHulud.checkIfTowerAreInRangeAndDeleteThem(gunTurretTowerPool);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(true, enemyUnit.isUsed());
        }

        assertEquals(0, (int) acousticTower.getPosition().x);
        assertEquals(0, (int) acousticTower.getPosition().z);
        assertEquals(0, (int) bombTower.getPosition().x);
        assertEquals(0, (int) bombTower.getPosition().z);
        assertEquals(0, (int) gunTurret.getPosition().x);
        assertEquals(0, (int) gunTurret.getPosition().z);


        shaiHulud.checkIfEnemiesAreInRangeAndKillThem(testScreen.wave);

        for (EnemyUnit enemyUnit:enemyUnits) {
            assertEquals(true, enemyUnit.isUsed());
        }

        assertEquals(0, (int) acousticTower.getPosition().x);
        assertEquals(0, (int) acousticTower.getPosition().z);
        assertEquals(0, (int) bombTower.getPosition().x);
        assertEquals(0, (int) bombTower.getPosition().z);
        assertEquals(0, (int) gunTurret.getPosition().x);
        assertEquals(0, (int) gunTurret.getPosition().z);
    }
}
