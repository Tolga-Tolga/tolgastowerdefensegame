package de.tolga.dunestowerdefense.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import de.tolga.dunestowerdefense.DunesTowerDefense;

public class DesktopLauncher {


	public DesktopLauncher() {
	}

	public static void main (String[] arg) {

		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setTitle("Dunes TD");
		config.useVsync(true);
		config.setWindowIcon("mark-kuiper-2BMSj-xN008-unsplash.jpg");
		//Graphics.DisplayMode[] displayModes = Lwjgl3ApplicationConfiguration.getDisplayModes();
		//for (Graphics.DisplayMode displayMode : displayModes) {
		//	System.out.println(displayMode);
		//}
		config.setFullscreenMode(Lwjgl3ApplicationConfiguration.getDisplayMode());
		Lwjgl3Application app = new Lwjgl3Application(new DunesTowerDefense(), config);
	}
}
