# Dunes TD
## Spiel
Dunes TD ist ein 3D tower defense game welches auf dem Framework libgdx programmiert wurde.

## Spielregeln
Der Spieler kann mit Spice Türme kaufen und platzieren.
Das Platzieren von 2 Klopfern bewirkt das erscheinen des Shai Huluds.
Türme können nur so gesetzt werden, dass sie auf keinen Fall den Weg der gegnerischen Einheiten blockieren können.
Die Klopfer können nur senkrecht oder waagerecht platziert werden.

## Steuerung
#### Tastatur und Maus:
Maus: Für das Ziehen der Türme und auswählen der Knöpfe.<br/>
L: Für das Starten einer neuen Wave.<br/>
WASD und Pfeiltasten: Für das bewegen der Kamera.<br/>
R: Für das zurücksetzen der Kameraposition.<br/>
ESC: Zum öffnen des Pausemenüs.

#### XBOX Controller: 
Linker Cursor: Für das Ziehen der Türme und auswählen der Knöpfe.<br/>
Y: Für das Starten einer neuen Wave.<br/>
Rechter Cursor und Richtungstasten: Für das bewegen der Kamera.<br/>
SELECT: Für das zurücksetzen der Kameraposition.<br/>
START: Zum öffnen des Pausemenüs.

## Bugs
Türme und Klopfer sollten nicht gekauft werden, falls die Kamera bewegt wurde.
D.h vor dem kaufen eines Klopfers oder Turmes sollte die Kameraposition zurückgesetzt werden. (R/Select)